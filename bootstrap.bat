@echo off

if not exist "external_bin" (
	echo Unzipping external binary dependencies ...
	.\unzip.exe -q -u .\external_bin.zip
	echo Done.
)

if not exist "lib" (
	echo Unzipping external library dependencies ...
	.\unzip.exe -q -u .\external_lib.zip
	echo Done.
)

@if [%1]==[] @pause