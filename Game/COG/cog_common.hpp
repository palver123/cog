#pragma once

#include "COG\ShaderConfiguration.h"
#include "COG\ShaderStructures.h"
#include "COG\DeviceResources.h"
#include "COG\DirectXHelper.h"

namespace COG {
	/// <summary>
	/// An enumerator for the pipeline stages of DirectX 11 </summary>
	enum PipelineStage : char{
		InputAssembler,
		VertexShader,
		HullShader,
		DomainShader,
		GeometryShader,
		StreamOutputShader,
		Rasterizer,
		PixelShader,
		OutputMerger
	};
}
