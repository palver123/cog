#pragma once

#include "COG\Mesh\NodeAnimation.h"

namespace COG{
	enum AnimationBehaviour { B_CONSTANT, B_REPEAT };

	class Animation final
	{
	public:
		double m_duration;
		double m_ticksPerSecond;
		std::string m_Name;
		std::unordered_map<unsigned int, NodeAnimation> m_channels;
		AnimationBehaviour m_behaviour;

		explicit Animation(const aiAnimation* anim, AnimationBehaviour behaviour = B_CONSTANT);
		Animation(const aiAnimation* anim, std::string name, AnimationBehaviour behaviour = B_CONSTANT);

		DirectX::XMMATRIX ApplyAnimation(const BoneNode& node, const double animationTime) const;
	};
}
