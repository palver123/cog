#include "pch.h"
#include "ResourcePool.h"


#pragma comment(lib, "d3dcompiler.lib")

using namespace std;
using namespace DirectX;
using namespace COG;

// Static member initialization
const std::wstring ResourcePool::prepassPS_key = L"prepass";
//const std::wstring ResourcePool::voidPS_key = L"void";
const std::string ResourcePool::shadowSampler_key = "shadow";
const unsigned int ResourcePool::timeCB_slot = 0;
const unsigned int ResourcePool::camposCB_slot = 1;
const unsigned int ResourcePool::lightCB_slot = 3;

ResourcePool::ResourcePool(
	const shared_ptr<DX::DeviceResources> deviceResources,
	bool shadowMappingEnabled) :
	m_deviceResources(deviceResources),
	m_shadowMappingEnabled{ shadowMappingEnabled },
	m_loadingComplete{ false }
{
	CreateDeviceDependentResources();
}

void ResourcePool::CreateDeviceDependentResources()
{
	// Create the constant buffers
	// This CB is likely to be used by the vertex shader for skinning, but sometimes the pixel shader needs it too so it's kinda shared
	CD3D11_BUFFER_DESC applicationTimeConstantBufferDesc(
		sizeof(XMFLOAT4X4),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&applicationTimeConstantBufferDesc,
			nullptr,
			m_timeConstantBuffer.GetAddressOf()
		)
	);

	// Create a shadow sampler state description.
	D3D11_SAMPLER_DESC samplerDesc;
	ID3D11SamplerState* samplerState;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;	// Hopefully it will be faster than linear interpolation. Using linear wouldn't really add much to the visual experience anyway...
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;	// the most logical for 'out-of-bound' addressing of the shadow map
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;	// the most logical for 'out-of-bound' addressing of the shadow map
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;	// the most logical for 'out-of-bound' addressing of the shadow map
	samplerDesc.MipLODBias = 0.0f;							// default
	samplerDesc.MaxAnisotropy = 1;							// default
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 1.0f;						// white stands for infinitely distant objects
	samplerDesc.BorderColor[1] = 1.0f;						// white stands for infinitely distant objects
	samplerDesc.BorderColor[2] = 1.0f;						// white stands for infinitely distant objects
	samplerDesc.BorderColor[3] = 0.0f;						// alpha channel is not used.
	samplerDesc.MinLOD = 0.0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the shadow sampler state.
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateSamplerState(
		&samplerDesc,
		&samplerState
		)
		);

	// Add the newly created sampler state to the container.
	// The container will take care of the freeing the memory.
	AddSamplerState(ResourcePool::shadowSampler_key, samplerState);

/*	ID3D11Buffer* materialConstantBuffer;
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
		&float4ConstantBufferDesc,
		nullptr,
		&materialConstantBuffer
		)
		);*/

	// Create a pixel shader for prepasses. This shader is only used to write the prepass data into a render target (which may either be a frame buffer or a texture)
	auto loadPSTask = DX::ReadDataAsync(L"PrepassVisualizer.cso");

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
		ID3D11PixelShader* visualizerPS;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&visualizerPS
			)
		);
		// Add the newly created pixel shader to the container.
		// The container will take care of the freeing the memory.
		AddPixelShader(ResourcePool::prepassPS_key, visualizerPS);

		m_loadingComplete = true;
	});

//	// Create a void pixel shader that literally does nothing. Useful in shadow mapping.
//	ID3D11PixelShader* voidPS;
//	ID3DBlob* compiledCode;
//	const string shaderCode = "void VoidPixelShader(float4 input : SV_POSITION) { }";
//	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
//#if defined( DEBUG ) || defined( _DEBUG )
//	flags |= D3DCOMPILE_DEBUG;
//#endif
//
//	D3DCompile(
//		shaderCode.c_str(),
//		shaderCode.length(),
//		nullptr,
//		nullptr,
//		nullptr,
//		"VoidPixelShader",
//		"ps_4_0",
//		flags,
//		0,
//		&compiledCode,
//		nullptr
//		);
//
//	DX::ThrowIfFailed(
//		m_deviceResources->GetD3DDevice()->CreatePixelShader(
//			(byte*)compiledCode->GetBufferPointer(),
//			compiledCode->GetBufferSize(),
//			nullptr,
//			&voidPS
//		)
//	);
//
//	// Add the newly created pixel shader to the container.
//	// The container will take care of the freeing the memory.
//	AddPixelShader(ResourcePool::voidPS_key, voidPS);

	// This step releases all the slots (sampler slots, constant buffer slots, etc...)
	ResetAvailableSlots();
}

void ResourcePool::ReleaseDeviceDependentResources()
{
	m_loadingComplete = false;
	m_timeConstantBuffer.Reset();
	for (auto ptr = m_blendStates.begin(); ptr != m_blendStates.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_depthStencilStates.begin(); ptr != m_depthStencilStates.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_rasterizerStates.begin(); ptr != m_rasterizerStates.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_samplerStates.begin(); ptr != m_samplerStates.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_inputLayouts.begin(); ptr != m_inputLayouts.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_pixelShaders.begin(); ptr != m_pixelShaders.end(); ++ptr)
		ptr->second.Reset();
	for (auto ptr = m_vertexShaders.begin(); ptr != m_vertexShaders.end(); ++ptr)
		ptr->second.Reset();
	m_blendStates.clear();
	m_depthStencilStates.clear();
	m_rasterizerStates.clear();
	m_samplerStates.clear();
	m_inputLayouts.clear();
	m_pixelShaders.begin();
	m_vertexShaders.clear();
	m_vertexStrides.clear();
}

void ResourcePool::ResetAvailableSlots()
{
	for (int i = 0; i < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; i++)
	{
		m_availableVSSamplerSlots[i] = true;
		m_availableHSSamplerSlots[i] = true;
		m_availableDSSamplerSlots[i] = true;
		m_availableGSSamplerSlots[i] = true;
		m_availablePSSamplerSlots[i] = true;
	}

	for (int i = 0; i < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; i++)
	{
		m_availableVSConstantBufferSlots[i] = true;
		m_availableHSConstantBufferSlots[i] = true;
		m_availableDSConstantBufferSlots[i] = true;
		m_availableGSConstantBufferSlots[i] = true;
		m_availablePSConstantBufferSlots[i] = true;
	}
	// First 2 slots are reserved for perFrame and perPass constant buffers in vertex shader
	m_availablePSConstantBufferSlots[0] = false;	// Application time constant buffer
	m_availableVSConstantBufferSlots[0] = false;	// Application time constant buffer
	if (m_shadowMappingEnabled)
		m_availablePSSamplerSlots[0] = false;		// Shadow sampler
}

void ResourcePool::BindCommonResources()
{
	if (!m_loadingComplete)
		return;

	// Send the constant buffers to the graphics device.
	m_availableVSConstantBufferSlots[timeCB_slot] = false;
	m_deviceResources->GetD3DDeviceContext()->
		VSSetConstantBuffers(timeCB_slot, 1, m_timeConstantBuffer.GetAddressOf());

	m_availablePSConstantBufferSlots[timeCB_slot] = false;
	m_deviceResources->GetD3DDeviceContext()->
		PSSetConstantBuffers(timeCB_slot, 1, m_timeConstantBuffer.GetAddressOf());

	if (m_shadowMappingEnabled)
	{
		// Send the shadow sampler to the graphics device.
		m_deviceResources->GetD3DDeviceContext()->PSSetSamplers(0, 1, m_samplerStates[shadowSampler_key].GetAddressOf());
		m_lastPSSamplerStateKey = shadowSampler_key;
		m_availablePSSamplerSlots[0] = false;
	}

	ResetAvailableSlots();
}

void ResourcePool::Update(const float totalSeconds)
{
	if (!m_loadingComplete)
		return;

	m_applicationTime.value = DirectX::XMFLOAT4(totalSeconds, 0.0f, 0.0f, 0.0f);

	auto context = m_deviceResources->GetD3DDeviceContext();

	// Prepare the constant buffers to send it to the graphics device.
	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	DX::ThrowIfFailed(
		context->Map(
			m_timeConstantBuffer.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
		)
	);
	memcpy(mappedResource.pData, &m_applicationTime.value, sizeof(XMFLOAT4));
	context->Unmap(m_timeConstantBuffer.Get(), 0);
}

/*
void ResourcePool::UpdateMaterialBuffer(const MaterialConstantBuffer& material)
{
	ID3D11Buffer* constantBuffer = m_constantBuffers.at(L"material").Get();
	auto context = m_deviceResources->GetD3DDeviceContext();

	// Prepare the constant buffers to send it to the graphics device.
	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	DX::ThrowIfFailed(
		context->Map(
		constantBuffer,
		0,
		D3D11_MAP_WRITE_DISCARD,
		0,
		&mappedResource
		)
		);
	memcpy(mappedResource.pData, &material.value, sizeof(XMFLOAT4));
	context->Unmap(constantBuffer, 0);
}*/

void ResourcePool::SetInputLayout(const wstring& key)
{
	if (m_lastInputLayoutKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->IASetInputLayout(m_inputLayouts[key].Get());
		m_lastInputLayoutKey = key;
	}
}

void ResourcePool::SetVertexShader(const wstring& key)
{
	if (m_lastVSKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->VSSetShader(
			m_vertexShaders[key].Get(),
			nullptr,
			0
			);
		m_lastVSKey = key;
	}
}

void ResourcePool::SetHullShader(const wstring& key)
{
	if (m_lastHSKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->HSSetShader(
			m_hullShaders[key].Get(),
			nullptr,
			0
			);
		m_lastHSKey = key;
	}
}

void ResourcePool::SetDomainShader(const wstring& key)
{
	if (m_lastDSKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->DSSetShader(
			m_domainShaders[key].Get(),
			nullptr,
			0
			);
		m_lastDSKey = key;
	}
}

void ResourcePool::SetPixelShader(const wstring& key)
{
	if (m_lastPSKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->PSSetShader(
			m_pixelShaders[key].Get(),
			nullptr,
			0
			);
		m_lastPSKey = key;
	}
}

void ResourcePool::SetRasterizerState(const wstring& key)
{
	if (m_lastRasterizerStateKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->RSSetState(m_rasterizerStates[key].Get());
		m_lastRasterizerStateKey = key;
	}
}

void ResourcePool::SetDepthStencilState(const wstring& key)
{
	if (m_lastDepthStencilKey != key)
	{
		m_deviceResources->GetD3DDeviceContext()->OMSetDepthStencilState(
			m_depthStencilStates[key].Get(),
			0
			);
		m_lastDepthStencilKey = key;
	}
}

void ResourcePool::SetBlendState(const wstring& key)
{
	if (m_lastBlendStateKey != key)
	{
		// TODO sample mask and blend factors need to be set according to the blend state description
		m_deviceResources->GetD3DDeviceContext()->OMSetBlendState(
			m_blendStates[key].Get(),
			nullptr,
			0xffffffff
			);
		m_lastBlendStateKey = key;
	}
}

void ResourcePool::CreateVertexShader(const MeshDescription& mesh, wstring& key, const VertexShaderConfiguration& shaderConfiguration)
{
	// Determine the key string for the vertex shader, the input layout and the vertex stride
	key.clear();

	// Load shader asynchronously.
	auto loadVSTask = DX::ReadTextAsyncA(L"COG\\TemplatedShaders\\tmpl_VS.hlsl");

	// After the vertex shader file is loaded, create the shader and input layout.
	auto createVSTask = loadVSTask.then([this, mesh, &key, &shaderConfiguration](const string& fileData) {
		// The vertex shader object itself
		ID3D11VertexShader* vertexShader;
		// These blobs are necessary for the D3DCompiler methods
		ID3DBlob* compiledCode;
		ID3DBlob* errorMessage;
		// Vertex structure size in bytes
		unsigned int vertexStride = 0;
		// Number of vertex channels.
		unsigned int numVertexChannels = 0;
		unsigned int textureChannels = 0;	// less or equal to mesh.nTextureChannels
		unsigned int colorChannels = 0;		// less or equal to mesh.nVertexColorChannels
		// string position indicators for analyzing the shader source code.
		size_t offerBegin = 0;
		size_t lineBreak = 0;
		string offer;
		// vectors containing the features offered by the shader.
		vector<string> buffers;
		vector<string> offeredFeatures;
		// A vector containing the preprocessor macros used for compiling the final shader.
		vector<D3D_SHADER_MACRO> macros;

		// Collect the offered features
		while ((offerBegin = fileData.find("COG_OFFER_", offerBegin + 1)) != std::string::npos)
		{
			lineBreak = fileData.find("\r\n", offerBegin + 11);
			offer = fileData.substr(offerBegin + 10, lineBreak - (offerBegin + 10));
			offeredFeatures.push_back(offer);
		}

		// 'offeredFeatures' now contains all the offered features. Ready to analyze it!
		buffers.reserve(1 + 8 + 8);
		buffers.push_back(std::to_string(shaderConfiguration.numberOfBones));
		macros.push_back(COG_HLSL_MAX_NUMBER_OF_BONES_PER_MODEL(buffers.back().data()));
		macros.push_back(COG_HLSL_MAX_NUMBER_OF_BONES_PER_VERTEX);
		if (shaderConfiguration.hasWorldMatrix && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "WORLDMATRIX") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_WORLDMATRIX);
		if (shaderConfiguration.hasNormalMatrix && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "NORMALMATRIX") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_NORMALMATRIX);
		if (shaderConfiguration.isTimeVariant && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TIME_VARIANT") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_TIME_VARIANT);
		if (shaderConfiguration.usesShadowMap && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SHADOW_MAP") != offeredFeatures.cend())
		{
			macros.push_back(COG_HLSL_SHADOW_MAP);
			key.append(L"_S_");
		}

		if (mesh.hasPositions)
		{
			numVertexChannels++;
			vertexStride += 3 * sizeof(float);
		}
		if (mesh.hasNormals && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "NORMAL") != offeredFeatures.cend())
		{
			numVertexChannels++;
			key.append(L"N");
			vertexStride += 3 * sizeof(float);
			macros.push_back(COG_HLSL_HAS_NORMAL);
		}
		if (mesh.hasTangentBinormals && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TANGENT_AND_BINORMAL") != offeredFeatures.cend())
		{
			numVertexChannels += 2;
			key.append(L"tb");
			vertexStride += 6 * sizeof(float);
			macros.push_back(COG_HLSL_HAS_TANGENT_AND_BINORMAL);
		}
		if (mesh.hasSkinning && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SKINNING") != offeredFeatures.cend())
		{
			numVertexChannels += 2;
			key.append(L"B");
			vertexStride += 4 * sizeof(float);
			vertexStride += 4 * sizeof(unsigned int);
			macros.push_back(COG_HLSL_HAS_SKINNING);
		}
		for (unsigned int k = 0; k < mesh.nTextureChannels && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TEXCOORD_" + std::to_string(k)) != offeredFeatures.cend(); k++)
		{
			numVertexChannels++;
			textureChannels++;
			key.append(L"T");
			vertexStride += 2 * sizeof(float);
			buffers.push_back("COG_HAS_TEXCOORD_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_TEXCOORD(buffers.back().data()));
		}
		for (unsigned int k = 0; k < mesh.nVertexColorChannels && k < 8 && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "VERTEX_COLOR_" + std::to_string(k)) != offeredFeatures.cend(); k++)
		{
			numVertexChannels++;
			colorChannels++;
			key.append(L"C");
			vertexStride += 4 * sizeof(float);
			buffers.push_back("COG_HAS_VERTEX_COLOR_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_VERTEX_COLOR(buffers.back().data()));
		}
		if (shaderConfiguration.shouldPassWorldPos)
			macros.push_back(COG_HLSL_USE_WORLDPOS);
		macros.push_back(D3D_SHADER_MACRO{ nullptr, nullptr });

		// If the container already contains an element with the same key do not create it again
		if (m_vertexShaders.count(key))
			return;

		UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		flags |= D3DCOMPILE_DEBUG;
#endif

		D3DCompile(
			fileData.c_str(),
			fileData.length(),
			nullptr,
			macros.data(),
			nullptr,
			"main",
			"vs_4_0",
			flags,
			0,
			&compiledCode,
			&errorMessage
			);

		if (errorMessage != nullptr)
		{
			const size_t siz = errorMessage->GetBufferSize();
			const std::string shaderErrorMsg{ (const char*)errorMessage->GetBufferPointer() };
			throw std::runtime_error{ "Error while compiling the vertex shader! Details: " + shaderErrorMsg };
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateVertexShader(
			(byte*)compiledCode->GetBufferPointer(),
			compiledCode->GetBufferSize(),
			nullptr,
			&vertexShader
			)
			);

		// Add the newly created vertex shader to the container.
		// The container will take care of the freeing the memory.
		AddVertexShader(key, vertexShader);

		// Create an input layout
		ID3D11InputLayout* inputLayout;
		vector<D3D11_INPUT_ELEMENT_DESC> vertexDeclaration = vector<D3D11_INPUT_ELEMENT_DESC>(numVertexChannels);
		unsigned int alignedByteOffset = 0;
		unsigned int i = 0;
		if (mesh.hasPositions)
		{
			vertexDeclaration[i++] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 3 * sizeof(float);
		}
		if (mesh.hasNormals)
		{
			vertexDeclaration[i++] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 3 * sizeof(float);
		}
		if (mesh.hasTangentBinormals)
		{
			vertexDeclaration[i++] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 3 * sizeof(float);
			vertexDeclaration[i++] = { "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 3 * sizeof(float);
		}
		if (mesh.hasSkinning)
		{
			vertexDeclaration[i++] = { "BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 4 * sizeof(float);
			vertexDeclaration[i++] = { "BLENDINDICES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 4 * sizeof(unsigned int);
		}
		for (unsigned int k = 0; k < textureChannels; k++)
		{
			vertexDeclaration[i++] = { "TEXCOORD", k, DXGI_FORMAT_R32G32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 2 * sizeof(float);
		}
		for (unsigned int k = 0; k < colorChannels; k++)
		{
			vertexDeclaration[i++] = { "COLOR", k, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 4 * sizeof(float);
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateInputLayout(
			vertexDeclaration.data(),
			numVertexChannels,
			(byte*)compiledCode->GetBufferPointer(),
			compiledCode->GetBufferSize(),
			&inputLayout
			)
			);

		// Add the newly created input layout to the container.
		// The container will take care of the freeing the memory.
		AddInputLayout(key, inputLayout, vertexStride);
	});

	// Wait for the task to complete (kinda makes it synchronous)
	createVSTask.wait();
}

void ResourcePool::CreateVertexShader(wstring& key, const PrepassVSConfiguration& shaderConfiguration)
{
	// Determine the key string for the vertex shader, the input layout and the vertex stride
	key = shaderConfiguration.ShadowMap() ?
		L"shadow_" :
		(L"prepass_" + Stringify(shaderConfiguration.m_flags) + L"_");

	// Load shader asynchronously.
	auto loadVSTask = DX::ReadTextAsyncA(L"COG\\TemplatedShaders\\tmpl_prepassVS.hlsl");

	// After the vertex shader file is loaded, create the shader and input layout.
	auto createVSTask = loadVSTask.then([this, &key, &shaderConfiguration](string& fileData) {
		// The vertex shader object itself
		ID3D11VertexShader* vertexShader;
		// These blobs are necessary for the D3DCompiler methods
		ID3DBlob* compiledCode;
		ID3DBlob* errorMessage;
		// Vertex structure size in bytes
		unsigned int vertexStride = 3 * sizeof(float);	// float3 for position. Skinning might increase this value, this is just initialization.
		// Number of vertex channels.
		unsigned int numVertexChannels = shaderConfiguration.HasBones() ? 3 : 1;
		// string position indicators for analyzing the shader source code.
		size_t offerBegin = 0;
		size_t lineBreak = 0;
		string offer;
		// vectors containing the features offered by the shader.
		vector<string> buffers;
		vector<string> offeredFeatures;
		// A vector containing the preprocessor macros used for compiling the final shader.
		vector<D3D_SHADER_MACRO> macros;

		// Collect the offered features
		while ((offerBegin = fileData.find("COG_OFFER_", offerBegin + 1)) != std::string::npos)
		{
			lineBreak = fileData.find("\r\n", offerBegin + 11);
			offer = fileData.substr(offerBegin + 10, lineBreak - (offerBegin + 10));
			offeredFeatures.push_back(offer);
		}

		// 'offeredFeatures' now contains all the offered features. Ready to analyze it!
		if (shaderConfiguration.HasBones() && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SKINNING") != offeredFeatures.cend())
		{
			key.append(L"B");
			vertexStride += 4 * sizeof(float);
			vertexStride += 4 * sizeof(unsigned int);
			const auto numberOfBones = shaderConfiguration.NumberOfBones();
			buffers.push_back(std::to_string(numberOfBones));
			macros.push_back(COG_HLSL_HAS_SKINNING);
			macros.push_back(COG_HLSL_MAX_NUMBER_OF_BONES_PER_MODEL(buffers.back().data()));
			macros.push_back(COG_HLSL_MAX_NUMBER_OF_BONES_PER_VERTEX);

			// Okay, honestly, ugliest code of my life... :
			string ss = "bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL] = {";
			// "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}" + trailing zero has the same number of characters as "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}," without the trailing zero.
			ss.reserve(ss.length() + integral_constant<size_t, sizeof("{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}")>::value * numberOfBones);	// prevent unnecessary reallocations
			for (unsigned int i = 0; i < numberOfBones; i++)
				ss += "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},";
			ss[ss.length() - 1] = '}';
			// "bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL" + trailing zero has the same number of characters as "bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL]" without the trailing zero.
			fileData.replace(fileData.find("bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL]"), integral_constant<size_t, sizeof("bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL")>::value, ss);
		}
		if (shaderConfiguration.HasWorldMatrix() && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "WORLDMATRIX") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_WORLDMATRIX);
		if (shaderConfiguration.IsTimeVariant() && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TIME_VARIANT") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_TIME_VARIANT);
		if (shaderConfiguration.ShadowMap() && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SHADOW_MAP") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_SHADOW_MAP);
		macros.push_back(D3D_SHADER_MACRO{ nullptr, nullptr });

		// If the container already contains an element with the same key do not create it again
		if (m_vertexShaders.count(key))
			return;

		UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		flags |= D3DCOMPILE_DEBUG;
#endif

		D3DCompile(
			fileData.c_str(),
			fileData.length(),
			nullptr,
			macros.data(),
			nullptr,
			"main",
			"vs_4_0",
			flags,
			0,
			&compiledCode,
			&errorMessage
			);

		if (errorMessage != nullptr)
		{
			const size_t siz = errorMessage->GetBufferSize();
			const std::string shaderErrorMsg{ (const char*)errorMessage->GetBufferPointer() };
			throw std::runtime_error{ "Error while compiling the prepass vertex shader! Details: " + shaderErrorMsg };
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateVertexShader(
				(byte*)compiledCode->GetBufferPointer(),
				compiledCode->GetBufferSize(),
				nullptr,
				&vertexShader)
			);

		// Add the newly created vertex shader to the container.
		// The container will take care of the freeing the memory.
		AddVertexShader(key, vertexShader);

		// Create an input layout
		ID3D11InputLayout* inputLayout;
		vector<D3D11_INPUT_ELEMENT_DESC> vertexDeclaration = vector<D3D11_INPUT_ELEMENT_DESC>(numVertexChannels);
		unsigned int alignedByteOffset = 0;
		unsigned int i = 0;
		vertexDeclaration[i++] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
		alignedByteOffset += 3 * sizeof(float);
		if (shaderConfiguration.HasBones())
		{
			vertexDeclaration[i++] = { "BLENDWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 4 * sizeof(float);
			vertexDeclaration[i++] = { "BLENDINDICES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, alignedByteOffset, D3D11_INPUT_PER_VERTEX_DATA, 0 };
			alignedByteOffset += 4 * sizeof(unsigned int);
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateInputLayout(
				vertexDeclaration.data(),
				numVertexChannels,
				(byte*)compiledCode->GetBufferPointer(),
				compiledCode->GetBufferSize(),
				&inputLayout)
			);

		// Add the newly created input layout to the container.
		// The container will take care of the freeing the memory.
		AddInputLayout(key, inputLayout, vertexStride);
	});

	// Wait for the task to complete (kinda makes it synchronous)
	createVSTask.wait();
}

void ResourcePool::CreatePixelShader(const wstring& key, const PixelShaderConfiguration shaderConfiguration)
{
	// If the container already contains an element with the same key do not create it again
	// If the key is an empty string do not attempt to create anything.
	if (key.empty() || m_pixelShaders.count(key))
		return;

	// Load shader asynchronously.
	auto loadPSTask = DX::ReadTextAsyncA(L"COG\\TemplatedShaders\\tmpl_PS.hlsl");

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this, &key, &shaderConfiguration](string& fileData) {	// parameter is not 'const', because it will be edited before compilation
		// The pixel shader object itself
		ID3D11PixelShader* pixelShader;
		// These blobs are necessary for the D3DCompiler methods
		ID3DBlob* compiledCode;
		ID3DBlob* errorMessage;
		// A variable used for multi-'texture channel' vertices and multi-'vertex-color channel' vertices
		unsigned int k = 0;
		// string position indicators for analyzing the shader source code.
		size_t offerBegin = 0;
		size_t lineBreak = 0;
		string offer;
		// vectors containing the features offered by the shader.
		vector<string> buffers;
		vector<string> offeredFeatures;
		// A vector containing the preprocessor macros used for compiling the final shader.
		vector<D3D_SHADER_MACRO> macros;

		// Reserve space for the strings use by D3D_SHADER_MACRO objects.
		// This is necessary, because D3D_SHADER_MACRO stores constant char pointers, which might get invalidated due to a reallocation.
		buffers.reserve(64);
		// Collect the offered features
		while ((offerBegin = fileData.find("COG_OFFER_", offerBegin + 1)) != std::string::npos)
		{
			lineBreak = fileData.find("\r\n", offerBegin + 11);
			offer = fileData.substr(offerBegin + 10, lineBreak - (offerBegin + 10));
			offeredFeatures.push_back(offer);
		}
		// Create the necessary macros
		const bool shadowMappingEnabled = m_shadowMappingEnabled && shaderConfiguration.usesShadowMap && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SHADOW_MAP") != offeredFeatures.cend();
		if (shadowMappingEnabled)
			macros.push_back(COG_HLSL_SHADOW_MAP);

		if (shaderConfiguration.isTimeVariant && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TIME_VARIANT") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_TIME_VARIANT);

		if (shaderConfiguration.hasNormal && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "NORMAL") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_NORMAL);
		if (shaderConfiguration.hasTangentBitangent && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TANGENT_AND_BINORMAL") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_TANGENT_AND_BINORMAL);
		while (k < shaderConfiguration.numTexChannel && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TEXCOORD_" + std::to_string(k)) != offeredFeatures.cend())
		{
			buffers.push_back("COG_HAS_TEXCOORD_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_TEXCOORD(buffers.back().data()));
			k++;
		}
		k = 0;
		while (k < shaderConfiguration.numVertColorChannel && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "VERTEX_COLOR_" + std::to_string(k)) != offeredFeatures.cend())
		{
			buffers.push_back("COG_HAS_VERTEX_COLOR_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_VERTEX_COLOR(buffers.back().data()));
			k++;
		}

		// Create sampler definitions and sampler-related macros
		list<string> samplerDeclarations;
		for (const auto& sampler : shaderConfiguration.samplers)
		{
			samplerDeclarations.push_back(sampler.samplerName);
		}
		samplerDeclarations.sort();
		samplerDeclarations.unique();
		if (shadowMappingEnabled && std::find(samplerDeclarations.cbegin(), samplerDeclarations.cend(), "shadowSampler") == samplerDeclarations.cend())
			samplerDeclarations.push_front("shadowSampler");	// It is important to place it in the front. This way it will be on sampler slot 0.
		for (const auto& sampler : shaderConfiguration.samplers)
		{
			if (std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), Stringify(sampler.type) + "_MAP") == offeredFeatures.cend())
				throw runtime_error("You attempted to use a texture type which is not supported by the current shader template. Check if your shader template code contains COG_OFFER_" + Stringify(sampler.type) + "_MAP!");
			buffers.push_back("COG_" + Stringify(sampler.type) + "_SAMPLER_NAME");
			buffers.push_back("COG_" + Stringify(sampler.type) + "_TEX_CHANNEL");
			buffers.emplace_back("tex" + std::to_string(sampler.texChannel));
			size_t index = buffers.size() - 3;
			macros.push_back(D3D_SHADER_MACRO{ buffers[index].data(), sampler.samplerName.data() });
			macros.push_back(D3D_SHADER_MACRO{ buffers[index + 1].data(), buffers[index + 2].data() });
		}
		string ss;	// should be std::stringstream but that does not compile. TODO: Check out why!
		k = 0;
		for (const auto& declaredName : samplerDeclarations)
		{
			ss.append("SamplerState " + declaredName + ": register(s" + to_string(k) + ");\n");
			k++;
		}
		buffers.push_back(Stringify(shaderConfiguration.lightType));
		macros.push_back({ "COG_LIGHT_TYPE", buffers.back().data() });
		if (shaderConfiguration.useSpecularHighlight)
			macros.push_back(COG_HLSL_USE_SPECULAR_HIGHLIGHTS);
		if (shaderConfiguration.lightHasNonDefaultColor)
			macros.push_back(COG_HLSL_LIGHT_HAS_COLOR);
		if (shaderConfiguration.material.HasColor())
			macros.push_back(COG_HLSL_MATERIAL_HAS_COLOR);
		if (shaderConfiguration.material.HasShininess())
			macros.push_back(COG_HLSL_MATERIAL_HAS_SHININESS);
		if (shaderConfiguration.material.HasOpacity())
			macros.push_back(COG_HLSL_MATERIAL_HAS_OPACITY);

		const auto featureLevel = m_deviceResources->GetDeviceFeatureLevel();
		buffers.push_back(to_string(featureLevel));
		macros.push_back({ "COG_FEATURE_LEVEL", buffers.back().data() });
		macros.push_back(D3D_SHADER_MACRO{ nullptr, nullptr });
		fileData.replace(fileData.find("COG_PLACEHOLDER_SAMPLERS\r\n"), integral_constant<size_t, sizeof("COG_PLACEHOLDER_SAMPLERS\r")>::value, ss);

		UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		flags |= D3DCOMPILE_DEBUG;
#endif

		D3DCompile(
			fileData.c_str(),
			fileData.length(),
			nullptr,
			macros.data(),
			nullptr,
			"main",
			featureLevel >= D3D_FEATURE_LEVEL_11_0 ? "ps_5_0" : "ps_4_0",
			flags,
			0,
			&compiledCode,
			&errorMessage
		);

		if (errorMessage != nullptr)
		{
			const size_t siz = errorMessage->GetBufferSize();
			const std::string shaderErrorMsg{ (const char*)errorMessage->GetBufferPointer() };
			throw std::runtime_error{ "Error while compiling the pixel shader! Details: " + shaderErrorMsg };
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				(byte*)compiledCode->GetBufferPointer(),
				compiledCode->GetBufferSize(),
				nullptr,
				&pixelShader
			)
		);

		// Add the newly created pixel shader to the container.
		// The container will take care of the freeing the memory.
		AddPixelShader(key, pixelShader);
	});

	// Wait for the task to complete (kinda makes it synchronous)
	createPSTask.wait();
}

void ResourcePool::CreatePixelShader(const wstring&& key, const PixelShaderConfiguration shaderConfiguration)
{
	// If the container already contains an element with the same key do not create it again
	// If the key is an empty string do not attempt to create anything.
	if (key.empty() || m_pixelShaders.count(key))
		return;

	// Load shader asynchronously.
	auto loadPSTask = DX::ReadTextAsyncA(L"COG\\TemplatedShaders\\tmpl_PS.hlsl");

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this, &key, &shaderConfiguration](string& fileData) {	// parameter is not 'const', because it will be edited before compilation
		// The pixel shader object itself
		ID3D11PixelShader* pixelShader;
		// These blobs are necessary for the D3DCompiler methods
		ID3DBlob* compiledCode;
		ID3DBlob* errorMessage;
		// A variable used for multi-'texture channel' vertices and multi-'vertex-color channel' vertices
		unsigned int k = 0;
		// string position indicators for analyzing the shader source code.
		size_t offerBegin = 0;
		size_t lineBreak = 0;
		string offer;
		// vectors containing the features offered by the shader.
		vector<string> buffers;
		vector<string> offeredFeatures;
		// A vector containing the preprocessor macros used for compiling the final shader.
		vector<D3D_SHADER_MACRO> macros;

		// Reserve space for the strings use by D3D_SHADER_MACRO objects.
		// This is necessary, because D3D_SHADER_MACRO stores constant char pointers, which might get invalidated due to a reallocation.
		buffers.reserve(64);
		// Collect the offered features
		while ((offerBegin = fileData.find("COG_OFFER_", offerBegin + 1)) != std::string::npos)
		{
			lineBreak = fileData.find("\r\n", offerBegin + 11);
			offer = fileData.substr(offerBegin + 10, lineBreak - (offerBegin + 10));
			offeredFeatures.push_back(offer);
		}
		// Create the necessary macros
		const bool shadowMappingEnabled = m_shadowMappingEnabled && shaderConfiguration.usesShadowMap && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "SHADOW_MAP") != offeredFeatures.cend();
		if (shadowMappingEnabled)
			macros.push_back(COG_HLSL_SHADOW_MAP);

		if (shaderConfiguration.isTimeVariant && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TIME_VARIANT") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_TIME_VARIANT);

		if (shaderConfiguration.hasNormal && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "NORMAL") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_NORMAL);
		if (shaderConfiguration.hasTangentBitangent && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TANGENT_AND_BINORMAL") != offeredFeatures.cend())
			macros.push_back(COG_HLSL_HAS_TANGENT_AND_BINORMAL);
		while (k < shaderConfiguration.numTexChannel && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "TEXCOORD_" + std::to_string(k)) != offeredFeatures.cend())
		{
			buffers.push_back("COG_HAS_TEXCOORD_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_TEXCOORD(buffers.back().data()));
			k++;
		}
		k = 0;
		while (k < shaderConfiguration.numVertColorChannel && std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), "VERTEX_COLOR_" + std::to_string(k)) != offeredFeatures.cend())
		{
			buffers.push_back("COG_HAS_VERTEX_COLOR_" + std::to_string(k));
			macros.push_back(COG_HLSL_HAS_VERTEX_COLOR(buffers.back().data()));
			k++;
		}

		// Create sampler definitions and sampler-related macros
		list<string> samplerDeclarations;
		for (const auto& sampler : shaderConfiguration.samplers)
		{
			samplerDeclarations.push_back(sampler.samplerName);
		}
		samplerDeclarations.sort();
		samplerDeclarations.unique();
		if (shadowMappingEnabled && std::find(samplerDeclarations.cbegin(), samplerDeclarations.cend(), "shadowSampler") == samplerDeclarations.cend())
			samplerDeclarations.push_front("shadowSampler");	// It is important to place it in the front. This way it will be on sampler slot 0.
		for (const auto& sampler : shaderConfiguration.samplers)
		{
			if (std::find(offeredFeatures.cbegin(), offeredFeatures.cend(), Stringify(sampler.type) + "_MAP") == offeredFeatures.cend())
				throw runtime_error("You attempted to use a texture type which is not supported by the current shader template. Check if your shader template code contains COG_OFFER_" + Stringify(sampler.type) + "_MAP!");
			buffers.push_back("COG_" + Stringify(sampler.type) + "_SAMPLER_NAME");
			buffers.push_back("COG_" + Stringify(sampler.type) + "_TEX_CHANNEL");
			buffers.emplace_back("tex" + std::to_string(sampler.texChannel));
			size_t index = buffers.size() - 3;
			macros.push_back(D3D_SHADER_MACRO{ buffers[index].data(), sampler.samplerName.data() });
			macros.push_back(D3D_SHADER_MACRO{ buffers[index + 1].data(), buffers[index + 2].data() });
		}
		string ss;	// should be std::stringstream but that does not compile. TODO: Check out why!
		k = 0;
		for (const auto& declaredName : samplerDeclarations)
		{
			ss.append("SamplerState " + declaredName + ": register(s" + to_string(k) + ");\n");
			k++;
		}
		buffers.push_back(Stringify(shaderConfiguration.lightType));
		macros.push_back({ "COG_LIGHT_TYPE", buffers.back().data() });
		if (shaderConfiguration.useSpecularHighlight)
			macros.push_back(COG_HLSL_USE_SPECULAR_HIGHLIGHTS);
		if (shaderConfiguration.lightHasNonDefaultColor)
			macros.push_back(COG_HLSL_LIGHT_HAS_COLOR);
		if (shaderConfiguration.material.HasColor())
			macros.push_back(COG_HLSL_MATERIAL_HAS_COLOR);
		if (shaderConfiguration.material.HasShininess())
			macros.push_back(COG_HLSL_MATERIAL_HAS_SHININESS);
		if (shaderConfiguration.material.HasOpacity())
			macros.push_back(COG_HLSL_MATERIAL_HAS_OPACITY);

		const auto featureLevel = m_deviceResources->GetDeviceFeatureLevel();
		buffers.push_back(to_string(featureLevel));
		macros.push_back({ "COG_FEATURE_LEVEL", buffers.back().data() });
		macros.push_back(D3D_SHADER_MACRO{ nullptr, nullptr });
		fileData.replace(fileData.find("COG_PLACEHOLDER_SAMPLERS\r\n"), integral_constant<size_t, sizeof("COG_PLACEHOLDER_SAMPLERS\r")>::value, ss);

		UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
		flags |= D3DCOMPILE_DEBUG;
#endif

		D3DCompile(
			fileData.c_str(),
			fileData.length(),
			nullptr,
			macros.data(),
			nullptr,
			"main",
			featureLevel >= D3D_FEATURE_LEVEL_11_0 ? "ps_5_0" : "ps_4_0",
			flags,
			0,
			&compiledCode,
			&errorMessage
		);

		if (errorMessage != nullptr)
		{
			const size_t siz = errorMessage->GetBufferSize();
			const std::string shaderErrorMsg{ (const char*)errorMessage->GetBufferPointer() };
			throw std::runtime_error{ "Error while compiling the pixel shader! Details: " + shaderErrorMsg };
		}

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
			(byte*)compiledCode->GetBufferPointer(),
			compiledCode->GetBufferSize(),
			nullptr,
			&pixelShader
			)
			);

		// Add the newly created pixel shader to the container.
		// The container will take care of the freeing the memory.
		AddPixelShader(key, pixelShader);
	});

	// Wait for the task to complete (kinda makes it synchronous)
	createPSTask.wait();
}

void ResourcePool::CreateSamplerState(const aiTextureMapMode* addressModeUVW, string& key)
{
	key.clear();
	key = "LIN_";

	// Create a texture sampler state description.
	D3D11_SAMPLER_DESC samplerDesc;
	ID3D11SamplerState* samplerState;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	switch (addressModeUVW[0])
	{
	case aiTextureMapMode_Wrap:
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		key.append("WRAP_");
		break;
	case aiTextureMapMode_Clamp:
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		key.append("CLAMP_");
		break;
	case aiTextureMapMode_Mirror:
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		key.append("MIRROR_");
		break;
	default:	// aiTextureMapMode_Decal:
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		key.append("BORDER_");
		break;
	}
	switch (addressModeUVW[1])
	{
	case aiTextureMapMode_Wrap:
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		key.append("WRAP_");
		break;
	case aiTextureMapMode_Clamp:
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		key.append("CLAMP_");
		break;
	case aiTextureMapMode_Mirror:
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		key.append("MIRROR_");
		break;
	default:	// aiTextureMapMode_Decal:
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		key.append("BORDER_");
		break;
	}
	switch (addressModeUVW[2])
	{
	case aiTextureMapMode_Wrap:
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		key.append("WRAP_");
		break;
	case aiTextureMapMode_Clamp:
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		key.append("CLAMP_");
		break;
	case aiTextureMapMode_Mirror:
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
		key.append("MIRROR_");
		break;
	default:	// aiTextureMapMode_Decal:
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
		key.append("BORDER_");
		break;
	}
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// If the container already contains an element with the same key do not create it again
	if (m_samplerStates.count(key))
		return;

	// Create the texture sampler state.
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateSamplerState(
		&samplerDesc,
		&samplerState
		)
		);

	// Add the newly created sampler state to the container.
	// The container will take care of the freeing the memory.
	AddSamplerState(key, samplerState);
}

void ResourcePool::AddInputLayout(const wstring& key, ID3D11InputLayout* il, const unsigned int vertexStride) {
	if (il != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11InputLayout> ptr(il);
		m_inputLayouts.insert(make_pair(key, ptr));
		m_vertexStrides.insert(make_pair(key, vertexStride));
	}
}

void ResourcePool::AddVertexShader(const wstring& key, ID3D11VertexShader* vs) {
	if (vs != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11VertexShader> ptr(vs);
		m_vertexShaders.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddPixelShader(const wstring& key, ID3D11PixelShader* ps) {
	if (ps != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11PixelShader> ptr(ps);
		m_pixelShaders.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddRasterizerState(const wstring& key, ID3D11RasterizerState1* rs)
{
	if (rs != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11RasterizerState1> ptr(rs);
		m_rasterizerStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddRasterizerState(const wstring&& key, ID3D11RasterizerState1* rs)
{
	if (rs != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11RasterizerState1> ptr(rs);
		m_rasterizerStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddDepthStencilState(const wstring& key, ID3D11DepthStencilState* dss)
{
	if (dss != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> ptr(dss);
		m_depthStencilStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddDepthStencilState(const wstring&& key, ID3D11DepthStencilState* dss)
{
	if (dss != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> ptr(dss);
		m_depthStencilStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddBlendState(const wstring&& key, ID3D11BlendState1* bs)
{
	if (bs != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11BlendState1> ptr(bs);
		m_blendStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddBlendState(const wstring& key, ID3D11BlendState1* bs)
{
	if (bs != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11BlendState1> ptr(bs);
		m_blendStates.insert(make_pair(key, ptr));
	}
}

void ResourcePool::AddSamplerState(const string& key, ID3D11SamplerState* ss) {
	if (ss != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11SamplerState> ptr(ss);
		m_samplerStates.insert(make_pair(key, ptr));
	}
}