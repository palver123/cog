#include "pch.h"
#include "NodeAnimation.h"
#include "COG/Mesh/Utils.hpp"

using namespace DirectX;
using namespace COG;

NodeAnimation::NodeAnimation(const aiNodeAnim* nodeAnim)
{
	unsigned int i;
	for (i = 0; i < nodeAnim->mNumPositionKeys; i++)
	{
		aiVectorKey aiKey = nodeAnim->mPositionKeys[i];
		m_translationKeys.push_back(AnimationKey{ aiKey.mTime, XMFLOAT4{ aiKey.mValue.x, aiKey.mValue.y, aiKey.mValue.z, 1.0f } });
	}
	for (i = 0; i < nodeAnim->mNumRotationKeys; i++)
	{
		aiQuatKey aiKey = nodeAnim->mRotationKeys[i];
		m_rotationKeys.push_back(AnimationKey{ aiKey.mTime, XMFLOAT4{ aiKey.mValue.x, aiKey.mValue.y, aiKey.mValue.z, aiKey.mValue.w } });
	}
	for (i = 0; i < nodeAnim->mNumScalingKeys; i++)
	{
		aiVectorKey aiKey = nodeAnim->mScalingKeys[i];
		m_scalingKeys.push_back(AnimationKey{ aiKey.mTime, XMFLOAT4{ aiKey.mValue.x, aiKey.mValue.y, aiKey.mValue.z, 1.0f } });
	}
}

XMMATRIX NodeAnimation::ApplyAnimation(const double animationTime) const
{
	XMVECTOR translation, rotation, scaling;
	double factor;

	// Interpolate the scaling keys
	if (m_scalingKeys.size() == 1)
		scaling = XMLoadFloat4(&m_scalingKeys[0].m_value);
	else
	{
#if defined(DEBUG) || defined(_DEBUG)
		const std::vector<AnimationKey>::const_iterator nextKey = std::upper_bound(m_scalingKeys.cbegin(), m_scalingKeys.cend(), AnimationKey(animationTime, DirectX::XMFLOAT4()));
#else
		const auto nextKey = std::upper_bound(m_scalingKeys.cbegin(), m_scalingKeys.cend(), AnimationKey{ animationTime, DirectX::XMFLOAT4{} });
#endif
		const auto currentKey = std::prev(nextKey);

		factor = (animationTime - currentKey->m_keyTime) / (nextKey->m_keyTime - currentKey->m_keyTime);
		// Uses linear interpolation
		// TODO: Allow the user to specify a custom interpolation method
		scaling = XMVectorLerp(
			XMLoadFloat4(&currentKey->m_value),
			XMLoadFloat4(&nextKey->m_value),
			(float)factor);
	}

	// Interpolate the rotation keys
	if (m_rotationKeys.size() == 1)
		rotation = XMLoadFloat4(&m_rotationKeys[0].m_value);
	else
	{
		const auto nextKey = std::upper_bound(m_rotationKeys.cbegin(), m_rotationKeys.cend(), AnimationKey{ animationTime, DirectX::XMFLOAT4{} });
		const auto currentKey = std::prev(nextKey);

		factor = (animationTime - currentKey->m_keyTime) / (nextKey->m_keyTime - currentKey->m_keyTime);
		// Uses linear interpolation
		// TODO: Allow the user to specify a custom interpolation method
		rotation = XMVectorLerp(
			XMLoadFloat4(&currentKey->m_value),
			XMLoadFloat4(&nextKey->m_value),
			(float)factor);
	}

	// Interpolate the translation keys
	if (m_translationKeys.size() == 1)
		translation = XMLoadFloat4(&m_translationKeys[0].m_value);
	else
	{
		const auto nextKey = std::upper_bound(m_translationKeys.cbegin(), m_translationKeys.cend(), AnimationKey{ animationTime, DirectX::XMFLOAT4{} });
		const auto currentKey = std::prev(nextKey);

		factor = (animationTime - currentKey->m_keyTime) / (nextKey->m_keyTime - currentKey->m_keyTime);
		// Uses linear interpolation
		// TODO: Allow the user to specify a custom interpolation method
		translation = XMVectorLerp(
			XMLoadFloat4(&currentKey->m_value),
			XMLoadFloat4(&nextKey->m_value),
			(float)factor);
	}

	// Combine the above transformations
	return XMMatrixTranspose(XMMatrixAffineTransformation(scaling, XMVectorZero(), rotation, translation));
}

BoneNode::BoneNode(const aiNode* node, const unsigned int maxNumOfBones, unsigned int& id, const ::std::list<aiNode*>& allBones)
{
	m_name = std::string{ node->mName.data };
	m_bindTransformation = ToXMFLOAT4X4(node->mTransformation);
	if (std::find(allBones.cbegin(), allBones.cend(), node) != allBones.cend())
	{
		// If the current bone IS in the global list of bones assign a valid index to it.
		// The global list of bones (allBones) contains all the nodes that were mentioned in an
		// animation or mesh skinning data, and only those. However, it is possible that a bone
		// in a hierarchy does not skin any meshes, nor participates in any of the animations.
		// Still we need to store them, because maybe one of its children does.
		m_boneID = id;
		id = id + 1;
	}
	else
	{
		// If the current bone IS NOT not in the global list of bones assign an invalid index to it.
		// If it was not mentioned in any of the animations or mesh skinnings, we will never attempt to use its ID,
		// therefore it is completely okay to invalidate it.
		m_boneID = maxNumOfBones;
	}
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		BoneNode childNode{ node->mChildren[i], maxNumOfBones, id, allBones };
		m_children.push_back(childNode);
	}
}

unsigned int BoneNode::GetBoneIDFromName(const ::std::string& name, const unsigned int maxNumOfBones) const
{
	if (m_name == name)
		return m_boneID;
	else
	{
		// every valid bone index should be between 0 and 'maxNumOfBones' - 1
		unsigned int retVal = maxNumOfBones;
		for (const auto& child : m_children)
		{
			if ((retVal = child.GetBoneIDFromName(name, maxNumOfBones)) < maxNumOfBones)
				// The bone was found in the current subtree
				return retVal;
		}
		// The bone was not found in the current subtree, return an invalid bone index
		return retVal;
	}
}