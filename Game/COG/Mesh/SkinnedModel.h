#pragma once

#include "COG/Mesh/Utils.hpp"
#include "COG/Mesh/Model.h"
#include "COG/Mesh/Animation.h"
#include "COG/HLSL_macros.h"

namespace COG
{
	class SkinnedModel : public Model
	{
	public:
		MatrixCollectionConstantBuffer	m_boneFinalTransforms;
		MatrixCollectionConstantBuffer	m_inverseBindPose;

		/// <summary> Constructs a new skinned model. </summary>
		/// <param name="scene">The aiScene that contains the model data.</param>
		/// <param name="deviceResources">The DX device object needed for GPU calls.</param>
		/// <param name="resourcePool">The resource pool object that handles DirectX resources globally.</param>
		SkinnedModel(
			const aiScene* scene,
			const std::shared_ptr<DX::DeviceResources> deviceResources,
			const std::shared_ptr<ResourcePool> resourcePool,
			const std::string modelName,
			const VertexShaderConfiguration& vs_conf,
			const PixelShaderConfiguration& ps_conf,
			const bool castsShadows = false,
			const bool receivesShadows = false);

		// Virtual methods
		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void Render() override;
		void GetDepth(bool shadowPass = false) override;
		void AddAnimation(const std::string& animationName);

		// Does not override Model::Update()
		void Update(DX::StepTimer const& timer, const DirectX::XMMATRIX& worldTransform);
	protected:
		// Unique to every model
		std::vector<Animation> m_animations;
		std::vector<BoneNode> m_rootNodes;
		std::list<unsigned int> m_animationQueue;
		unsigned int m_numBones;

		// Unique to every instance of model
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_boneTransformsConstantBuffer;
		double m_currentAnimationTime;

		// Methods
		void CreateVertexBuffer(const aiMesh* mesh, MeshData& mainMeshdata, MeshData& prepassMeshData) override;
		void ReadSkeletonHierarchy();
		void UpdateSkinning(const BoneNode& node, const DirectX::XMMATRIX& parentTransform);
	};
}