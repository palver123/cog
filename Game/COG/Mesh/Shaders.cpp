#include "pch.h"

#include "COG/Mesh/PixelShader.h"

using namespace COG;

void PixelShaderClass::Set(const std::shared_ptr<DX::DeviceResources>& deviceResources){
	deviceResources->GetD3DDeviceContext()->PSSetShader(
		m_shader.Get(),
		nullptr,
		0
		);

	if (m_configuration.isTimeVariant)
		m_resourcePool->SetConstantBuffer(L"appTime", PixelShader, 0);
	if (m_configuration.useSpecularHighlight)
		m_resourcePool->SetConstantBuffer(L"viewProj", PixelShader, 1);
	ID3D11Buffer* lightCB = m_constantBuffers.at(L"light").Get();
	auto context = m_deviceResources->GetD3DDeviceContext();

}