#pragma once

#include "assimp/vector3.h"
#include "assimp/quaternion.h"
#include "assimp/matrix4x4.h"
#include "assimp/material.h"

namespace COG{
	inline DirectX::XMFLOAT4X4 ToXMFLOAT4X4(const aiMatrix4x4& matrix)
	{
		return DirectX::XMFLOAT4X4{
		matrix.a1, matrix.a2, matrix.a3, matrix.a4,
		matrix.b1, matrix.b2, matrix.b3, matrix.b4,
		matrix.c1, matrix.c2, matrix.c3, matrix.c4,
		matrix.d1, matrix.d2, matrix.d3, matrix.d4
	};
	}

	inline aiMatrix4x4 ToAiMatrix4x4(const DirectX::XMFLOAT4X4& matrix)
	{
		return aiMatrix4x4{
		matrix._11, matrix._12, matrix._13, matrix._14,
		matrix._21, matrix._22, matrix._23, matrix._24,
		matrix._31, matrix._32, matrix._33, matrix._34,
		matrix._41, matrix._42, matrix._43, matrix._44
	};
	}

	inline DirectX::XMMATRIX ToXMMATRIX(const aiMatrix4x4& matrix)
	{
		DirectX::XMFLOAT4X4 temp = ToXMFLOAT4X4(matrix);
		return DirectX::XMLoadFloat4x4(&temp);
	}

	inline DirectX::XMVECTOR ToXMVECTOR(const aiVector3D& v)	{
		return DirectX::XMVectorSet(v.x, v.y, v.z, 1.0f);
	}

	inline DirectX::XMVECTOR ToXMVECTOR(const aiQuaternion q)	{
		return DirectX::XMVectorSet(q.x, q.y, q.z, q.w);
	}

	inline std::string ConstructSamplerName(const aiTextureMapMode* addressModeUVW)
	{
		std::string key = "LIN_";

		// Create a texture sampler state description.
		switch (addressModeUVW[0])
		{
		case aiTextureMapMode_Wrap:
			key.append("WRAP_");
			break;
		case aiTextureMapMode_Clamp:
			key.append("CLAMP_");
			break;
		case aiTextureMapMode_Mirror:
			key.append("MIRROR_");
			break;
		default:	// aiTextureMapMode_Decal:
			key.append("BORDER_");
			break;
		}
		switch (addressModeUVW[1])
		{
		case aiTextureMapMode_Wrap:
			key.append("WRAP_");
			break;
		case aiTextureMapMode_Clamp:
			key.append("CLAMP_");
			break;
		case aiTextureMapMode_Mirror:
			key.append("MIRROR_");
			break;
		default:	// aiTextureMapMode_Decal:
			key.append("BORDER_");
			break;
		}
		switch (addressModeUVW[2])
		{
		case aiTextureMapMode_Wrap:
			key.append("WRAP_");
			break;
		case aiTextureMapMode_Clamp:
			key.append("CLAMP_");
			break;
		case aiTextureMapMode_Mirror:
			key.append("MIRROR_");
			break;
		default:	// aiTextureMapMode_Decal:
			key.append("BORDER_");
			break;
		}

		return key;
	}
}
