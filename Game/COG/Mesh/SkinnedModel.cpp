#include "pch.h"
#include "SkinnedModel.h"
#include "COG/DDS/DDSTextureLoader.h"

using namespace std;
using namespace DirectX;
using namespace COG;

SkinnedModel::SkinnedModel(
	const aiScene* scene,
	const shared_ptr<DX::DeviceResources> deviceResources,
	const shared_ptr<ResourcePool> resourcePool,
	const string modelName,
	const VertexShaderConfiguration& vs_conf,
	const PixelShaderConfiguration& ps_conf,
	const bool castsShadows,
	const bool receivesShadows) :
	m_boneTransformsConstantBuffer{}
{
	m_currentAnimationTime = (double) rand() / RAND_MAX * 2.54;
	m_deviceResources = deviceResources;
	m_resourcePool = resourcePool;
	m_modelName = modelName;
	isShadowCaster = castsShadows;
	isShadowReceiver = receivesShadows;
	const auto& hashedModelData = Model::resources.find(modelName);

	if (hashedModelData == Model::resources.end())
	{
		ModelData modelData;
		modelData.m_scene = shared_ptr<const aiScene>(scene);
		modelData.m_loadingComplete = false;
		modelData.vs_shaderConfiguration = vs_conf;
		modelData.vs_shaderConfiguration.usesTessellation = false;
		modelData.ps_shaderConfiguration = ps_conf;
		/*		for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
		{
		if (scene->mMaterials[i]->GetTextureCount(aiTextureType_DISPLACEMENT) > 0)
		modelData.m_usesTessellation = true;
		}*/
		Model::resources.insert(make_pair(m_modelName, modelData));
		ReadSkeletonHierarchy();
		CreateDeviceDependentResources();
	}
	else
	{
		if (vs_conf.hasNormalMatrix)
			if (vs_conf.hasWorldMatrix)
				m_matrices.matrices.resize(2);
			else
				m_matrices.matrices.resize(1);
		else if (vs_conf.hasWorldMatrix)
			m_matrices.matrices.resize(1);

		m_matrices.matrices.shrink_to_fit();
		if (vs_conf.hasWorldMatrix || vs_conf.hasNormalMatrix)
		{
			CD3D11_BUFFER_DESC constantBufferDesc(
				min(sizeof(XMFLOAT4X4) * (unsigned int)m_matrices.matrices.size(), D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT),
				D3D11_BIND_CONSTANT_BUFFER,
				D3D11_USAGE_DYNAMIC,
				D3D11_CPU_ACCESS_WRITE);
			DX::ThrowIfFailed(
				m_deviceResources->GetD3DDevice()->CreateBuffer(
					&constantBufferDesc,
					nullptr,
					m_perInstanceConstantBuffer_main.GetAddressOf()
				)
			);
			constantBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);
			DX::ThrowIfFailed(
				m_deviceResources->GetD3DDevice()->CreateBuffer(
					&constantBufferDesc,
					nullptr,
					m_perInstanceConstantBuffer_prepass.GetAddressOf()
				)
			);
		}

		ReadSkeletonHierarchy();
		if (m_numBones > 0)
		{
			// https://msdn.microsoft.com/en-us/library/windows/desktop/ff476092%28v=vs.85%29.aspx
			CD3D11_BUFFER_DESC boneTransformDesc(
				min(sizeof(XMFLOAT4X4)* m_numBones, D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT),
				D3D11_BIND_CONSTANT_BUFFER,
				D3D11_USAGE_DYNAMIC,
				D3D11_CPU_ACCESS_WRITE);
			DX::ThrowIfFailed(
				m_deviceResources->GetD3DDevice()->CreateBuffer(
					&boneTransformDesc,
					nullptr,
					m_boneTransformsConstantBuffer.GetAddressOf()
				)
			);
		}

		m_loadingComplete = true;
	}

	m_animationQueue.push_back(0);
}

void SkinnedModel::CreateDeviceDependentResources()
{
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ff476092%28v=vs.85%29.aspx
	CD3D11_BUFFER_DESC boneTransformDesc(
		min(sizeof(XMFLOAT4X4)* m_numBones, D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&boneTransformDesc,
			nullptr,
			m_boneTransformsConstantBuffer.GetAddressOf()
		)
	);

	Model::CreateDeviceDependentResources();
}

void SkinnedModel::ReleaseDeviceDependentResources()
{
	Model::ReleaseDeviceDependentResources();
	m_boneTransformsConstantBuffer.Reset();
}

void SkinnedModel::Update(DX::StepTimer const& timer, const XMMATRIX& worldTransform)
{
	Model::Update(timer);

	if (m_animationQueue.empty() || !Model::resources[m_modelName].m_loadingComplete || !m_loadingComplete || m_animations.empty())
		return;

	double ticksPerSecond = m_animations[m_animationQueue.front()].m_ticksPerSecond;
	double timeInTicks = timer.GetElapsedSeconds() * ticksPerSecond;
	m_currentAnimationTime += timeInTicks;
	if (m_currentAnimationTime > m_animations[m_animationQueue.front()].m_duration)
	{
		if (m_animations[m_animationQueue.front()].m_behaviour != B_REPEAT)
		{
			m_currentAnimationTime = m_animations[m_animationQueue.front()].m_duration;
			for (const auto& skeleton : m_rootNodes)
				UpdateSkinning(skeleton, worldTransform);
			m_animationQueue.pop_front();
			m_currentAnimationTime = 0.0;
		}
		else
		{
			m_currentAnimationTime -= m_animations[m_animationQueue.front()].m_duration;
			for (const auto& skeleton : m_rootNodes)
				UpdateSkinning(skeleton, worldTransform);
		}
	}
	else
	{
		for (const auto& skeleton : m_rootNodes)
			UpdateSkinning(skeleton, worldTransform);
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	if (m_boneTransformsConstantBuffer)
	{
		// Prepare the constant buffer to send it to the graphics device.
		static D3D11_MAPPED_SUBRESOURCE mappedResource;
		DX::ThrowIfFailed(
			context->Map(
			m_boneTransformsConstantBuffer.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
			)
			);

		memcpy(mappedResource.pData, m_boneFinalTransforms.matrices.data(), m_boneFinalTransforms.matrices.size() * sizeof(XMFLOAT4X4));

		context->Unmap(m_boneTransformsConstantBuffer.Get(), 0);
	}
}

void SkinnedModel::Render()
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
	{
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

//	m_resourcePool->SetRasterizerState(L"cull_none");
	if (!m_matrices.IsEmpty() && m_perInstanceConstantBuffer_main)
	{
		// Send the bone matrices constant buffer to the graphics device.
		m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_main.Get(), 4);
	}

	// Draw each mesh separately
	for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
	{
		const auto& meshData = modelData.m_mainMeshData[i];
		UINT stride = m_resourcePool->GetVertexStride(meshData.m_ILkey);
		UINT offset = 0;
		const aiMesh* mesh = modelData.m_scene->mMeshes[i];

		if (mesh->HasBones() && m_boneTransformsConstantBuffer)
		{
			// Send the bone matrices constant buffer to the graphics device.
			m_resourcePool->SetConstantBuffer<VertexShader>(m_boneTransformsConstantBuffer.Get(), 3);
		}

		if (meshData.m_psConfig.HasAdditionalMaterialParamsFlag())
		{
			// Send the material constant buffer to the graphics device.
			m_resourcePool->SetConstantBuffer<PixelShader>(meshData.m_perMaterialConstantBuffer.Get(), 2);
		}

		// Bind the mesh's vertex buffer to the vertex shader.
		context->IASetVertexBuffers(
			0,
			1,
			meshData.m_vertexBuffer.GetAddressOf(),
			&stride,
			&offset
		);

		context->IASetIndexBuffer(
			meshData.m_indexBuffer.Get(),
			DXGI_FORMAT_R16_UINT, // Each index is one 16-bit unsigned integer (short).
			0
		);

		switch (mesh->mPrimitiveTypes)
		{
		case aiPrimitiveType_POINT:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
		case aiPrimitiveType_LINE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			break;
		case aiPrimitiveType_TRIANGLE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		case aiPrimitiveType_POLYGON:
			// The assimp documentation says little about polygon primitives. Use triangulation...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		default:
			// This code should never be reached...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		}

		// Attach our input layout.
		m_resourcePool->SetInputLayout(meshData.m_ILkey);

		// Attach our vertex shader.
		m_resourcePool->SetVertexShader(meshData.m_VSkey);

		if (modelData.vs_shaderConfiguration.usesTessellation)
		{
			// Attach our hull shader.
			m_resourcePool->SetHullShader(meshData.m_HSkey);

			// Attach our domain shader.
			m_resourcePool->SetDomainShader(meshData.m_DSkey);
		}

		// Attach our pixel shader.
		m_resourcePool->SetPixelShader(meshData.m_PSkey);

		SetShaderTexturesAndSamplers(context, meshData.m_texturePointers, meshData.m_samplerKeys);

		// Draw the objects.
		context->DrawIndexed(
			mesh->mNumFaces * 3,
			0,
			0
		);

		m_resourcePool->ResetAvailableSlots();
	}
}

void SkinnedModel::GetDepth(bool shadowPass /*= false*/)
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
	{
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	//	m_resourcePool->SetRasterizerState(L"cull_none");
	if (!m_matrices.IsEmpty() && m_perInstanceConstantBuffer_prepass)
	{
		// Send the bone matrices constant buffer to the graphics device.
		m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_prepass.Get(), 4);
	}

	// Draw each mesh separately
	for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
	{
		const auto& meshData = modelData.m_prepassMeshData[i];
		UINT stride = m_resourcePool->GetVertexStride(meshData.m_ILkey);
		UINT offset = 0;
		const aiMesh* mesh = modelData.m_scene->mMeshes[i];

		if (mesh->HasBones() && m_boneTransformsConstantBuffer)
		{
			// Send the bone matrices constant buffer to the graphics device.
			m_resourcePool->SetConstantBuffer<VertexShader>(m_boneTransformsConstantBuffer.Get(), 3);
		}

		// Bind the mesh's vertex buffer to the vertex shader.
		context->IASetVertexBuffers(
			0,
			1,
			meshData.m_vertexBuffer.GetAddressOf(),
			&stride,
			&offset
		);

		context->IASetIndexBuffer(
			meshData.m_indexBuffer.Get(),
			DXGI_FORMAT_R16_UINT, // Each index is one 16-bit unsigned integer (short).
			0
		);

		switch (mesh->mPrimitiveTypes)
		{
		case aiPrimitiveType_POINT:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
		case aiPrimitiveType_LINE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			break;
		case aiPrimitiveType_TRIANGLE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		case aiPrimitiveType_POLYGON:
			// The assimp documentation says little about polygon primitives. Use triangulation...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		default:
			// This code should never be reached...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		}

		// Attach our input layout.
		m_resourcePool->SetInputLayout(meshData.m_ILkey);

		// Attach our vertex shader.
		m_resourcePool->SetVertexShader(shadowPass ? meshData.m_VSkey_shadow : meshData.m_VSkey);

		// Attach our pixel shader.
		m_resourcePool->SetPixelShader(meshData.m_PSkey);

		// Draw the objects.
		context->DrawIndexed(
			mesh->mNumFaces * 3,
			0,
			0
		);

		m_resourcePool->ResetAvailableSlots();
	}
}

void SkinnedModel::CreateVertexBuffer(const aiMesh* mesh, MeshData& mainMeshData, MeshData& prepassMeshData)
{
	// Vertex structure size in bytes
	unsigned int mainVertexStride = m_resourcePool->GetVertexStride(mainMeshData.m_ILkey);
	unsigned int prepassVertexStride = m_resourcePool->GetVertexStride(prepassMeshData.m_ILkey);
	// Offset inside the vertex structure in bytes for the current vertex
	unsigned int mainOffsetInsideVertexStructure = 0;
	unsigned int prepassOffsetInsideVertexStructure = 0;
	// A variable used for multi-texture channel vertices and multi-vertex-color channel vertices
	unsigned int j, k = 0;
	// The buffer that containes all the vertex data, which might be heterogenous
	// Can contain floats and unsigned ints (for the bone indices in the skinning data).
	// The exact structure of a vertex is determined in runtime.
	vector<char> mainVertices(mesh->mNumVertices * mainVertexStride);			// TODO: change this to an vector for RAII purposes.
	vector<char> prepassVertices(mesh->mNumVertices * prepassVertexStride);	// TODO: change this to an vector for RAII purposes.
	static_assert(sizeof(aiVector3D) == 3 * sizeof(float), "Assimp aiVector3D's size must be 3 times the size of float. If that's not true, use a memcpy for each vector component's copy instead of copying the whole structure!");
	static_assert(sizeof(aiColor4D) == 4 * sizeof(float), "Assimp aiColor4D's size must be 4 times the size of float. If that's not true, use a memcpy for each vector component's copy instead of copying the whole structure!");

	// Each vertex can have any number of bone weights associated to it in the model file.
	// My built-in shaders only accept 4 bone weights/indices per vertex. 
	// Therefore only the 4 'most influential' bones are kept.
	// Their weights are normalized so that their sum = 1.0f
	// These bone weights are stored in ascending order on the CPU side, descending order on GPU side
	vector<XMFLOAT4> boneWeights;
	vector<XMUINT4> boneIndices;
	// Pre-allocate memory for the data just like with dynamic arrays, except vector takes care of memory deallocation automatically
	// (even if there is an exception before the end of the scope).
	boneWeights.reserve(mesh->mNumVertices);
	boneIndices.reserve(mesh->mNumVertices);

	for (j = 0; j < mesh->mNumVertices; j++)
	{
		// Initialize bone weights to zero - no influence -
		boneWeights.push_back({ 0.0f, 0.0f, 0.0f, 0.0f });
		boneIndices.push_back({ 0, 0, 0, 0 });
	}

	// Import skinning data of vertices
	for (j = 0; j < mesh->mNumBones; j++)
	{
		const aiBone* bone = mesh->mBones[j];
		unsigned int boneID = m_numBones;
		for (const auto& node : m_rootNodes)
		{
			if ((boneID = node.GetBoneIDFromName(bone->mName.data, m_numBones)) < m_numBones)
				break;
		}

		if (boneID == m_numBones)
		{
			char errorMessage[256];
			sprintf_s(errorMessage, 256, "The bone named %s was not found in the bone hierarchy of the model: %s", bone->mName.data, m_modelName);
			throw runtime_error(errorMessage);
		}

		// Maybe it is not the first time we save the inverse bind pose for this particular bone,
		// but overwriting it a couple of times does not hurt.
		m_inverseBindPose[boneID] = ToXMFLOAT4X4(bone->mOffsetMatrix);

		// Filtering out the 4 most influential bones for every vertex
		for (unsigned int p = 0; p < bone->mNumWeights; p++)
		{
			aiVertexWeight weight = bone->mWeights[p];
			if (boneWeights[weight.mVertexId].w < weight.mWeight)
			{
				if (boneWeights[weight.mVertexId].z < weight.mWeight)
				{
					if (boneWeights[weight.mVertexId].y < weight.mWeight)
					{
						if (boneWeights[weight.mVertexId].x < weight.mWeight)
						{
							// Shift newWeight -> x -> y -> z -> w
							boneWeights[weight.mVertexId].w = boneWeights[weight.mVertexId].z;
							boneWeights[weight.mVertexId].z = boneWeights[weight.mVertexId].y;
							boneWeights[weight.mVertexId].y = boneWeights[weight.mVertexId].x;
							boneWeights[weight.mVertexId].x = weight.mWeight;
							boneIndices[weight.mVertexId].w = boneIndices[weight.mVertexId].z;
							boneIndices[weight.mVertexId].z = boneIndices[weight.mVertexId].y;
							boneIndices[weight.mVertexId].y = boneIndices[weight.mVertexId].x;
							boneIndices[weight.mVertexId].x = boneID;
						}
						else
						{
							// Shift newWeight -> y -> z -> w
							boneWeights[weight.mVertexId].w = boneWeights[weight.mVertexId].z;
							boneWeights[weight.mVertexId].z = boneWeights[weight.mVertexId].y;
							boneWeights[weight.mVertexId].y = weight.mWeight;
							boneIndices[weight.mVertexId].w = boneIndices[weight.mVertexId].z;
							boneIndices[weight.mVertexId].z = boneIndices[weight.mVertexId].y;
							boneIndices[weight.mVertexId].y = boneID;
						}
					}
					else
					{
						// Shift newWeight -> z -> w
						boneWeights[weight.mVertexId].w = boneWeights[weight.mVertexId].z;
						boneWeights[weight.mVertexId].z = weight.mWeight;
						boneIndices[weight.mVertexId].w = boneIndices[weight.mVertexId].z;
						boneIndices[weight.mVertexId].z = boneID;
					}
				}
				else
				{
					// Shift newWeight -> w
					boneWeights[weight.mVertexId].w = weight.mWeight;
					boneIndices[weight.mVertexId].w = boneID;
				}
			}
		}
	}

	// Iterate through every vertex and import the data from the model file
	for (j = 0; j < mesh->mNumVertices; j++, mainOffsetInsideVertexStructure = 0, prepassOffsetInsideVertexStructure = 0, k = 0)
	{
		// Address of the j-th vertex is 'vertices + j * vertexStride'
		if (mesh->HasPositions())
		{
			memcpy(&mainVertices[j * mainVertexStride], &mesh->mVertices[j], sizeof(float)* 3);
			memcpy(&prepassVertices[j * prepassVertexStride], &mesh->mVertices[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
			prepassOffsetInsideVertexStructure += sizeof(float)* 3;
		}
		if (mesh->HasNormals())
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mNormals[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
		}
		if (mesh->HasTangentsAndBitangents())
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mTangents[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mBitangents[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
		}
		if (mesh->HasBones())
		{
			// boneWeights[j] contains the 4 'most influential' bone weights for the j-th vertex.
			// These weights have to be normalized.
			float sum = boneWeights[j].x + boneWeights[j].y + boneWeights[j].z + boneWeights[j].w;
			XMStoreFloat4(&boneWeights[j], XMLoadFloat4(&boneWeights[j]) * (1.0f / sum));
			// bone weights are stored in descending order, and sent to the GPU in descending order
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &boneWeights[j], sizeof(float)* 4);
			mainOffsetInsideVertexStructure += sizeof(float)* 4;
			// boneIndices[j] contains the 4 'most influential' bones' index for the j-th vertex.
			// bone indices are stored in descending order, and sent to the GPU in descending order
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &boneIndices[j], sizeof(unsigned int)* 4);
			mainOffsetInsideVertexStructure += sizeof(unsigned int)* 4;

			memcpy(&prepassVertices[j * prepassVertexStride + prepassOffsetInsideVertexStructure], &boneWeights[j], sizeof(float)* 4);
			prepassOffsetInsideVertexStructure += sizeof(float) * 4;
			memcpy(&prepassVertices[j * prepassVertexStride + prepassOffsetInsideVertexStructure], &boneIndices[j], sizeof(unsigned int)* 4);
			prepassOffsetInsideVertexStructure += sizeof(unsigned int) * 4;
		}
		while (mesh->HasTextureCoords(k++) && k < 7)
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mTextureCoords[k - 1][j], sizeof(float)* 2);
			mainOffsetInsideVertexStructure += sizeof(float)* 2;
		}
		k = 0;
		while (mesh->HasVertexColors(k++) && k < 7)
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mColors[k - 1][j], sizeof(float)* 4);
			mainOffsetInsideVertexStructure += sizeof(float)* 4;
		}
	}

	// Create the vertex buffer
	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = mainVertices.data();
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;
	CD3D11_BUFFER_DESC vertexBufferDesc(mesh->mNumVertices * mainVertexStride, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE);

	// Store the newly created vertex buffer in the mesh data
	// vector copies the pointer and converts it to a ComPtr using its converting constructor
	// ComPtr and vector take care of memory management
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			mainMeshData.m_vertexBuffer.GetAddressOf()
		)
	);

	// Now create the prepass vertex buffer too.
	vertexBufferData.pSysMem = prepassVertices.data();
	vertexBufferDesc.ByteWidth = mesh->mNumVertices * prepassVertexStride;
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			prepassMeshData.m_vertexBuffer.GetAddressOf()
		)
	);
}

void SkinnedModel::ReadSkeletonHierarchy()
{
	// Collect all the nodes mentioned in one of the animations or meshes in a list.
	// Furthermore, only the nodes found here are treated as "bone nodes"
	list<aiNode*> boneNodes;
	ModelData& modelData = Model::resources[m_modelName];
	shared_ptr<const aiScene> scene = modelData.m_scene;
	for (unsigned int i = 0; i < scene->mNumAnimations; i++)
	{
		const aiAnimation* animation = scene->mAnimations[i];
		for (unsigned int j = 0; j < animation->mNumChannels; j++)
			boneNodes.push_back(scene->mRootNode->FindNode(animation->mChannels[j]->mNodeName.C_Str()));
	}
	for (unsigned int i = 0; i < scene->mNumMeshes; i++)
	{
		const aiMesh* mesh = scene->mMeshes[i];
		for (unsigned int j = 0; j < mesh->mNumBones; j++)
			boneNodes.push_back(scene->mRootNode->FindNode(mesh->mBones[j]->mName.C_Str()));
	}
	boneNodes.sort();
	boneNodes.unique();
	modelData.vs_shaderConfiguration.numberOfBones = (unsigned int)boneNodes.size();
	m_numBones = modelData.vs_shaderConfiguration.numberOfBones;
	m_boneFinalTransforms.ResizeWithIdentity(m_numBones);		// This resize will fill it with identity matrices
	m_inverseBindPose.ResizeWithZeros(m_numBones);	// This resize will fill it with zero matrices

	// Collect the root node of every skeleton hierarchy in the scene
	// An aiNode is a "skeleton hierarchy root node" IFF its parent node is not in the above bone list (or null)
	// Every descendant of a "skeleton hierarchy root node" is considered a "bone node".
	unsigned int boneCount = 0;
	for (const auto node : boneNodes)
	{
		if (node->mParent == nullptr || find(boneNodes.cbegin(), boneNodes.cend(), node->mParent) == boneNodes.cend())
		{
			BoneNode rootNode{ node, m_numBones, boneCount, boneNodes };
			XMStoreFloat4x4(&rootNode.m_bindTransformation,
				ToXMMATRIX(scene->mRootNode->mTransformation) *
				XMLoadFloat4x4(&rootNode.m_bindTransformation));
			m_rootNodes.push_back(rootNode);
		}
	}

	// Store all the animations.
	// Constructing the bone structure was necessary to do previously
	// because we need the ID of each bone for the bone channels in the animations
	for (unsigned int i = 0; i < scene->mNumAnimations; i++)
	{
		const aiAnimation* animation = scene->mAnimations[i];
		Animation my_anim = Animation{ animation, AnimationBehaviour::B_REPEAT };
		for (unsigned int j = 0; j < animation->mNumChannels; j++)
		{
			// Find the bone that corresponds to the current animation channel
			const aiNodeAnim* nodeAnim = animation->mChannels[j];
			unsigned int boneID = m_numBones;
			for (const auto& skeleton : m_rootNodes)
			{
				if ((boneID = skeleton.GetBoneIDFromName(nodeAnim->mNodeName.data, m_numBones)) < m_numBones)
					break;
			}
			// Construct a new NodeAnimation object and store it in the current animation
			my_anim.m_channels.insert(make_pair(boneID, NodeAnimation{ nodeAnim }));
		}
		m_animations.push_back(my_anim);
	}

	// Look up each and every bone and save the corresponding inverse bind transformation (aka bone offset matrix).
	for (unsigned int i = 0; i < scene->mNumMeshes; i++)
	{
		const aiMesh* mesh = scene->mMeshes[i];
		for (unsigned int j = 0; j < mesh->mNumBones; j++)
		{
			const aiBone* bone = mesh->mBones[j];
			unsigned int boneID = m_numBones;
			for (const auto& node : m_rootNodes)
			{
				if ((boneID = node.GetBoneIDFromName(bone->mName.data, m_numBones)) < m_numBones)
					break;
			}

			if (boneID == m_numBones)
			{
				char errorMessage[256];
				sprintf_s(errorMessage, 256, "The bone named %s was not found in the bone hierarchy of the model: %s.", bone->mName.data, m_modelName);
				throw runtime_error(errorMessage);
			}
			m_inverseBindPose[boneID] = ToXMFLOAT4X4(bone->mOffsetMatrix);
		}
	}
}

void SkinnedModel::UpdateSkinning(const BoneNode& node, const XMMATRIX& parentTransform)
{
	XMMATRIX globalTransform = parentTransform;
	if (node.m_boneID < m_numBones)
	{
		const Animation& nodeAnim = m_animations[m_animationQueue.front()];
		globalTransform *= nodeAnim.ApplyAnimation(node, m_currentAnimationTime);

		XMStoreFloat4x4(&m_boneFinalTransforms[node.m_boneID],
			globalTransform *
			XMLoadFloat4x4(&m_inverseBindPose[node.m_boneID]));
	}
	for (const auto& childNode : node.m_children)
		UpdateSkinning(childNode, globalTransform);
}