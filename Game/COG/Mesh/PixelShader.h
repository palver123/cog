#pragma once

#include "COG/ShaderConfiguration.h"
#include "COG/DirectXHelper.h"
#include <d3d11.h>

namespace COG{
	class PixelShaderClass final{
	public:
		PixelShaderClass(const ID3D11PixelShader* s, const PixelShaderConfiguration& c):
			m_shader{ s },
			m_configuration{c}
		{
		}

		void Set(const std::shared_ptr<DX::DeviceResources>& deviceResources);
	
	private:
		Microsoft::WRL::ComPtr<ID3D11PixelShader> m_shader;
		PixelShaderConfiguration m_configuration;
		std::shared_ptr<ResourcePool> m_resourcePool
	};
}