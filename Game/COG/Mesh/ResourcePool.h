#pragma once

#include <d3dcompiler.h>
#include "MeshDescription.h"
#include "assimp/material.h"

namespace COG
{
	/// <summary> A class that stores graphics resources in a hash set and effectively manages them through
	/// the lifetime of an application. </summary>
	class ResourcePool final
	{
	public:
		//static const std::wstring materialCB_key;
		static const std::wstring prepassPS_key;
//		static const std::wstring voidPS_key;
		static const std::string shadowSampler_key;
		static const unsigned int timeCB_slot;
		static const unsigned int camposCB_slot;
		static const unsigned int lightCB_slot;
		bool m_shadowMappingEnabled;

		/// <summary>
		/// Constructor. </summary>
		ResourcePool(
			const std::shared_ptr<DX::DeviceResources> deviceResources,
			bool shadowMappingEnabled);

		/// <summary> Adds an input layout object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the input layout. Passing an empty string does nothing.</param>
		/// <param name="il">The input layout to add. Passing NULL does nothing.</param>
		/// <param name="vertexStride">Vertex stride in bytes.</param>
		void AddInputLayout(const std::wstring& key, ID3D11InputLayout* il, const unsigned int vertexStride);
		
		/// <summary> Adds a vertex shader object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the vertex shader. Passing an empty string does nothing.</param>
		/// <param name="vs">The vertex shader to add. Passing NULL does nothing.</param>
		void AddVertexShader(const std::wstring& key, ID3D11VertexShader* vs);
		
		/// <summary> Adds a pixel shader object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the pixel shader. Passing an empty string does nothing.</param>
		/// <param name="ps">The pixel shader to add. Passing NULL does nothing.</param>
		void AddPixelShader(const std::wstring& key, ID3D11PixelShader* ps);

		/// <summary> Adds a rasterizer state object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the rasterizer state. Passing an empty string does nothing.</param>
		/// <param name="rs">The rasterizer state to add. Passing NULL does nothing.</param>
		void AddRasterizerState(const std::wstring& key, ID3D11RasterizerState1* rs);
		void AddRasterizerState(const std::wstring&& key, ID3D11RasterizerState1* rs);

		/// <summary> Adds a depth stencil state object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the depth stencil state. Passing an empty string does nothing.</param>
		/// <param name="dss">The depth stencil state to add. Passing NULL does nothing.</param>
		void AddDepthStencilState(const std::wstring& key, ID3D11DepthStencilState* dss);
		void AddDepthStencilState(const std::wstring&& key, ID3D11DepthStencilState* dss);

		/// <summary> Adds a blend state object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the blend state. Passing an empty string does nothing.</param>
		/// <param name="bs">The blend state to add. Passing NULL does nothing.</param>
		void AddBlendState(const std::wstring& key, ID3D11BlendState1* bs);
		void AddBlendState(const std::wstring&& key, ID3D11BlendState1* bs);

		/// <summary> Adds a sampler state object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		/// <param name="key">A key to identify the sampler state. Passing an empty string does nothing.</param>
		/// <param name="ss">The sampler state to add. Passing NULL does nothing.</param>
		void AddSamplerState(const std::string& key, ID3D11SamplerState* ss);

		void CreateDeviceDependentResources();
		void ReleaseDeviceDependentResources();
		void ResetAvailableSlots();
		void BindCommonResources();
		bool Ready() const { return m_loadingComplete; }

		/// <summary>
		/// Updates per frame data of the scene. This per frame data is the application time and the viewProj matrices. </summary>
		/// <param name="totalSeconds">The total seconds passed since the start of the application.</param>
		/// <param name="viewMatrix">The view matrix.</param>
		/// <param name="projMatrix">The projection matrix.</param>
		/// <param name="lightParams">Either the sun direction or the light position, depending on the type of the light used (directional or positional).</param>
		/// <param name="cameraPosition">The camera position.</param>
		void Update(const float totalSeconds);

		/// <summary> Returns the vertex stride in bytes. </summary>
		/// <remarks> If the key was not found, it throws an std:out_of_range exception. </remarks>
		/// <param name="key">A key to identify the input layout (which determines the vertex stride).</param>
		/// <returns>The vertex stride in bytes to the corresponding input layout.</returns>
		unsigned int GetVertexStride(const std::wstring& key) const {
			return m_vertexStrides.at(key);
		}

		///// <summary>
		///// Updates per frame data of the scene. This per frame data is the application time and the viewProj matrices. </summary>
		///// <param name="material">The structure containing the new parameters of a material.</param>
		//void UpdateMaterialBuffer(const MaterialConstantBuffer& material);

		/// <summary> A function that creates a basic vertex shader that can process the geometry data in a mesh </summary>
		/// <param name="mesh">A mesh that contains the geometry data which is neccessary for identifying the vertex structure.</param>
		/// <param name="key">An output parameter to identify the vertex shader.</param>
		/// <param name="shaderConfiguration">Everything that describes a vertex shader.</param>
		void CreateVertexShader(const MeshDescription& mesh, std::wstring& key, const VertexShaderConfiguration& shaderConfiguration = VertexShaderConfiguration());
		/// <summary> A function that creates a basic vertex shader that can process the geometry data in a mesh </summary>
		/// <param name="mesh">A mesh that contains the geometry data which is neccessary for identifying the vertex structure.</param>
		/// <param name="key">An output parameter to identify the vertex shader.</param>
		/// <param name="shaderConfiguration">Everything that describes a prepass vertex shader.</param>
		void CreateVertexShader(std::wstring& key, const PrepassVSConfiguration& shaderConfiguration);

		/// <summary> A function that creates a basic pixel shader that can process the geometry data in a mesh </summary>
		/// <param name="key">An input parameter to identify the pixel shader.</param>
		/// <param name="mesh">A mesh that contains the geometry data which is neccessary for identifying the vertex structure.</param>
		/// <param name="shaderConfiguration">Everything that describes a vertex shader.</param>
		void CreatePixelShader(const std::wstring& key, const PixelShaderConfiguration shaderConfiguration);
		/// <summary> A function that creates a basic pixel shader that can process the geometry data in a mesh </summary>
		/// <param name="key">An input parameter to identify the pixel shader.</param>
		/// <param name="mesh">A mesh that contains the geometry data which is neccessary for identifying the vertex structure.</param>
		/// <param name="shaderConfiguration">Everything that describes a vertex shader.</param>
		void CreatePixelShader(const std::wstring&& key, const PixelShaderConfiguration shaderConfiguration);

		/// <summary> A function that creates a basic sampler state, that uses
		/// linear filtering for minification, magnification and mip-level sampling. </summary>
		/// <param name="mapMode">Address mode for UVW coordinates</param>
		/// <param name="key">An output parameter used to identify the sampler state.</param>
		void CreateSamplerState(const aiTextureMapMode* mapModeUVW, std::string& key);
		
		/// <summary> Binds an input layout to the input assembler. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the input layout</param>
		void SetInputLayout(const std::wstring& key);

		/// <summary> Binds a vertex shader. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the vertex shader</param>
		void SetVertexShader(const std::wstring& key);

		/// <summary> Binds a hull shader. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the hull shader</param>
		void SetHullShader(const std::wstring& key);

		/// <summary> Binds a domain shader. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the domain shader</param>
		void SetDomainShader(const std::wstring& key);

		/// <summary> Binds a pixel shader. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the pixel shader</param>
		void SetPixelShader(const std::wstring& key);

		/// <summary> Binds a constant buffer to the specified slot if that's available.</summary>
		/// <param name="cb">The constant buffer to bind</param>
		/// <param name="inputSlot">The input slot.</param>
		/// <remarks>
		/// If the specified input slot is not avaiable this function throws an exception. Be careful!
		///	Only the specializations listed below are defined in the inline file (ResourcePool.inl).
		/// Other attempts to instantiate this template method should (and do) result in a compiler error.
		/// </remarks>
		template<PipelineStage stage>
		void SetConstantBuffer(ID3D11Buffer* const cb, const unsigned int inputSlot);

		template<>
		void SetConstantBuffer<VertexShader>(ID3D11Buffer* const cb, const unsigned int inputSlot)
		{
			if (m_availableVSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableVSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					VSSetConstantBuffers(inputSlot, 1, &cb);
			}
			else
				throw std::runtime_error("The specified input slot (" + std::to_string(inputSlot) + ") is not available for the vertex shader!");
		}

		template<>
		void SetConstantBuffer<HullShader>(ID3D11Buffer* const cb, const unsigned int inputSlot)
		{
			if (m_availableHSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableHSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					HSSetConstantBuffers(inputSlot, 1, &cb);
			}
			else
				throw std::runtime_error("The specified input slot (" + std::to_string(inputSlot) + ") is not available for the hull shader!");
		}

		template<>
		void SetConstantBuffer<DomainShader>(ID3D11Buffer* const cb, const unsigned int inputSlot)
		{
			if (m_availableDSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableDSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					DSSetConstantBuffers(inputSlot, 1, &cb);
			}
			else
				throw std::runtime_error("The specified input slot (" + std::to_string(inputSlot) + ") is not available for the domain shader!");
		}

		template<>
		void SetConstantBuffer<GeometryShader>(ID3D11Buffer* const cb, const unsigned int inputSlot)
		{
			if (m_availableGSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableGSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					GSSetConstantBuffers(inputSlot, 1, &cb);
			}
			else
				throw std::runtime_error("The specified input slot (" + std::to_string(inputSlot) + ") is not available for the geometry shader!");
		}

		template<>
		void SetConstantBuffer<PixelShader>(ID3D11Buffer* const cb, const unsigned int inputSlot)
		{
			if (m_availablePSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availablePSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					PSSetConstantBuffers(inputSlot, 1, &cb);
			}
			else
				throw std::runtime_error("The specified input slot (" + std::to_string(inputSlot) + ") is not available for the pixel shader!");
		}

		/// <summary> Binds a constant buffer.
		/// Automatically calculates which slot it should be attached to. </summary>
		/// <remarks>
		///	Only the specializations listed below are defined in the inline file (ResourcePool.inl).
		/// Other attempts to instantiate this template method should (and do) result in a compiler error.
		/// </remarks>
		/// <param name="cb">The constant buffer to bind</param>
		template<PipelineStage stage>
		void SetConstantBuffer(ID3D11Buffer* const cb);

		template<>
		void SetConstantBuffer<VertexShader>(ID3D11Buffer* const cb)
		{
			for (unsigned int inputSlot = 0; inputSlot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++inputSlot)
			if (m_availableVSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableVSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					VSSetConstantBuffers(inputSlot, 1, &cb);
				break;
			}
		}

		template<>
		void SetConstantBuffer<HullShader>(ID3D11Buffer* const cb)
		{
			for (unsigned int inputSlot = 0; inputSlot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++inputSlot)
			if (m_availableHSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableHSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					HSSetConstantBuffers(inputSlot, 1, &cb);
				break;
			}
		}

		template<>
		void SetConstantBuffer<DomainShader>(ID3D11Buffer* const cb)
		{
			for (unsigned int inputSlot = 0; inputSlot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++inputSlot)
			if (m_availableDSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableDSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					DSSetConstantBuffers(inputSlot, 1, &cb);
				break;
			}
		}

		template<>
		void SetConstantBuffer<GeometryShader>(ID3D11Buffer* const cb)
		{
			for (unsigned int inputSlot = 0; inputSlot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++inputSlot)
			if (m_availableGSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availableGSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					GSSetConstantBuffers(inputSlot, 1, &cb);
				break;
			}
		}

		template<>
		void SetConstantBuffer<PixelShader>(ID3D11Buffer* const cb)
		{
			for (unsigned int inputSlot = 0; inputSlot < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; ++inputSlot)
			if (m_availablePSConstantBufferSlots[inputSlot])	// Input slot available
			{
				m_availablePSConstantBufferSlots[inputSlot] = false;
				m_deviceResources->GetD3DDeviceContext()->
					PSSetConstantBuffers(inputSlot, 1, &cb);
				break;
			}
		}

		/// <summary> Binds a sampler state. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		///	If the key was not found this method might throw and exception!
		///	Only the specializations listed below are defined in the inline file (ResourcePool.inl).
		/// Other attempts to instantiate this template method should (and do) result in a compiler error.
		/// </remarks>
		/// <param name="key">A key used to identify the sampler state</param>
		template<PipelineStage stage>
		void SetSamplerState(const std::string& key);

		template<>
		void SetSamplerState<VertexShader>(const std::string& key)
		{
			if (m_lastVSSamplerStateKey != key)
			{
				for (unsigned int samplerSlot = 0; samplerSlot < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++samplerSlot)
				if (m_availableVSSamplerSlots[samplerSlot])
				{
					m_deviceResources->GetD3DDeviceContext()->VSSetSamplers(
						samplerSlot,
						1,
						m_samplerStates[key].GetAddressOf()
						);
					m_lastVSSamplerStateKey = key;
					m_availableVSSamplerSlots[samplerSlot] = false;
					break;
				}
			}
		}

		template<>
		void SetSamplerState<HullShader>(const std::string& key)
		{
			if (m_lastHSSamplerStateKey != key)
			{
				for (unsigned int samplerSlot = 0; samplerSlot < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++samplerSlot)
				if (m_availableHSSamplerSlots[samplerSlot])
				{
					m_deviceResources->GetD3DDeviceContext()->HSSetSamplers(
						samplerSlot,
						1,
						m_samplerStates[key].GetAddressOf()
						);
					m_lastHSSamplerStateKey = key;
					m_availableHSSamplerSlots[samplerSlot] = false;
					break;
				}
			}
		}

		template<>
		void SetSamplerState<DomainShader>(const std::string& key)
		{
			if (m_lastDSSamplerStateKey != key)
			{
				for (unsigned int samplerSlot = 0; samplerSlot < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++samplerSlot)
				if (m_availableDSSamplerSlots[samplerSlot])
				{
					m_deviceResources->GetD3DDeviceContext()->DSSetSamplers(
						samplerSlot,
						1,
						m_samplerStates[key].GetAddressOf()
						);
					m_lastDSSamplerStateKey = key;
					m_availableDSSamplerSlots[samplerSlot] = false;
					break;
				}
			}
		}

		template<>
		void SetSamplerState<GeometryShader>(const std::string& key)
		{
			if (m_lastGSSamplerStateKey != key)
			{
				for (unsigned int samplerSlot = 0; samplerSlot < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++samplerSlot)
				if (m_availableGSSamplerSlots[samplerSlot])
				{
					m_deviceResources->GetD3DDeviceContext()->GSSetSamplers(
						samplerSlot,
						1,
						m_samplerStates[key].GetAddressOf()
						);
					m_lastGSSamplerStateKey = key;
					m_availableGSSamplerSlots[samplerSlot] = false;
					break;
				}
			}
		}

		template<>
		void SetSamplerState<PixelShader>(const std::string& key)
		{
			if (m_lastPSSamplerStateKey != key)
			{
				for (unsigned int samplerSlot = 0; samplerSlot < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; ++samplerSlot)
				if (m_availablePSSamplerSlots[samplerSlot])
				{
					m_deviceResources->GetD3DDeviceContext()->PSSetSamplers(
						samplerSlot,
						1,
						m_samplerStates[key].GetAddressOf()
						);
					m_lastPSSamplerStateKey = key;
					m_availablePSSamplerSlots[samplerSlot] = false;
					break;
				}
			}
		}

		/// <summary> Binds a rasterizer state. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the rasterizer state</param>
		void SetRasterizerState(const std::wstring& key);

		/// <summary> Binds a depth stencil state. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the depth stencil state</param>
		void SetDepthStencilState(const std::wstring& key);
		
		/// <summary> Binds a blend state. </summary>
		/// <remarks> If the same object was the last object bound the function does nothing.
		/// If the key was not found this method might throw and exception! </remarks>
		/// <param name="key">A key used to identify the blend state</param>
		void SetBlendState(const std::wstring& key);

	private:
		/// <summary>
		/// Shared pointer for a DeviceResources instance to help with low-level programming.</summary>
		const std::shared_ptr<DX::DeviceResources> m_deviceResources;

		TimeConstantBuffer		m_applicationTime;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_timeConstantBuffer;
		bool m_loadingComplete;

#pragma region Resources
		/// <summary>
		/// A hashtable for the stored (pooled) vertex shader objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11VertexShader>>		m_vertexShaders;
		/// <summary>
		/// A hashtable for the stored (pooled) hull shader objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11HullShader>>			m_hullShaders;
		/// <summary>
		/// A hashtable for the stored (pooled) domain shader objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11DomainShader>>		m_domainShaders;
		/// <summary>
		/// A hashtable for the stored (pooled) geometry shader objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11GeometryShader>>		m_geometryShaders;
		/// <summary>
		/// A hashtable for the stored (pooled) pixel shader objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11PixelShader>>			m_pixelShaders;
		/// <summary>
		/// A hashtable for the stored (pooled) input layout objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11InputLayout>>			m_inputLayouts;
		/// <summary>
		/// A hashtable for the stored (pooled) sampler state objects.</summary>
		std::unordered_map<std::string, Microsoft::WRL::ComPtr<ID3D11SamplerState>>			m_samplerStates;
		/// <summary>
		/// A hashtable for the stored (pooled) rasterizer state objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11RasterizerState1>>	m_rasterizerStates;
		/// <summary>
		/// A hashtable for the stored (pooled) depth stencil state objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11DepthStencilState>>	m_depthStencilStates;
		/// <summary>
		/// A hashtable for the stored (pooled) blend state objects.</summary>
		std::unordered_map<std::wstring, Microsoft::WRL::ComPtr<ID3D11BlendState1>>			m_blendStates;
		/// <summary>
		/// A hashtable for the stored (pooled) vertex stride objects. Used along with input layout objects.</summary>
		std::unordered_map<std::wstring, unsigned int>										m_vertexStrides;
#pragma endregion

#pragma region Recent resource keys
		/// <summary>
		/// The key of the most recently used vertex shader. </summary>
		std::wstring m_lastVSKey;
		/// <summary>
		/// The key of the most recently used hull shader. </summary>
		std::wstring m_lastHSKey;
		/// <summary>
		/// The key of the most recently used domain shader. </summary>
		std::wstring m_lastDSKey;
		/// <summary>
		/// The key of the most recently used geometry shader. </summary>
		std::wstring m_lastGSKey;
		/// <summary>
		/// The key of the most recently used pixel shader. </summary>
		std::wstring m_lastPSKey;
		/// <summary>
		/// The key of the most recently used input layout. </summary>
		std::wstring m_lastInputLayoutKey;
		/// <summary>
		/// The key of the most recently used sampler state in a vertex shader. </summary>
		std::string m_lastVSSamplerStateKey;
		/// <summary>
		/// The key of the most recently used sampler state in a hull shader. </summary>
		std::string m_lastHSSamplerStateKey;
		/// <summary>
		/// The key of the most recently used sampler state in a domain shader. </summary>
		std::string m_lastDSSamplerStateKey;
		/// <summary>
		/// The key of the most recently used sampler state in a geometry shader. </summary>
		std::string m_lastGSSamplerStateKey;
		/// <summary>
		/// The key of the most recently used sampler state in a pixel shader. </summary>
		std::string m_lastPSSamplerStateKey;
		/// <summary>
		/// The key of the most recently used rasterizer state. </summary>
		std::wstring m_lastRasterizerStateKey;
		/// <summary>
		/// The key of the most recently used depth stencil state. </summary>
		std::wstring m_lastDepthStencilKey;
		/// <summary>
		/// The key of the most recently used blend state. </summary>
		std::wstring m_lastBlendStateKey;
#pragma endregion

#pragma region Resource slot state variables
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableVSSamplerSlots[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableHSSamplerSlots[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableDSSamplerSlots[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableGSSamplerSlots[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availablePSSamplerSlots[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableVSConstantBufferSlots[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableHSConstantBufferSlots[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableDSConstantBufferSlots[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availableGSConstantBufferSlots[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
		/// <summary>
		/// A lookup table for available sampler slots. </summary>
		/// <seealso cref="ResetAvailableSlots"/>
		bool m_availablePSConstantBufferSlots[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];
#pragma endregion
	};
}