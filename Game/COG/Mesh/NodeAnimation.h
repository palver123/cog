#pragma once

#include "assimp/scene.h"

namespace COG{
	struct BoneNode final{

		DirectX::XMFLOAT4X4 m_bindTransformation;
		unsigned int m_boneID;
		std::string m_name;
		std::vector<BoneNode> m_children;

		BoneNode(const aiNode* node, const unsigned int maxNumOfBones, unsigned int& id, const std::list<aiNode*>& allBones);

		unsigned int GetBoneIDFromName(const std::string& name, const unsigned int maxNumOfBones) const;
	};

	class NodeAnimation final
	{
	public:
		struct AnimationKey final{
			const double m_keyTime;
			const DirectX::XMFLOAT4 m_value;

#if defined(DEBUG) || defined(_DEBUG)
			// For some reason in debug mode this constructor is necessary in VS2013. Whatever...
			AnimationKey(double d, const DirectX::XMFLOAT4& f):
				m_keyTime(d),
				m_value(f)
			{ }
#endif

			bool operator<(const AnimationKey& other) const
			{
				return m_keyTime < other.m_keyTime;	// no epsilon. Should there be? Fuzzy comparison is tough business, needs further investigation. Probably the real solution is not to use floating point values as comparation keys...
			}
	};
	public:
		std::vector<AnimationKey> m_translationKeys;
		std::vector<AnimationKey> m_rotationKeys;
		std::vector<AnimationKey> m_scalingKeys;

		NodeAnimation() {};
		explicit NodeAnimation(const aiNodeAnim* nodeAnim);

		DirectX::XMMATRIX ApplyAnimation(const double animationTime) const;
	};
}