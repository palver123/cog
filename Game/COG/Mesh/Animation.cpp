#include "pch.h"
#include "Animation.h"

using namespace DirectX;
using namespace COG;

Animation::Animation(const aiAnimation* anim, AnimationBehaviour behaviour) : Animation{anim, anim->mName.data, behaviour } { }

Animation::Animation(const aiAnimation* anim, std::string name, AnimationBehaviour behaviour) : m_behaviour{behaviour}
{
	m_duration = anim->mDuration;
	m_ticksPerSecond = anim->mTicksPerSecond != 0.0 ? anim->mTicksPerSecond : 25.0;
	m_Name = name;
}

XMMATRIX Animation::ApplyAnimation(const BoneNode& node, const double animationTime) const
{
	auto nodeAnim = m_channels.find(node.m_boneID);
	if (nodeAnim != m_channels.end())
		return nodeAnim->second.ApplyAnimation(animationTime);
	else
		return XMLoadFloat4x4(&node.m_bindTransformation);
}