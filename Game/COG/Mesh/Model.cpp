#include "pch.h"
#include "Model.h"
#include <unordered_set>

#include "..\COG\DirectXHelper.h"
#include "..\COG\DDS\DDSTextureLoader.h"
#include "..\COG\Mesh\Utils.hpp"

using namespace std;
using namespace DirectX;
using namespace COG;

//////////////////////////////////////////////////////////////////////////
// Global and static variables
//////////////////////////////////////////////////////////////////////////
unordered_map<string, Model::ModelData> Model::resources;
const aiTextureType aiTextureTypes[] = {
	aiTextureType_DIFFUSE,
	aiTextureType_SPECULAR,
	aiTextureType_AMBIENT,
	aiTextureType_EMISSIVE,
	aiTextureType_HEIGHT,
	aiTextureType_NORMALS,
	aiTextureType_SHININESS,
	aiTextureType_OPACITY,
	aiTextureType_DISPLACEMENT,
	aiTextureType_LIGHTMAP,
	aiTextureType_REFLECTION
};
const map<aiTextureType, TextureType> cogTextureTypes = {
	pair<aiTextureType, TextureType> { aiTextureType_DIFFUSE, COG_Diffuse_map },
	pair<aiTextureType, TextureType> { aiTextureType_SPECULAR, COG_Specular_map },
	pair<aiTextureType, TextureType> { aiTextureType_AMBIENT, COG_Ambient_map },
	pair<aiTextureType, TextureType> { aiTextureType_EMISSIVE, COG_Emissive_map },
	pair<aiTextureType, TextureType> { aiTextureType_NORMALS, COG_Normals_map },
	pair<aiTextureType, TextureType> { aiTextureType_SHININESS, COG_Shininess_map },
	pair<aiTextureType, TextureType> { aiTextureType_OPACITY, COG_Opacity_map },
	pair<aiTextureType, TextureType> { aiTextureType_LIGHTMAP, COG_Light_map },
	pair<aiTextureType, TextureType> { aiTextureType_REFLECTION, COG_Reflection_map }
};

//////////////////////////////////////////////////////////////////////////
// ModelData
//////////////////////////////////////////////////////////////////////////
void Model::ModelData::CollectMaterialResourcesInMeshes(unsigned int meshIndex)
{
	aiString path;
	aiTextureMapMode mapModeUVW[3];
	aiTextureMapping mappingTechnique;
	unsigned int uvIndex;
	TextureType cogType;
	const auto mesh = m_scene->mMeshes[meshIndex];
	const aiMaterial *material = m_scene->mMaterials[mesh->mMaterialIndex];
	float materialProperty_float;
	aiColor3D materialProperty_color;

	// Manipulate the pixel shader configuration
	if (material->GetTextureCount(aiTextureType_AMBIENT) == 0)
	{
		if (AI_SUCCESS == material->Get<aiColor3D>(AI_MATKEY_COLOR_AMBIENT, materialProperty_color))
			m_mainMeshData[meshIndex].m_psConfig.defaultAmbient = XMFLOAT4{ materialProperty_color.r, materialProperty_color.g, materialProperty_color.b, 0.0f };
		else
			m_mainMeshData[meshIndex].m_psConfig.defaultAmbient = XMFLOAT4{ 0.1f, 0.1f, 0.1f, 0.0f };
	}
	if (material->GetTextureCount(aiTextureType_SHININESS) == 0 && AI_SUCCESS == material->Get<float>(AI_MATKEY_SHININESS, materialProperty_float))
		m_mainMeshData[meshIndex].m_psConfig.material.shininess = materialProperty_float;
	if (material->GetTextureCount(aiTextureType_OPACITY) == 0 && AI_SUCCESS == material->Get(AI_MATKEY_OPACITY, materialProperty_float))
		m_mainMeshData[meshIndex].m_psConfig.material.opacity = materialProperty_float;

	m_mainMeshData[meshIndex].m_texturePointers.reserve(D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);	// just to avoid unnecessary reallocations
	m_mainMeshData[meshIndex].m_samplerKeys.reserve(D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT);	// just to avoid unnecessary reallocations
	for (const auto aiType : aiTextureTypes)
	{
		const auto iterator = cogTextureTypes.find(aiType);
		if (iterator == cogTextureTypes.cend())
			continue;
		else
			cogType = iterator->second;
		for (unsigned int j = 0; j < material->GetTextureCount(aiType); j++)
		{
			material->GetTexture(aiType, j, &path, &mappingTechnique, &uvIndex, nullptr, nullptr, mapModeUVW);

			// Create a sampler state for this texture
			const auto samplerStateKey = ConstructSamplerName(mapModeUVW);
			if (!samplerStateKey.empty())
			{
				if (find_if(m_mainMeshData[meshIndex].m_psConfig.samplers.begin(), m_mainMeshData[meshIndex].m_psConfig.samplers.end(), [&samplerStateKey, &cogType](const SamplerInfo& s) ->bool{
					return s.type == cogType && s.samplerName == samplerStateKey;
				}) == m_mainMeshData[meshIndex].m_psConfig.samplers.end())
					m_mainMeshData[meshIndex].m_psConfig.samplers.push_back({ cogType, samplerStateKey, uvIndex });
			}
			material->GetTexture(aiType, j, &path);
			m_mainMeshData[meshIndex].m_texturePointers.push_back(m_textures.at(path.C_Str()).Get());
			m_mainMeshData[meshIndex].m_samplerKeys.push_back(m_SamplerKeys.at((j * m_scene->mNumMaterials + mesh->mMaterialIndex) * AI_TEXTURE_TYPE_MAX + aiType));
		}
	}

	m_mainMeshData[meshIndex].m_psConfig.UpdateFlags();	// flags might have changed
	m_mainMeshData[meshIndex].m_texturePointers.shrink_to_fit();
	m_mainMeshData[meshIndex].m_samplerKeys.shrink_to_fit();
}

//////////////////////////////////////////////////////////////////////////
// Model
//////////////////////////////////////////////////////////////////////////
Model::Model(
	const aiScene* scene,
	const shared_ptr<DX::DeviceResources> deviceResources,
	const shared_ptr<ResourcePool> resourcePool,
	const string modelName,
	const VertexShaderConfiguration& vs_conf,
	const PixelShaderConfiguration& ps_conf,
	const bool castsShadows,
	const bool receivesShadows) :
	DrawableGameComponent{castsShadows, receivesShadows},
	m_deviceResources{ deviceResources },
	m_resourcePool{ resourcePool },
	m_modelName{ modelName },
	m_materialsWrittenToConstantBuffer{ false }
{
	const auto& hashedModelData = Model::resources.find(modelName);

	if (ps_conf.isMaterialAnimated)
		throw runtime_error("Error! Animated materials are not yet fully implemented!");

	if (hashedModelData == Model::resources.end())
	{
		ModelData modelData;
		modelData.m_scene = shared_ptr<const aiScene>(scene);
		modelData.m_loadingComplete = false;
		modelData.vs_shaderConfiguration = vs_conf;
		modelData.vs_shaderConfiguration.usesTessellation = false;
		modelData.ps_shaderConfiguration = ps_conf;
		/*		for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
		{
		if (scene->mMaterials[i]->GetTextureCount(aiTextureType_DISPLACEMENT) > 0)
		modelData.m_usesTessellation = true;
		}*/
		Model::resources.insert(make_pair(m_modelName, modelData));
		CreateDeviceDependentResources();
	}
	else
	{
		InitConstantBuffers();
		m_loadingComplete = true;
	}
}

void Model::CreateDeviceDependentResources()
{
	auto& modelData = Model::resources[m_modelName];

	InitConstantBuffers();

	// Create the model from the meshes
	auto createModelTask = Concurrency::task<void>([this, &modelData]() {
		modelData.m_mainMeshData.resize(modelData.m_scene->mNumMeshes);
		modelData.m_mainMeshData.shrink_to_fit();
		modelData.m_prepassMeshData.resize(modelData.m_scene->mNumMeshes);
		modelData.m_prepassMeshData.shrink_to_fit();
		//modelData.m_shadowMeshData.resize(modelData.m_scene->mNumMeshes);
		//modelData.m_shadowMeshData.shrink_to_fit();

		CreateTexturesAndSamplers();

		for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
		{
			const aiMesh* mesh = modelData.m_scene->mMeshes[i];
			MeshData& mainMeshData = modelData.m_mainMeshData[i];
			MeshData& prepassMeshData = modelData.m_prepassMeshData[i];

			// For every mesh create a basic vertex shader
			// The vertex shaders are contained in a finite set.
			// Duplicates are filtered out for identical vertex structures in meshes
			m_resourcePool->CreateVertexShader(
				mesh,
				mainMeshData.m_VSkey,
				modelData.vs_shaderConfiguration);

			// Create a prepass vertex shader too. It will not necessarily be used, but if yes it will already be ready.
			auto prepassConfiguration = PrepassVSConfiguration{ modelData.vs_shaderConfiguration };
			m_resourcePool->CreateVertexShader(
				prepassMeshData.m_VSkey,
				prepassConfiguration);

			// If it's a shadow caster object create a vertex shader for shadow passes.
			if (isShadowCaster)
			{
				prepassConfiguration.SetUsesShadowMap(true);
				m_resourcePool->CreateVertexShader(
					prepassMeshData.m_VSkey_shadow,
					prepassConfiguration);
			}

			// The default behaviour is that the input layout resource key is the same as the vertex shader resource key
			mainMeshData.m_ILkey = mainMeshData.m_VSkey;
			prepassMeshData.m_ILkey = prepassMeshData.m_VSkey;

			// After the vertex shader and the input layout are created (and the vertex stride is determined)
			// we can finally create the geometry using one vertex and one index buffer for each mesh
			CreateVertexBuffer(mesh, mainMeshData, prepassMeshData);
			CreateIndexBuffer(mesh, mainMeshData, prepassMeshData);

			mainMeshData.m_psConfig = modelData.ps_shaderConfiguration;
			mainMeshData.m_psConfig.hasNormal = mesh->HasNormals();
			mainMeshData.m_psConfig.hasTangentBitangent = mesh->HasTangentsAndBitangents();
			mainMeshData.m_psConfig.numTexChannel = mesh->GetNumUVChannels();
			mainMeshData.m_psConfig.numVertColorChannel = mesh->GetNumColorChannels();
			prepassMeshData.m_psConfig.isTimeVariant = mainMeshData.m_psConfig.isTimeVariant;
			modelData.CollectMaterialResourcesInMeshes(i);
			// No need to create any textures or samplers for the prepass pixel shader

			// Check if the pixel shader configuration is valid. Specular lighting is particularly messy...
			if (mainMeshData.m_psConfig.useSpecularHighlight &&
				find_if(mainMeshData.m_psConfig.samplers.begin(), mainMeshData.m_psConfig.samplers.end(), [](const SamplerInfo& s) -> bool{
				return s.type == COG_Specular_map;
			}) == mainMeshData.m_psConfig.samplers.end())
			{
				// If the pixel shader needs to calculate the specular highlights it has 3 prerequisites: normal vectors, worldPos from the vertex(/domain/geometry) shader and a shininess value
				if (!mesh->HasNormals())
					throw runtime_error("Error! If you intend to calculate the specular highlights (and not use a specular map instead) then provide normal vectors. Or switch 'useSpecularHighlights' off in the pixel shader configuration.");
				if (modelData.vs_shaderConfiguration.shouldPassWorldPos == false)
					throw runtime_error("Error !If you intend to calculate the specular highlights (and not use a specular map instead) then use a vertex shader that passes through the world position of the vertices. Or switch 'useSpecularHighlights' off in the pixel shader configuration.");
				if (find_if(mainMeshData.m_psConfig.samplers.begin(), mainMeshData.m_psConfig.samplers.end(), [](const SamplerInfo& s) -> bool{
					return s.type == COG_Shininess_map;
				}) == mainMeshData.m_psConfig.samplers.end() && !mainMeshData.m_psConfig.material.HasShininess())
					throw runtime_error("Error! If you intend to calculate the specular highlights (and not use a specular map instead), then specify a shininess map OR a shininess value in the material parameters!");
			}
			mainMeshData.m_psConfig.UpdateFlags();

			if (mainMeshData.m_psConfig.HasAdditionalMaterialParamsFlag())
			{
				// One or more material parameters (shininess, opacity, etc..) are set so we create a constant buffer to pass them to the shader
				CD3D11_BUFFER_DESC bufferDesc(
					mainMeshData.m_psConfig.material.AlignedSize16(),
					D3D11_BIND_CONSTANT_BUFFER,
					D3D11_USAGE_DYNAMIC,
					D3D11_CPU_ACCESS_WRITE);
				DX::ThrowIfFailed(
					m_deviceResources->GetD3DDevice()->CreateBuffer(
						&bufferDesc,
						nullptr,
						mainMeshData.m_perMaterialConstantBuffer.GetAddressOf()
					)
				);
			}

			// Create the pixel shader for the mesh.
			const wstring& psKey = mainMeshData.m_psConfig.Stringify();
			m_resourcePool->CreatePixelShader(psKey, mainMeshData.m_psConfig);
			mainMeshData.m_PSkey = psKey;
			prepassMeshData.m_PSkey = ResourcePool::prepassPS_key;
		}

		// Once the model's resources are loaded, it is ready to be rendered.
		modelData.m_loadingComplete = true;
	});

	// Once the model is loaded, the object is ready to be rendered.
	createModelTask.then([this]() {
		// Once the model is loaded (with all its resources), the object is ready to be rendered.m
		m_loadingComplete = true;
	});
}

void Model::ReleaseDeviceDependentResources()
{
	m_loadingComplete = false;
	m_perInstanceConstantBuffer_main.Reset();
	m_perInstanceConstantBuffer_prepass.Reset();
	m_matrices.matrices.clear();
	// The destructor of the ModelData will call the destructor of the stl containers which,
	// will call the destructor of the smart pointers which will clean the dynamic memory
	// As soon as a new device becomes available everything will be reconstructed from scratch.
	Model::resources.erase(m_modelName);
}

void Model::Update(DX::StepTimer const& timer)
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!modelData.m_loadingComplete)
		return;

	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	const auto context = m_deviceResources->GetD3DDeviceContext();

	// Prepare the constant buffer to send it to the graphics device.
	if (!m_materialsWrittenToConstantBuffer)
	{
		vector<float> materialParams;
		for (const auto& meshData : modelData.m_mainMeshData)
		{
			if (!meshData.m_psConfig.HasAdditionalMaterialParamsFlag())
				continue;

			if (meshData.m_psConfig.material.HasColor())
			{
				materialParams.push_back(meshData.m_psConfig.material.color.x);
				materialParams.push_back(meshData.m_psConfig.material.color.y);
				materialParams.push_back(meshData.m_psConfig.material.color.z);
			}
			if (meshData.m_psConfig.material.HasShininess())
				materialParams.push_back(meshData.m_psConfig.material.shininess);
			if (meshData.m_psConfig.material.HasOpacity())
				materialParams.push_back(meshData.m_psConfig.material.opacity);

			DX::ThrowIfFailed(
				context->Map(
					meshData.m_perMaterialConstantBuffer.Get(),
					0,
					D3D11_MAP_WRITE_DISCARD,
					0,
					&mappedResource
				)
			);

			memcpy(mappedResource.pData, materialParams.data(), meshData.m_psConfig.material.AlignedSize16());
			context->Unmap(meshData.m_perMaterialConstantBuffer.Get(), 0);
			materialParams.clear();
		}
		m_materialsWrittenToConstantBuffer = true;	// Materials are only written to the constant buffer once. They cannot be animated for now, although it's on the TODO list.
	}
}

void Model::UpdateWorldTransform(const XMMATRIX& worldTransform)
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
		return;

	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	const auto context = m_deviceResources->GetD3DDeviceContext();

	if (!m_perInstanceConstantBuffer_main)
		return;

	if (modelData.vs_shaderConfiguration.hasWorldMatrix)
	{
		XMStoreFloat4x4(&m_matrices[0], worldTransform);
		// If there are normal vectors in the shader, pass the inverse transpose of the world transform matrix as well.
		if (modelData.vs_shaderConfiguration.hasNormalMatrix)
			XMStoreFloat4x4(&m_matrices[1], XMMatrixTranspose(XMMatrixInverse(nullptr, worldTransform)));

		DX::ThrowIfFailed(
			context->Map(
				m_perInstanceConstantBuffer_prepass.Get(),
				0,
				D3D11_MAP_WRITE_DISCARD,
				0,
				&mappedResource
			)
		);

		memcpy(mappedResource.pData, m_matrices.matrices.data(), sizeof(XMFLOAT4X4));
		context->Unmap(m_perInstanceConstantBuffer_prepass.Get(), 0);
	}
	else if (modelData.vs_shaderConfiguration.hasNormalMatrix)
		XMStoreFloat4x4(&m_matrices[0], XMMatrixTranspose(XMMatrixInverse(nullptr, worldTransform)));
	else
		return;

	// Prepare the constant buffer to send it to the graphics device.
	DX::ThrowIfFailed(
		context->Map(
			m_perInstanceConstantBuffer_main.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
		)
	);

	memcpy(mappedResource.pData, m_matrices.matrices.data(), m_matrices.matrices.size() * sizeof(XMFLOAT4X4));
	context->Unmap(m_perInstanceConstantBuffer_main.Get(), 0);
}

void Model::Render()
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
	{
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	//	m_resourcePool->SetRasterizerState(L"cull_none");
	if (!m_matrices.IsEmpty() && m_perInstanceConstantBuffer_main)
	{
		// Send the normal and world matrices constant buffer to the graphics device.
		m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_main.Get(), 4);
	}

	// Draw each mesh separately
	for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
	{
		const auto& meshData = modelData.m_mainMeshData[i];
		UINT stride = m_resourcePool->GetVertexStride(meshData.m_ILkey);
		UINT offset = 0;
		const aiMesh* mesh = modelData.m_scene->mMeshes[i];

		//if (meshData.m_psConfig.isMaterialAnimated)
		//{
		//	// TODO: Animate the material...
		//}

		if (meshData.m_psConfig.HasAdditionalMaterialParamsFlag())
		{
			// Send the material constant buffer to the graphics device.
			m_resourcePool->SetConstantBuffer<PixelShader>(meshData.m_perMaterialConstantBuffer.Get(), 2);
		}

		// Bind the mesh's vertex buffer to the vertex shader.
		context->IASetVertexBuffers(
			0,
			1,
			meshData.m_vertexBuffer.GetAddressOf(),
			&stride,
			&offset
			);

		context->IASetIndexBuffer(
			meshData.m_indexBuffer.Get(),
			DXGI_FORMAT_R16_UINT, // Each index is one 16-bit unsigned integer (short).
			0
			);

		switch (mesh->mPrimitiveTypes)
		{
		case aiPrimitiveType_POINT:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
		case aiPrimitiveType_LINE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			break;
		case aiPrimitiveType_TRIANGLE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		case aiPrimitiveType_POLYGON:
			// The assimp documentation says little about polygon primitives. Use triangulation...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		default:
			// This code should never be reached...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		}

		// Attach our input layout.
		m_resourcePool->SetInputLayout(meshData.m_ILkey);

		// Attach our vertex shader.
		m_resourcePool->SetVertexShader(meshData.m_VSkey);

		if (modelData.vs_shaderConfiguration.usesTessellation)
		{
			// Attach our hull shader.
			m_resourcePool->SetHullShader(meshData.m_HSkey);

			// Attach our domain shader.
			m_resourcePool->SetDomainShader(meshData.m_DSkey);
		}

		// Attach our pixel shader.
		m_resourcePool->SetPixelShader(meshData.m_PSkey);

		SetShaderTexturesAndSamplers(context, meshData.m_texturePointers, meshData.m_samplerKeys);

		// Draw the objects.
		context->DrawIndexed(
			mesh->mNumFaces * 3,
			0,
			0
		);

		m_resourcePool->ResetAvailableSlots();
	}
}

void Model::GetDepth(bool shadowPass /*= false*/)
{
	const auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
	{
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	//	m_resourcePool->SetRasterizerState(L"cull_none");
	if (!m_matrices.IsEmpty() && m_perInstanceConstantBuffer_prepass)
	{
		// Send the normal and world matrices constant buffer to the graphics device.
		m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_prepass.Get(), 4);
	}

	// Draw each mesh separately
	for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
	{
		const auto& meshData = modelData.m_prepassMeshData[i];
		UINT stride = m_resourcePool->GetVertexStride(meshData.m_ILkey);
		UINT offset = 0;
		const aiMesh* mesh = modelData.m_scene->mMeshes[i];

		// Bind the mesh's vertex buffer to the vertex shader.
		context->IASetVertexBuffers(
			0,
			1,
			meshData.m_vertexBuffer.GetAddressOf(),
			&stride,
			&offset
		);

		context->IASetIndexBuffer(
			meshData.m_indexBuffer.Get(),
			DXGI_FORMAT_R16_UINT, // Each index is one 16-bit unsigned integer (short).
			0
		);

		switch (mesh->mPrimitiveTypes)
		{
		case aiPrimitiveType_POINT:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
		case aiPrimitiveType_LINE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			break;
		case aiPrimitiveType_TRIANGLE:
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		case aiPrimitiveType_POLYGON:
			// The assimp documentation says little about polygon primitives. Use triangulation...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		default:
			// This code should never be reached...
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED);
			break;
		}

		// Attach our input layout.
		m_resourcePool->SetInputLayout(meshData.m_ILkey);

		// Attach our vertex shader.
		m_resourcePool->SetVertexShader(shadowPass ? meshData.m_VSkey_shadow : meshData.m_VSkey);

		// Attach our pixel shader.
		m_resourcePool->SetPixelShader(meshData.m_PSkey);

		// Draw the objects.
		context->DrawIndexed(
			mesh->mNumFaces * 3,
			0,
			0
		);

		m_resourcePool->ResetAvailableSlots();
	}
}

void Model::ToggleShadowMapping(bool enabled)
{
	auto& modelData = Model::resources[m_modelName];

	// Loading is asynchronous.
	if (!m_loadingComplete || !modelData.m_loadingComplete)
		return;

	unsigned int i = 0;
	modelData.m_loadingComplete = false;
	modelData.vs_shaderConfiguration.usesShadowMap = enabled;

	auto recreateVS_PSTask = Concurrency::task<void>([this, &modelData]() {
		for (unsigned int i = 0; i < modelData.m_scene->mNumMeshes; i++)
		{
			const aiMesh* mesh = modelData.m_scene->mMeshes[i];
			MeshData& mainMeshData = modelData.m_mainMeshData[i];

			m_resourcePool->CreateVertexShader(
				mesh,
				mainMeshData.m_VSkey,
				modelData.vs_shaderConfiguration);

			mainMeshData.m_PSkey = mainMeshData.m_psConfig.Stringify();
			m_resourcePool->CreatePixelShader(mainMeshData.m_PSkey, mainMeshData.m_psConfig);
			i++;
		}
		modelData.m_loadingComplete = true;
	});
}

void Model::AddTexture(const char* filename, ID3D11ShaderResourceView* texture)
{
	string key(filename);
	// TODO check if const char* is okay to store or not
	if (texture != nullptr && !key.empty())
	{
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> ptr(texture);
		Model::resources[m_modelName].m_textures.insert(make_pair(key, ptr));
	}
}

void Model::InitConstantBuffers()
{
	const auto& modelData = Model::resources[m_modelName];

	if (modelData.vs_shaderConfiguration.hasNormalMatrix)
	if (modelData.vs_shaderConfiguration.hasWorldMatrix)
		m_matrices.matrices.resize(2);
	else
		m_matrices.matrices.resize(1);
	else if (modelData.vs_shaderConfiguration.hasWorldMatrix)
		m_matrices.matrices.resize(1);

	m_matrices.matrices.shrink_to_fit();
	if (modelData.vs_shaderConfiguration.hasWorldMatrix || modelData.vs_shaderConfiguration.hasNormalMatrix)
	{
		CD3D11_BUFFER_DESC constantBufferDesc(
			sizeof(XMFLOAT4X4)* (unsigned int)m_matrices.matrices.size(),
			D3D11_BIND_CONSTANT_BUFFER,
			D3D11_USAGE_DYNAMIC,
			D3D11_CPU_ACCESS_WRITE);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&constantBufferDesc,
				nullptr,
				m_perInstanceConstantBuffer_main.GetAddressOf()
			)
		);
		constantBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&constantBufferDesc,
				nullptr,
				m_perInstanceConstantBuffer_prepass.GetAddressOf()
			)
		);
	}
}

void Model::CreateVertexBuffer(const aiMesh* mesh, MeshData& mainMeshData, MeshData& prepassMeshData)
{
	// Vertex structure size in bytes
	unsigned int mainVertexStride = m_resourcePool->GetVertexStride(mainMeshData.m_ILkey);
	unsigned int prepassVertexStride = m_resourcePool->GetVertexStride(prepassMeshData.m_ILkey);
	// Offset inside the vertex structure in bytes for the current vertex
	unsigned int mainOffsetInsideVertexStructure = 0;
	// A variable used for multi-texture channel vertices and multi-vertex-color channel vertices
	unsigned int j, k = 0;
	// The buffer that contains all the vertex data, which might be heterogeneous
	// Can contain floats and unsigned integers (for the bone indices in the skinning data).
	// The exact structure of a vertex is determined in runtime.
	vector<char> mainVertices(mesh->mNumVertices * mainVertexStride);
	vector<char> prepassVertices(mesh->mNumVertices * prepassVertexStride);
	static_assert(sizeof(aiVector3D) == 3 * sizeof(float), "Assimp aiVector3D's size must be 3 times the size of float. If that's not true, use a memcpy for each vector component's copy instead of copying the whole structure!");
	static_assert(sizeof(aiColor4D) == 4 * sizeof(float), "Assimp aiColor4D's size must be 4 times the size of float. If that's not true, use a memcpy for each vector component's copy instead of copying the whole structure!");

	// Iterate through every vertex and import the data from the model file
	for (j = 0; j < mesh->mNumVertices; j++, mainOffsetInsideVertexStructure = 0, k = 0)
	{
		// Address of the j-th vertex is 'vertices + j * vertexStride'
		if (mesh->HasPositions())
		{
			memcpy(&mainVertices[j * mainVertexStride], &mesh->mVertices[j], sizeof(float) * 3);
			memcpy(&prepassVertices[j * prepassVertexStride], &mesh->mVertices[j], sizeof(float) * 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
		}
		if (mesh->HasNormals())
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mNormals[j], sizeof(float) * 3);
			mainOffsetInsideVertexStructure += sizeof(float) * 3;
		}
		if (mesh->HasTangentsAndBitangents())
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mTangents[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mBitangents[j], sizeof(float)* 3);
			mainOffsetInsideVertexStructure += sizeof(float)* 3;
		}
		while (mesh->HasTextureCoords(k++) && k < 7)
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mTextureCoords[k - 1][j], sizeof(float)* 2);
			mainOffsetInsideVertexStructure += sizeof(float)* 2;
		}
		k = 0;
		while (mesh->HasVertexColors(k++) && k < 8)
		{
			memcpy(&mainVertices[j * mainVertexStride + mainOffsetInsideVertexStructure], &mesh->mColors[k - 1][j], sizeof(float)* 4);
			mainOffsetInsideVertexStructure += sizeof(float)* 4;
		}
	}

	// Create the vertex buffer
	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = mainVertices.data();
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;
	CD3D11_BUFFER_DESC vertexBufferDesc(mesh->mNumVertices * mainVertexStride, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE);

	// Store the newly created vertex buffer in the mesh data
	// vector copies the pointer and converts it to a ComPtr using its converting constructor
	// ComPtr and vector take care of memory management
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			mainMeshData.m_vertexBuffer.GetAddressOf()
		)
	);

	// Now create the prepass vertex buffer too.
	vertexBufferData.pSysMem = prepassVertices.data();
	vertexBufferDesc.ByteWidth = mesh->mNumVertices * prepassVertexStride;
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			prepassMeshData.m_vertexBuffer.GetAddressOf()
		)
	);
}

void Model::CreateIndexBuffer(const aiMesh* mesh, MeshData& mainMeshData, MeshData& prepassMeshData)
{
	// Buffer for the index data (common for main and prepass)
	vector<unsigned short> indices(mesh->mNumFaces * 3);

	// Assemble every face in the mesh just like the IA would do
	for (unsigned int j = 0; j < mesh->mNumFaces; j++)
	{
		const aiFace& Face = mesh->mFaces[j];
		// Only triangles are supported!
		assert(Face.mNumIndices == 3);
		indices[3 * j] = Face.mIndices[0];
		indices[3 * j + 1] = Face.mIndices[1];
		indices[3 * j + 2] = Face.mIndices[2];
	}

	// Create the index buffer
	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices.data();
	indexBufferData.SysMemPitch = 0;
	indexBufferData.SysMemSlicePitch = 0;
	CD3D11_BUFFER_DESC indexBufferDesc(mesh->mNumFaces * 3 * sizeof(short), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE);

	// Store the newly created index buffer in the mesh data
	// vector copies the pointer and converts it to a ComPtr using its converting constructor
	// ComPtr and vector take care of memory management
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
		&indexBufferDesc,
		&indexBufferData,
		mainMeshData.m_indexBuffer.GetAddressOf()
		)
		);

	// The index buffer is a common resource for main and prepass vertex shaders
	prepassMeshData.m_indexBuffer = mainMeshData.m_indexBuffer;	// ComPtr behaves like a shared pointer so it can be copied.
}

void Model::CreateTexturesAndSamplers()
{
	ModelData& modelData = Model::resources[m_modelName];
	const auto nTextureTypes = sizeof(aiTextureTypes) / sizeof(aiTextureTypes[0]);
	TextureType cogType;
	for (unsigned int i = 0; i < modelData.m_scene->mNumMaterials; i++)
	{
		aiMaterial* material = modelData.m_scene->mMaterials[i];
		aiString path;
		wchar_t textureFilename[100] = { '\0' };
		aiTextureMapMode mapModeUVW[3];
		aiTextureMapping mappingTechnique;
		unsigned int uvIndex;
		string samplerStateKey;

		for (const auto aiType : aiTextureTypes)
		{
			const auto iterator = cogTextureTypes.find(aiType);
			if (iterator == cogTextureTypes.cend())
				continue;
			else
				cogType = iterator->second;

			for (unsigned int j = 0; j < material->GetTextureCount(aiType); j++)
			{
				material->GetTexture(aiType, j, &path, &mappingTechnique, &uvIndex, nullptr, nullptr, mapModeUVW);

				// Create a sampler state for this texture
				m_resourcePool->CreateSamplerState(mapModeUVW, samplerStateKey);
				if (!samplerStateKey.empty())
					modelData.m_SamplerKeys.insert(make_pair((j * modelData.m_scene->mNumMaterials + i) * AI_TEXTURE_TYPE_MAX + aiType, samplerStateKey));

				// Create the texture
				ID3D11ShaderResourceView* texture;
				MultiByteToWideChar(CP_UTF8, 0, path.C_Str(), -1, textureFilename, 100);
				DX::ThrowIfFailed(
					CreateDDSTextureFromFile(
					m_deviceResources->GetD3DDevice(),
					textureFilename,
					nullptr,
					&texture
					)
					);

				// Add the newly created texture to the container.
				// The container will take care of the freeing the memory.
				Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> ptr(texture);
				modelData.m_textures.insert(make_pair(path.C_Str(), ptr));
			}
		}
	}
}

void Model::SetShaderTexturesAndSamplers(ID3D11DeviceContext2* context, const vector<ID3D11ShaderResourceView*>& textures, const vector<string>& samplers) const
{
	for (const auto& samplerKey : samplers)
		m_resourcePool->SetSamplerState<PixelShader>(samplerKey);
	context->PSSetShaderResources(1, static_cast<unsigned int>(textures.size()), textures.data());
}