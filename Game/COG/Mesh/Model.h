#pragma once

#include <string>
#include "assimp/scene.h"
#include "COG/ShaderStructures.h"
#include "COG/Entities/DrawableGameComponent.h"
#include "COG/Mesh/ResourcePool.h"

namespace COG
{
	class Model : public DrawableGameComponent
	{
	public:
		// Forward declaration
		struct MeshData;

		// Direct3D resources for the geometry.
		struct ModelData
		{
			// Handles to global resources
			// Keys used to retrieve resources from the ResourcePool
			std::vector<MeshData> m_mainMeshData;		// this is the mesh data used for drawing the model. Prepass mesh resources are stored in a separate array (elements on the same index correspond to each other in the 2 arrays)
			std::vector<MeshData> m_prepassMeshData;	// prepasses require custom vertex buffer and shaders (for all pipeline stages)
			std::unordered_map<unsigned int, std::string> m_SamplerKeys;
			// Handles to local resources
			std::unordered_map<std::string, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> m_textures;
			std::shared_ptr<const aiScene> m_scene;

			VertexShaderConfiguration vs_shaderConfiguration;
			PixelShaderConfiguration ps_shaderConfiguration;	// The default config. The full configuration is stored in each and every mesh data
			bool m_loadingComplete;

			void CollectMaterialResourcesInMeshes(unsigned int meshIndex);
		};

		struct MeshData{
			std::wstring	m_ILkey;
			std::wstring	m_VSkey;
			std::wstring	m_VSkey_shadow;
			std::wstring	m_HSkey;	// used only for tessellation
			std::wstring	m_DSkey;	// used only for tessellation
			std::wstring	m_GSkey;	// rarely used (tessellation makes it almost obsolete)
			std::wstring	m_PSkey;
			Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer;
			Microsoft::WRL::ComPtr<ID3D11Buffer> m_indexBuffer;
			Microsoft::WRL::ComPtr<ID3D11Buffer> m_perMaterialConstantBuffer;
			PixelShaderConfiguration m_psConfig;
			MaterialConstantBuffer m_materialParameters;	// unused (for now...)
			std::vector<ID3D11ShaderResourceView*> m_texturePointers;	// Pointers to another container. Absolutely no ownership here, these pointers are only collected for performance boost.
			std::vector<std::string> m_samplerKeys;

			bool m_loadingComplete;
		};

		// Default constructor does almost nothing.
		Model() : DrawableGameComponent{ false, false }	{};
		/// <summary> Constructs a new model. </summary>
		/// <param name="scene">The aiScene that contains the model data.</param>
		/// <param name="deviceResources">The DX device object needed for GPU calls.</param>
		/// <param name="resourcePool">The resource pool object that handles DirectX resources globally.</param>
		Model(
			const aiScene* scene,
			const std::shared_ptr<DX::DeviceResources> deviceResources,
			const std::shared_ptr<ResourcePool> resourcePool,
			const std::string modelName,
			const VertexShaderConfiguration& vs_conf,
			const PixelShaderConfiguration& ps_conf,
			const bool castShadows = false,
			const bool receivessShadows = false);

		// Virtual methods
		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void Update(DX::StepTimer const& timer) override;
		void Render() override;
		void GetDepth(bool shadowPass = false) override;
		void ToggleShadowMapping(bool enabled) override;

		/// <summary> Adds a texture object to the container with the given key. </summary>
		/// <remarks> If the container already contains an element with the same key, the new element
		/// will be considered the copy of the old one and it will not be added again. </remarks>
		void AddTexture(const char* key, ID3D11ShaderResourceView* texture);
		void UpdateWorldTransform(const DirectX::XMMATRIX& worldTransform);
	protected:
		static std::unordered_map<std::string, ModelData> resources;

		/*const */std::shared_ptr<DX::DeviceResources> m_deviceResources;
		/*const */std::shared_ptr<ResourcePool>	m_resourcePool;

		Microsoft::WRL::ComPtr<ID3D11Buffer> m_perInstanceConstantBuffer_main;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_perInstanceConstantBuffer_prepass;

		std::string m_modelName;
		MatrixCollectionConstantBuffer m_matrices;
		bool m_materialsWrittenToConstantBuffer;

		virtual void CreateVertexBuffer(const aiMesh* mesh, MeshData& mainData, MeshData& prepassData);
		void CreateIndexBuffer(const aiMesh* mesh, MeshData& mainData, MeshData& prepassData);
		void InitConstantBuffers();
		void CreateTexturesAndSamplers();
		void SetShaderTexturesAndSamplers(ID3D11DeviceContext2* context, const std::vector<ID3D11ShaderResourceView*>& textures, const std::vector<std::string>& samplers) const;
	};
}