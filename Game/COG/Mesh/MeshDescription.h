#pragma once

#include "assimp/mesh.h"

namespace COG{
	struct MeshDescription{
		bool hasPositions;
		bool hasNormals;
		bool hasTangentBinormals;
		bool hasSkinning;
		unsigned int nTextureChannels;
		unsigned int nVertexColorChannels;

		MeshDescription() :
			hasPositions{ true },
			hasNormals{ false },
			hasTangentBinormals{ false },
			hasSkinning{ false },
			nTextureChannels{ 0 },
			nVertexColorChannels{ 0 }
		{
		}

		MeshDescription(const aiMesh* mesh) :		// implicit conversion constructor!
			hasPositions{ mesh->HasPositions() },
			hasNormals{ mesh->HasNormals() },
			hasTangentBinormals{ mesh->HasTangentsAndBitangents() },
			hasSkinning{ mesh->HasBones() },
			nTextureChannels{ mesh->GetNumUVChannels() },
			nVertexColorChannels{ mesh->GetNumColorChannels() }
		{
		}
	};
}
