#pragma once

#include <algorithm>

namespace COG {
	enum PrepassFlag {
		None		= 0x0,
		Depth		= 0x1,
		ObjectID	= 0x1 << 1,	// not implemented yet...
		// more to come ...
		All			= Depth | ObjectID
	};

	enum TextureType : short {
		COG_Diffuse_map = 0x0,
		COG_Specular_map,
		COG_Ambient_map,
		COG_Emissive_map,
		COG_Normals_map,
		COG_Shininess_map,
		COG_Opacity_map,
		COG_Light_map,
		COG_Reflection_map
	};

	enum LightType : short {
		COG_Directional = 0x0,
		COG_Positional,
		COG_Spot	// unimplemented
	};

	struct SamplerInfo{
		TextureType type;
		std::string samplerName;
		unsigned int texChannel;	// 0..7
	};

	struct MaterialInfo{
		DirectX::XMFLOAT3 color;
		float shininess;
		float opacity;

		MaterialInfo():
			color{ default_color },
			shininess{ defaultShininess },
			opacity{ defaultOpacity }
		{
		}

		UINT AlignedSize16() const { return HasColor() && HasShininess() && HasOpacity() ? 8 * sizeof(float) : 4 * sizeof(float); }

		bool HasColor() const { return color.x != default_color.x || color.y != default_color.y || color.z != default_color.z; }
		bool HasShininess() const { return shininess != defaultShininess; }
		bool HasOpacity() const { return opacity != defaultOpacity; }

		bool IsDefault() const{ return	HasColor() || HasShininess() || HasOpacity(); }

	private:
		static const DirectX::XMFLOAT3 default_color;
		static const float defaultShininess;
		static const float defaultOpacity;
	};

	struct VertexShaderConfiguration{
		bool isTimeVariant;
		bool hasWorldMatrix;
		bool hasNormalMatrix;
		bool usesTessellation;
		bool shouldPassWorldPos;
		bool usesShadowMap;
		unsigned int numberOfBones;

		::std::vector<D3D_SHADER_MACRO> macros;
	};

	struct HullShaderConfiguration{
		// TODO Implement.
	};

	struct DomainShaderConfiguration{
		// TODO Implement.
	};

	struct GeometryShaderConfiguration{
		// TODO Implement.
	};

	struct PixelShaderConfiguration{
		bool hasNormal;
		bool hasTangentBitangent;
		unsigned int numTexChannel;			// 0..7
		unsigned int numVertColorChannel;	// 0..8
		bool usesShadowMap;
		bool isTimeVariant;
		std::vector<SamplerInfo> samplers;
		LightType lightType;
		bool lightHasNonDefaultColor;
		MaterialInfo material;
		DirectX::XMFLOAT4 defaultAmbient;
		bool useSpecularHighlight;
		bool isMaterialAnimated;

		PixelShaderConfiguration() :
			hasNormal{ false },
			hasTangentBitangent{ false },
			numTexChannel{ 0 },
			numVertColorChannel{ 0 },
			usesShadowMap{ false },
			isTimeVariant{ false },
			samplers{},
			lightType{ COG_Directional },
			lightHasNonDefaultColor{ false },
			material{},
			defaultAmbient{ 0.0f, 0.0f, 0.0f, 1.0f },
			useSpecularHighlight{ false },
			isMaterialAnimated{ false },
			_hasAdditionalMaterialParams{ false },
			_needCamposConstantBuffer{ false }
		{
		}

		std::wstring Stringify() const {
			// TODO: Implement this correctly!!
			std::wstring key;
			if (hasNormal)
				key.append(L"N");
			if (hasTangentBitangent)
				key.append(L"tb");
			for (unsigned int i = 0; i < numTexChannel; i++)
				key.append(L"T");
			if (usesShadowMap)
				key.append(L"_S_");

			return key;
		}

		void UpdateFlags() {
			_needCamposConstantBuffer = useSpecularHighlight &&
				std::find_if(samplers.begin(), samplers.end(), [](const SamplerInfo& s) -> bool{
				return s.type == COG_Specular_map;
			}) == samplers.end();

			_hasAdditionalMaterialParams = material.IsDefault();
		}

		bool NeedCamposFlag() const { return _needCamposConstantBuffer; }
		bool HasAdditionalMaterialParamsFlag() const { return _hasAdditionalMaterialParams; }

	private:
		// Inner flags to speed up the render loop
		bool _needCamposConstantBuffer;
		bool _hasAdditionalMaterialParams;
		/*		size_t Hash() const{
			static const std::hash<bool> boolHasher;
			static const std::hash<std::underlying_type<TextureType>::type> textureHasher;	// short
			static const std::hash<std::underlying_type<LightType>::type> lightHasher;		// short
			static const std::hash<int> intHasher;
			static const std::hash<float> floatHasher;
		}*/
	};

	struct PrepassVSConfiguration {
		PrepassFlag m_flags;

		PrepassVSConfiguration(bool isTimeVariant, bool hasWorldMatrix, int nBones = -1, PrepassFlag flags = Depth)
		{
			m_hasBones = nBones > 0 ;
			m_flags = flags;
			_underlyingConfig = {
				isTimeVariant,
				hasWorldMatrix,
				false,	// do not care about the normal vectors
				false,	// no tessellation so far
				false,	// no need to pass the world position to the pixel shader (or the next pipeline stage, whatsoever)
				false,	// This is a prepass vertex shader. There is no need to pass the position from the light's view (which happens when 'usesShadowMap' is true)
				nBones > 0 ? (unsigned int)nBones : 0
			};
		}

		PrepassVSConfiguration(const VertexShaderConfiguration& shaderConfiguration, PrepassFlag flags = Depth)
		{
			m_hasBones = shaderConfiguration.numberOfBones > 0;
			m_flags = flags;
			_underlyingConfig = {
				shaderConfiguration.isTimeVariant,
				shaderConfiguration.hasWorldMatrix,
				false,	// do not care about the normal vectors
				false,	// no tessellation so far
				false,	// no need to pass the world position to the pixel shader (or the next pipeline stage, whatsoever)
				false,	// This is a prepass vertex shader. There is no need to pass the position from the light's view (which happens when 'usesShadowMap' is true)
				shaderConfiguration.numberOfBones
			};
		}

		unsigned int NumberOfBones() const { return _underlyingConfig.numberOfBones; }
		bool IsTimeVariant() const { return _underlyingConfig.isTimeVariant; }
		bool HasWorldMatrix() const { return _underlyingConfig.hasWorldMatrix; }
		bool ShadowMap() const { return _underlyingConfig.usesShadowMap; }
		bool HasBones() const { return m_hasBones; }

		void SetNumberOfBones(int nBones){
			if (nBones > 0) {
				_underlyingConfig.numberOfBones = (unsigned int)nBones;
				m_hasBones = true;
			}
			else {
				_underlyingConfig.numberOfBones = 0;
				m_hasBones = false;
			}
		}
		void SetUsesShadowMap(bool b)
		{
			_underlyingConfig.usesShadowMap = b;
		}

		void SetIsTimeVariant(bool b) { _underlyingConfig.isTimeVariant = b; }
		void SetHasWorldMatrix(bool b) { _underlyingConfig.hasWorldMatrix = b; }
	private:
		VertexShaderConfiguration _underlyingConfig;
		bool m_hasBones;
	};

	std::string Stringify(TextureType t);

	std::string Stringify(LightType l);

	std::wstring Stringify(PrepassFlag f);
}

namespace std{
//	template<class T = void>
//	struct equal_to{
//		bool operator()(const COG::SamplerInfo&, const COG::SamplerInfo&) const;
//	};
}