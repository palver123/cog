﻿#pragma once

#include "COG/HLSL_macros.h"

namespace COG
{
	// Constant buffer used to send MVP matrices to the vertex shader.
	struct ModelViewProjectionConstantBuffer
	{
		DirectX::XMFLOAT4X4 model;
		DirectX::XMFLOAT4X4 view;
		DirectX::XMFLOAT4X4 projection;

		ModelViewProjectionConstantBuffer()
		{
			XMStoreFloat4x4(&model, DirectX::XMMatrixIdentity());
			XMStoreFloat4x4(&view, DirectX::XMMatrixIdentity());
			XMStoreFloat4x4(&projection, DirectX::XMMatrixIdentity());
		}
	};

	// Constant buffer used to send a matrix to the vertex shader.
	struct MatrixConstantBuffer
	{
		DirectX::XMFLOAT4X4 matrix;

		MatrixConstantBuffer()
		{
			XMStoreFloat4x4(&matrix, DirectX::XMMatrixIdentity());
		}
	};

	// Constant buffer used to send a matrix to the vertex shader.
	struct MatrixCollectionConstantBuffer
	{
		::std::vector<DirectX::XMFLOAT4X4> matrices;

		explicit MatrixCollectionConstantBuffer(unsigned int n = 0) : matrices(n)
		{
			for (auto& matrix : matrices)
				XMStoreFloat4x4(&matrix, DirectX::XMMatrixIdentity());
		}

		DirectX::XMFLOAT4X4& operator[](unsigned int index) { return matrices[index]; }

		void ResizeWithIdentity(unsigned int n)
		{
			matrices.resize(n);
			matrices.shrink_to_fit();
			for (auto& matrix : matrices)
				XMStoreFloat4x4(&matrix, DirectX::XMMatrixIdentity());
		}

		void ResizeWithZeros(unsigned int n)
		{
			matrices.resize(n);
			matrices.shrink_to_fit();
			for (auto& matrix : matrices)
				XMStoreFloat4x4(&matrix, DirectX::XMMatrixScaling(0.0f, 0.0f, 0.0f));
		}

		bool IsEmpty() const { return matrices.empty(); }
	};

	// Constant buffer used to send a float4 to the vertex shader.
	struct Float4ConstantBuffer{
		DirectX::XMFLOAT4 value;

		Float4ConstantBuffer(const float x = 0.0f, const float y = 0.0f, const float z = 0.0f, const float w = 0.0f) :
			value{ x, y, z, w}
		{
		}
	};

	// Constant buffer used to send the elapsed time to the vertex shader.
	struct TimeConstantBuffer : public Float4ConstantBuffer
	{
		TimeConstantBuffer() :
			Float4ConstantBuffer{}
		{}
	};

	// Constant buffer used to send the material parameters to the pixel shader.
	struct MaterialConstantBuffer : public Float4ConstantBuffer
	{
		MaterialConstantBuffer() :
			Float4ConstantBuffer{}
		{}
	};

	// Constant buffer used to send the camera position to the pixel shader.
	struct CamPosConstantBuffer : public Float4ConstantBuffer
	{
		CamPosConstantBuffer() :
			Float4ConstantBuffer{ }
		{}
	};

	// Constant buffer used to send the light parameters to the pixel shader.
	struct LightConstantBuffer
	{
		::std::vector<::DirectX::XMFLOAT4> values;

		explicit LightConstantBuffer(size_t s):
			values{s}	// set all parameters to zero
		{}
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPosition
	{
		DirectX::XMFLOAT3 pos;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionColor
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 color;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionNormal
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 normal;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionTexture
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT2 tex;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPosition2DTexture
	{
		DirectX::XMFLOAT2 pos2D;
		DirectX::XMFLOAT2 tex;
	};

	// Used to send per-vertex data to the vertex shader.
	struct VertexPositionNormalTexture
	{
		DirectX::XMFLOAT3 pos;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 tex;
	};

	// Used to send per-vertex data to the particle vertex shader. The w component of Position is the 'initial age'.
	struct ParticleVertex
	{
		DirectX::XMFLOAT3 initialPos;
		DirectX::XMFLOAT3 initialVelocity;
		DirectX::XMFLOAT2 age_lifespan;
	};
}