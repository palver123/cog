#pragma once

#include "COG/Mesh/ResourcePool.h"

namespace COG{
	// Forward declaration
	class BlurPostProcessor;

	enum PostEffect{
		Blur		= 0x1,
		Bloom		= 0x1 << 1,
		DoF			= 0x1 << 2,
		Fog3D		= 0x1 << 3,
		Vignette	= 0x1 << 4,

		UsesDepthMap = DoF | Fog3D,
		UsesBlurring = Blur | Bloom | DoF
	};

	struct PingPongHelper
	{
		bool m_initialized = false;

		void CreateWindowSizeDependentResources(ID3D11Device2* device, unsigned int width, unsigned int height);
		void ReleaseResources();

		ID3D11RenderTargetView*	const	GetPostProcessRenderTargetView() const { return m_useA ? m_postProcessRTV_A.Get() : m_postProcessRTV_B.Get(); }
		ID3D11ShaderResourceView* const	GetPostProcessShaderResourceView() const { return m_useA ? m_postProcessSRV_A.Get() : m_postProcessSRV_B.Get(); }
		ID3D11ShaderResourceView* const	GetPostProcessShaderResourceViewAndSwap() { m_useA = !m_useA;  return m_useA ? m_postProcessSRV_B.Get() : m_postProcessSRV_A.Get(); }

	private:
		// 2 textures used for ping-pong rendering in case of multiple post processes
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	m_postProcessRTV_A;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	m_postProcessRTV_B;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_postProcessSRV_A;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_postProcessSRV_B;
		bool m_useA = true;
	};

	class PostProcessor
	{
	public:
		PostProcessor():
			m_deviceResources{ nullptr },
			m_resourcePool{ nullptr },
			m_blurProcessor{ nullptr },
			m_loadingComplete{ false }
		{}

		PostProcessor(std::shared_ptr<DX::DeviceResources> deviceResources, std::shared_ptr<ResourcePool> resourcePool, PostEffect effect) :
			m_deviceResources{ deviceResources },
			m_resourcePool{ resourcePool },
			m_effectType{ effect },
			m_blurProcessor{ nullptr },
			m_loadingComplete{ false }
		{
			SetPixelShaderKey();
			CreateDeviceDependentResources();
		}

		virtual void CreateDeviceDependentResources();
		virtual void ReleaseDeviceDependentResources();
		virtual void Apply(ID3D11ShaderResourceView* const* inputTextures, unsigned int numInputTextures, ID3D11RenderTargetView* const renderTarget);

		void CreateWindowSizeDependentResources();
		void SetType(PostEffect newType);
		bool IsReady() const;
		bool RequiresDepthMap() const { return (m_effectType & UsesDepthMap) != 0; }

		static ID3D11RenderTargetView*	const	GetPostProcessRenderTargetView() { return PostProcessor::pingPong.GetPostProcessRenderTargetView(); }
		static ID3D11ShaderResourceView* const	GetPostProcessShaderResourceView() { return PostProcessor::pingPong.GetPostProcessShaderResourceView(); }
		static ID3D11ShaderResourceView* const	GetPostProcessShaderResourceViewAndSwap() { return PostProcessor::pingPong.GetPostProcessShaderResourceViewAndSwap(); }
		static void CreateWindowSizeDependentResources(ID3D11Device2* device, unsigned int width, unsigned int height) { pingPong.ReleaseResources(); pingPong.CreateWindowSizeDependentResources(device, width, height); }
		static void ReleaseResources() { pingPong.ReleaseResources(); }
		static bool Ready() { return pingPong.m_initialized; }
	protected:
		void SetPixelShaderKey();
		void RecreatePixelShader();

		static PingPongHelper	pingPong;

		const std::wstring		m_postProcessIL_key = L"postProcessIL";
		const std::wstring		m_postProcessVS_key = L"PassthroughVS";
		const std::string		m_postProcessSampler_key = "postProcessSS";
		std::wstring			m_postProcessPS_key;
		bool					m_loadingComplete;
		PostEffect				m_effectType;

		std::shared_ptr<DX::DeviceResources>	m_deviceResources;
		std::shared_ptr<ResourcePool>			m_resourcePool;
		Microsoft::WRL::ComPtr<ID3D11Buffer>	m_screenSizedQuadVB;
		std::unique_ptr<BlurPostProcessor>		m_blurProcessor;
	};
}