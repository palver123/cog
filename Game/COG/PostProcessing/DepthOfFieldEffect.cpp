#include "pch.h"
#include "DepthOfFieldEffect.h"

using namespace DirectX;
using namespace COG;

void DoFPostProcessor::Apply(ID3D11ShaderResourceView* const* inputTextures, unsigned int numInputTextures, ID3D11RenderTargetView* const renderTarget)
{
	if (!m_loadingComplete || !m_blurProcessor->IsReady())
		return;

	auto context = m_deviceResources->GetD3DDeviceContext();
	const unsigned int stride = sizeof(VertexPosition2DTexture);
	const unsigned int offset = 0;

	m_blurProcessor->ApplyBlurTo(inputTextures[0]);	// I assume inputTextures[0] is the original image and inputTextures[1] is the corresponding depth map.

	context->IASetVertexBuffers(
		0,
		1,
		m_screenSizedQuadVB.GetAddressOf(),
		&stride,
		&offset
		);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Attach our input layout.
	m_resourcePool->SetInputLayout(m_postProcessIL_key);

	// Attach our vertex shader.
	m_resourcePool->SetVertexShader(m_postProcessVS_key);

	// Bind the sampler state for this texture
	m_resourcePool->SetSamplerState<PixelShader>(m_postProcessSampler_key);	// sampler state slots are reset after every draw call so at this point they are all free so it is guaranteed to be bound to sampler state slot 0.

	ID3D11ShaderResourceView* const textures[] = { inputTextures[0], inputTextures[1], m_blurProcessor->GetBlurredTextureSRV() };
	context->OMSetRenderTargets(1, &renderTarget, nullptr);
	//context->ClearRenderTargetView(renderTarget, DirectX::Colors::CornflowerBlue);
	m_resourcePool->SetPixelShader(m_postProcessPS_key);
	context->PSSetShaderResources(0, 3, textures);
	context->Draw(4, 0);

	m_resourcePool->ResetAvailableSlots();
}