#pragma once

#include "PostProcessor.h"

namespace COG{
	class BlurPostProcessor final: public PostProcessor
	{
	public:
		BlurPostProcessor(std::shared_ptr<DX::DeviceResources> deviceResources, std::shared_ptr<ResourcePool> resourcePool, float shrinkage) :
			m_shrinkage{ shrinkage }
		{
			m_effectType = Blur;
			m_deviceResources = deviceResources;
			m_resourcePool = resourcePool;
			SetPixelShaderKey();
			CreateDeviceDependentResources();
		}

		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void ApplyBlurTo(ID3D11ShaderResourceView* const texture);

		void CreateWindowSizeDependentResources();
		ID3D11ShaderResourceView* const GetBlurredTextureSRV() const { return m_SRVs[2].Get(); }
	private:
		const float m_shrinkage;
		const std::wstring m_HorizontalPS_key = L"Blur_H";
		const std::wstring m_VerticalPS_key = L"Blur_V";
		D3D11_VIEWPORT m_shrunkViewport;
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView>	m_RTVs[3];
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_SRVs[3];
		bool m_deviceDependentInitialized = false;
		bool m_windowDependentInitialized = false;
	};
}