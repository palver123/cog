#pragma once

#include "BlurEffect.h"

namespace COG{
	class DoFPostProcessor final: public PostProcessor
	{
	public:
		DoFPostProcessor(std::shared_ptr<DX::DeviceResources> deviceResources, std::shared_ptr<ResourcePool> resourcePool, float blurShrinkage = 2.0f) :
			PostProcessor{ deviceResources, resourcePool, DoF }
		{
			m_blurProcessor = std::make_unique<BlurPostProcessor>(m_deviceResources, m_resourcePool, blurShrinkage);
		}

		void Apply(ID3D11ShaderResourceView* const* inputTextures, unsigned int numInputTextures, ID3D11RenderTargetView* const renderTarget) override;
	};
}