#include "pch.h"
#include "BlurEffect.h"

using namespace DirectX;
using namespace COG;

PingPongHelper PostProcessor::pingPong;

void PingPongHelper::CreateWindowSizeDependentResources(ID3D11Device2* device, unsigned int width, unsigned int height)
{
	// Setup the description of the textures used.
	Microsoft::WRL::ComPtr<ID3D11Texture2D> postProcessTexture_A, postProcessTexture_B;
	CD3D11_TEXTURE2D_DESC postProcessDesc(
		DXGI_FORMAT_R32G32B32A32_FLOAT,
		width,
		height,
		1, // This view has only one texture.
		1, // Use a single mipmap level.
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		);

	// Create the textures.
	DX::ThrowIfFailed(
		device->CreateTexture2D(
			&postProcessDesc,
			nullptr,
			postProcessTexture_A.GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateTexture2D(
			&postProcessDesc,
			nullptr,
			postProcessTexture_B.GetAddressOf()
		)
	);

	// Setup the description of the render target view.
	CD3D11_RENDER_TARGET_VIEW_DESC postProcessRTVDesc(D3D11_RTV_DIMENSION_TEXTURE2D, postProcessDesc.Format);
	// Create the textures' render target views.
	DX::ThrowIfFailed(
		device->CreateRenderTargetView(
			postProcessTexture_A.Get(),
			&postProcessRTVDesc,
			m_postProcessRTV_A.GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateRenderTargetView(
			postProcessTexture_B.Get(),
			&postProcessRTVDesc,
			m_postProcessRTV_B.GetAddressOf()
		)
	);

	// Setup the description of the shader resource view.
	CD3D11_SHADER_RESOURCE_VIEW_DESC postProcessSRVDesc(D3D11_SRV_DIMENSION_TEXTURE2D, postProcessDesc.Format);
	// Create the textures' shader resource views.
	DX::ThrowIfFailed(
		device->CreateShaderResourceView(
			postProcessTexture_A.Get(),
			&postProcessSRVDesc,
			m_postProcessSRV_A.GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateShaderResourceView(
			postProcessTexture_B.Get(),
			&postProcessSRVDesc,
			m_postProcessSRV_B.GetAddressOf()
		)
	);

	m_initialized = true;
}

void PingPongHelper::ReleaseResources()
{
	m_initialized = false;
	m_postProcessRTV_A.Reset();
	m_postProcessRTV_B.Reset();
	m_postProcessSRV_B.Reset();
	m_postProcessSRV_A.Reset();
}

//////////////////////////////////////////////////////////////////////////

void PostProcessor::CreateDeviceDependentResources()
{
	auto loadVSTask = DX::ReadDataAsync(m_postProcessVS_key + L".cso");
	auto loadPSTask = DX::ReadDataAsync(m_postProcessPS_key + L".cso");

	if (m_blurProcessor)
		m_blurProcessor->CreateDeviceDependentResources();

	auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
		// Create a simple vertex shader for the visualization
		ID3D11VertexShader* VS;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateVertexShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&VS
			)
		);
		m_resourcePool->AddVertexShader(m_postProcessVS_key, VS);

		static const D3D11_INPUT_ELEMENT_DESC vertexDeclaration[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosition2DTexture, pos2D), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(VertexPosition2DTexture, tex), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		// Create an input layout too.
		ID3D11InputLayout* IL;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateInputLayout(
				vertexDeclaration,
				2,
				&fileData[0],
				fileData.size(),
				&IL
			)
		);
		m_resourcePool->AddInputLayout(m_postProcessIL_key, IL, sizeof(VertexPosition2DTexture));
	});

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
		ID3D11PixelShader* PS;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&PS
			)
		);
		m_resourcePool->AddPixelShader(m_postProcessPS_key, PS);
	});

	// After the pixel shader is created, create the resources for the texture sampling
	auto createPSResourcesTask = createPSTask.then([this]() {
		D3D11_SAMPLER_DESC samplerDesc;

		// Create a texture sampler state description.
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		ID3D11SamplerState* Sampler;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateSamplerState(
				&samplerDesc,
				&Sampler
			)
		);
		m_resourcePool->AddSamplerState(m_postProcessSampler_key, Sampler);
	});

	auto createVBO = (createPSResourcesTask && createVSTask).then([this]() {
		// Create a vertex buffer for a screen sized quad
		static const VertexPosition2DTexture vertices[] =
		{
			{ XMFLOAT2{ -1.0f, -1.0f }, XMFLOAT2{ 0.0f, 1.0f } },
			{ XMFLOAT2{ -1.0f, 1.0f }, XMFLOAT2{ 0.0f, 0.0f } },
			{ XMFLOAT2{ 1.0f, -1.0f }, XMFLOAT2{ 1.0f, 1.0f } },
			{ XMFLOAT2{ 1.0f, 1.0f }, XMFLOAT2{ 1.0f, 0.0f } }
		};

		D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
		vertexBufferData.pSysMem = vertices;
		vertexBufferData.SysMemPitch = 0;
		vertexBufferData.SysMemSlicePitch = 0;
		CD3D11_BUFFER_DESC vertexBufferDesc{ 4 * sizeof(VertexPosition2DTexture), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE };

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&vertexBufferDesc,
				&vertexBufferData,
				m_screenSizedQuadVB.GetAddressOf()
			)
		);
		m_loadingComplete = true;
	});
}

void PostProcessor::CreateWindowSizeDependentResources()
{
	if (m_blurProcessor)
		m_blurProcessor->CreateWindowSizeDependentResources();
}

void PostProcessor::ReleaseDeviceDependentResources()
{
	m_screenSizedQuadVB.Reset();
	if (m_blurProcessor)
		m_blurProcessor->ReleaseDeviceDependentResources();
}

bool PostProcessor::IsReady() const
{
	return m_loadingComplete && (!m_blurProcessor || m_blurProcessor->IsReady());
}

void PostProcessor::SetType(PostEffect newType)
{
	m_effectType = newType;
	m_loadingComplete = false;
	RecreatePixelShader();
}

void PostProcessor::Apply(ID3D11ShaderResourceView* const* inputTextures, unsigned int numInputTextures, ID3D11RenderTargetView* const renderTarget)
{
	if (!pingPong.m_initialized || !IsReady())
		return;

	auto context = m_deviceResources->GetD3DDeviceContext();
	const unsigned int stride = sizeof(VertexPosition2DTexture);
	const unsigned int offset = 0;

	context->OMSetRenderTargets(1, &renderTarget, nullptr);
	//context->ClearRenderTargetView(renderTarget, DirectX::Colors::CornflowerBlue);

	context->IASetVertexBuffers(
		0,
		1,
		m_screenSizedQuadVB.GetAddressOf(),
		&stride,
		&offset
		);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Attach our input layout.
	m_resourcePool->SetInputLayout(m_postProcessIL_key);

	// Attach our vertex shader.
	m_resourcePool->SetVertexShader(m_postProcessVS_key);

	// Attach our pixel shader.
	m_resourcePool->SetPixelShader(m_postProcessPS_key);

	// Bind the sampler state for this texture
	m_resourcePool->SetSamplerState<PixelShader>(m_postProcessSampler_key);	// sampler state slots are reset after every draw call so at this point they are all free so it is guaranteed to be bound to sampler state slot 0.

	// Bind the texture object(s).
	context->PSSetShaderResources(0, numInputTextures, inputTextures);

	// Draw the objects.
	context->Draw(4, 0);
	m_resourcePool->ResetAvailableSlots();
}

void PostProcessor::SetPixelShaderKey()
{
	switch (m_effectType)
	{
	case COG::Blur:
		m_postProcessPS_key = L"PassthroughPS";
		break;
	case COG::Bloom:
		m_postProcessPS_key = L"postProcess_bloom";
		break;
	case COG::DoF:
		m_postProcessPS_key = L"postProcess_dof";
		break;
	case COG::Fog3D:
		m_postProcessPS_key = L"postProcess_fog3D";
		break;
	case COG::Vignette:
		m_postProcessPS_key = L"postProcess_vignette";
		break;
	default:
		throw std::logic_error{ "Invalid post process type!" };
		break;
	}
}

void PostProcessor::RecreatePixelShader()
{
	SetPixelShaderKey();

	auto loadPSTask = DX::ReadDataAsync(m_postProcessPS_key + L".cso");

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
		ID3D11PixelShader* PS = nullptr;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
			&fileData[0],
			fileData.size(),
			nullptr,
			&PS
			)
			);
		m_resourcePool->AddPixelShader(m_postProcessPS_key, PS);
		m_loadingComplete = true;
	});
}