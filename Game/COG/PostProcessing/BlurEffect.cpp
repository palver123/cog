#include "pch.h"
#include "BlurEffect.h"

using namespace DirectX;
using namespace COG;

void BlurPostProcessor::CreateDeviceDependentResources()
{
	PostProcessor::CreateDeviceDependentResources();

	auto loadPSTaskH = DX::ReadDataAsync(m_HorizontalPS_key + L".cso");
	auto loadPSTaskV = DX::ReadDataAsync(m_VerticalPS_key + L".cso");

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTaskH = loadPSTaskH.then([this](const std::vector<byte>& fileData) {
		ID3D11PixelShader* PS = nullptr;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&PS
			)
		);
		m_resourcePool->AddPixelShader(m_HorizontalPS_key, PS);
	});

	auto createPSTaskV = loadPSTaskV.then([this](const std::vector<byte>& fileData) {
		ID3D11PixelShader* PS = nullptr;
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&PS
			)
		);
		m_resourcePool->AddPixelShader(m_VerticalPS_key, PS);
	});

	(createPSTaskH && createPSTaskV).then([this]() {
		m_deviceDependentInitialized = true;
		m_loadingComplete = m_windowDependentInitialized;
	});
}

void BlurPostProcessor::ReleaseDeviceDependentResources()
{
	for (auto rtv : m_RTVs)
		rtv.Reset();
	for (auto srv : m_SRVs)
		srv.Reset();
	m_windowDependentInitialized = false;
	m_deviceDependentInitialized = false;
}

void BlurPostProcessor::CreateWindowSizeDependentResources()
{
	if (m_deviceResources->GetDeviceFeatureLevel() > D3D_FEATURE_LEVEL_10_1)
		// Direct3D 11 supports fractional viewports; the parameter types are floating-point numbers.
		m_shrunkViewport = CD3D11_VIEWPORT(
			0.0f,
			0.0f,
			m_deviceResources->GetBackBufferSize().Width / m_shrinkage,
			m_deviceResources->GetBackBufferSize().Height / m_shrinkage
			);
	else
		// Direct3D 10 (D3D_FEATURE_LEVEL_10_1 (or below)) does not support fractional viewports.
		m_shrunkViewport = CD3D11_VIEWPORT(
			0.0f,
			0.0f,
			(unsigned int)(m_deviceResources->GetBackBufferSize().Width / m_shrinkage),
			(unsigned int)(m_deviceResources->GetBackBufferSize().Height / m_shrinkage)
		);
	const auto device = m_deviceResources->GetD3DDevice();

	// Setup the description of the textures used.
	Microsoft::WRL::ComPtr<ID3D11Texture2D> horizontallyBlurredTexture, verticallyBlurredTexture, upsampledTexture;
	CD3D11_TEXTURE2D_DESC postProcessDesc(
		DXGI_FORMAT_R32G32B32A32_FLOAT,
		(unsigned int)m_shrunkViewport.Width,
		(unsigned int)m_shrunkViewport.Height,
		1, // This view has only one texture.
		1, // Use a single mipmap level.
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		);

	// Create the textures.
	DX::ThrowIfFailed(
		device->CreateTexture2D(
			&postProcessDesc,
			nullptr,
			horizontallyBlurredTexture.GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateTexture2D(
			&postProcessDesc,
			nullptr,
			verticallyBlurredTexture.GetAddressOf()
		)
	);

	postProcessDesc.Width = (unsigned int)m_deviceResources->GetBackBufferSize().Width;
	postProcessDesc.Height = (unsigned int)m_deviceResources->GetBackBufferSize().Height;
	DX::ThrowIfFailed(
		device->CreateTexture2D(
			&postProcessDesc,
			nullptr,
			upsampledTexture.GetAddressOf()
		)
	);

	// Setup the description of the render target view.
	CD3D11_RENDER_TARGET_VIEW_DESC postProcessRTVDesc(D3D11_RTV_DIMENSION_TEXTURE2D, postProcessDesc.Format);
	// Create the textures' render target views.
	DX::ThrowIfFailed(
		device->CreateRenderTargetView(
			horizontallyBlurredTexture.Get(),
			&postProcessRTVDesc,
			m_RTVs[0].GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateRenderTargetView(
			verticallyBlurredTexture.Get(),
			&postProcessRTVDesc,
			m_RTVs[1].GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateRenderTargetView(
			upsampledTexture.Get(),
			&postProcessRTVDesc,
			m_RTVs[2].GetAddressOf()
		)
	);

	// Setup the description of the shader resource view.
	CD3D11_SHADER_RESOURCE_VIEW_DESC postProcessSRVDesc(D3D11_SRV_DIMENSION_TEXTURE2D, postProcessDesc.Format);
	// Create the textures' shader resource views.
	DX::ThrowIfFailed(
		device->CreateShaderResourceView(
			horizontallyBlurredTexture.Get(),
			&postProcessSRVDesc,
			m_SRVs[0].GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateShaderResourceView(
			verticallyBlurredTexture.Get(),
			&postProcessSRVDesc,
			m_SRVs[1].GetAddressOf()
		)
	);
	DX::ThrowIfFailed(
		device->CreateShaderResourceView(
			upsampledTexture.Get(),
			&postProcessSRVDesc,
			m_SRVs[2].GetAddressOf()
		)
	);

	m_windowDependentInitialized = true;
	m_loadingComplete = m_deviceDependentInitialized;
}

void BlurPostProcessor::ApplyBlurTo(ID3D11ShaderResourceView* const texture)
{
	if (!m_loadingComplete)
		return;

	auto context = m_deviceResources->GetD3DDeviceContext();
	const unsigned int stride = sizeof(VertexPosition2DTexture);
	const unsigned int offset = 0;

	context->IASetVertexBuffers(
		0,
		1,
		m_screenSizedQuadVB.GetAddressOf(),
		&stride,
		&offset
		);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Attach our input layout.
	m_resourcePool->SetInputLayout(m_postProcessIL_key);

	// Attach our vertex shader.
	m_resourcePool->SetVertexShader(m_postProcessVS_key);

	// Bind the sampler state for this texture
	m_resourcePool->SetSamplerState<PixelShader>(m_postProcessSampler_key);	// sampler state slots are reset after every draw call so at this point they are all free so it is guaranteed to be bound to sampler state slot 0.

	// Horizontal pass
	context->RSSetViewports(1, &m_shrunkViewport);
	context->OMSetRenderTargets(1, m_RTVs[0].GetAddressOf(), nullptr);
	//context->ClearRenderTargetView(m_RTVs[0].Get(), DirectX::Colors::CornflowerBlue);
	m_resourcePool->SetPixelShader(m_HorizontalPS_key);
	context->PSSetShaderResources(0, 1, &texture);
	context->Draw(4, 0);

	// Vertical pass
	context->OMSetRenderTargets(1, m_RTVs[1].GetAddressOf(), nullptr);
	//context->ClearRenderTargetView(m_RTVs[1].Get(), DirectX::Colors::CornflowerBlue);
	m_resourcePool->SetPixelShader(m_VerticalPS_key);
	context->PSSetShaderResources(0, 1, m_SRVs[0].GetAddressOf());
	context->Draw(4, 0);

	// Upsample pass
	context->RSSetViewports(1, m_deviceResources->GetScreenViewport());
	context->OMSetRenderTargets(1, m_RTVs[2].GetAddressOf(), nullptr);
	//context->ClearRenderTargetView(m_RTVs[2].Get(), DirectX::Colors::CornflowerBlue);
	m_resourcePool->SetPixelShader(m_postProcessPS_key);
	context->PSSetShaderResources(0, 1, m_SRVs[1].GetAddressOf());
	context->Draw(4, 0);

	m_resourcePool->ResetAvailableSlots();
}