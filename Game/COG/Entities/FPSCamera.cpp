#include "pch.h"
#include "FPSCamera.h"

using namespace COG;

FPSCamera::FPSCamera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up,
	const XMFLOAT3& lookDirection,
	const XMFLOAT3& rollPitchYaw) :
	Camera(deviceResources, resourcePool, fovAngleY, eyePos, up),
	m_rollPitchYaw{ rollPitchYaw },
	m_lookDirection{ lookDirection },
	m_speed{ _DEFAULT_CAMERA_SPEED_ },
	m_stateChanged{ true }
{
	UpdateViewMatrices();
}

FPSCamera::FPSCamera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float aspectRatio,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up,
	const XMFLOAT3& lookDirection,
	const XMFLOAT3& rollPitchYaw) :
	Camera(deviceResources, resourcePool, aspectRatio, fovAngleY, eyePos, up),
	m_rollPitchYaw{ rollPitchYaw },
	m_lookDirection{ lookDirection },
	m_speed{ _DEFAULT_CAMERA_SPEED_ },
	m_stateChanged{ true }
{
	UpdateViewMatrices();
}

FPSCamera::~FPSCamera()
{
}

void FPSCamera::UpdateViewMatrices(/*const double elapsedTime*/)
{
	XMVECTOR eye = XMLoadFloat3(&m_eyePos);
	XMVECTOR lookDir = XMLoadFloat3(&m_lookDirection);
	const auto llll = XMVector3Normalize(lookDir);
	XMVECTOR upDir = XMLoadFloat3(&m_upDirection);

	XMMATRIX rotation = XMMatrixLookToLH(eye, lookDir, upDir);

	XMStoreFloat4x4(&m_inverseViewMatrix, XMMatrixTranspose(XMMatrixTranslationFromVector(eye) * rotation));
	XMStoreFloat4x4(&m_viewMatrix, XMMatrixTranspose(rotation) * XMMatrixTranspose(XMMatrixTranslationFromVector(-eye)));
	XMStoreFloat3(&m_strafeDirection, XMVector3Normalize(XMVector3Cross(upDir, lookDir)));

	StoreViewProj();
}

void FPSCamera::Update(DX::StepTimer const& timer)
{
	if (m_stateChanged)
	{
		UpdateViewMatrices(/*timer.GetElapsedSeconds()*/);
		StoreCameraPositionCB();
		m_stateChanged = false;
	}
}

void FPSCamera::MoveForward()
{
	XMStoreFloat3(&m_eyePos, XMLoadFloat3(&m_eyePos) + XMLoadFloat3(&m_lookDirection) * m_speed);
	m_stateChanged = true;
}

void FPSCamera::MoveBackward()
{
	XMStoreFloat3(&m_eyePos, XMLoadFloat3(&m_eyePos) - XMLoadFloat3(&m_lookDirection) * m_speed);
	m_stateChanged = true;
}

void FPSCamera::MoveLeft()
{
	XMStoreFloat3(&m_eyePos, XMLoadFloat3(&m_eyePos) - XMLoadFloat3(&m_strafeDirection) * m_speed);
	m_stateChanged = true;
}

void FPSCamera::MoveRight()
{
	XMStoreFloat3(&m_eyePos, XMLoadFloat3(&m_eyePos) + XMLoadFloat3(&m_strafeDirection) * m_speed);
	m_stateChanged = true;
}

void FPSCamera::LeanLeft()
{
	m_rollPitchYaw.z -= 0.02f;
	m_stateChanged = true;
}

void FPSCamera::LeanRight()
{
	m_rollPitchYaw.z += 0.02f;
	m_stateChanged = true;
}

void FPSCamera::Reorient(float deltaX, float deltaY)
{
	m_rollPitchYaw.x += deltaX;
	m_rollPitchYaw.y += deltaY;
	XMVECTOR angles = XMLoadFloat3(&m_rollPitchYaw);
	XMStoreFloat3(&m_rollPitchYaw,
		XMVectorClamp(XMVectorModAngles(angles),
		XMVectorSet(-_CAMERA_PITCH_LIMIT_RADIANS_, -XM_2PI, -XM_PIDIV4, 1.0f),
		XMVectorSet(_CAMERA_PITCH_LIMIT_RADIANS_, XM_2PI, XM_PIDIV4, 1.0f))
		);
	XMVECTOR sin = XMVectorSin(angles);
	XMVECTOR cos = XMVectorCos(angles);
	m_lookDirection = { XMVectorGetY(sin) * XMVectorGetX(cos), -XMVectorGetX(sin), XMVectorGetY(cos) * XMVectorGetX(cos) };
	m_stateChanged = true;
}