#include "pch.h"
#include "StaticCamera.h"

using namespace COG;

StaticCamera::StaticCamera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up,
	const XMFLOAT3& lookAt) :
		Camera(deviceResources, resourcePool, fovAngleY, eyePos, up),
		m_lookAt(lookAt) { }

StaticCamera::StaticCamera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float aspectRatio,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up,
	const XMFLOAT3& lookAt) :
		Camera(deviceResources, resourcePool, aspectRatio, fovAngleY, eyePos, up),
		m_lookAt(lookAt) { }

StaticCamera::~StaticCamera()
{
}

void StaticCamera::UpdateViewMatrices()
{
	XMVECTOR eye = XMLoadFloat3(&m_eyePos);
	XMVECTOR lookAt = XMLoadFloat3(&m_lookAt);
	XMVECTOR up = XMLoadFloat3(&m_upDirection);

	if (XMVector3NearEqual(XMVector3Cross(lookAt - eye, up), XMVectorZero(), XMVectorSet(Camera::m_epsilon, Camera::m_epsilon, Camera::m_epsilon, 1.0f)))
	{
		throw "Error! Look direction of the camera is too close to up vector.";
	}

	XMMATRIX view = XMMatrixTranspose(XMMatrixLookAtRH(eye, lookAt, up));
	XMVECTOR determinant;

	XMStoreFloat4x4(&m_viewMatrix, view);
	XMStoreFloat4x4(&m_inverseViewMatrix, XMMatrixInverse(&determinant, view));

	StoreViewProj();
	StoreCameraPositionCB();
}