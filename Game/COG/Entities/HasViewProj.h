#pragma once

#include "COG/Mesh/ResourcePool.h"

namespace COG
{
	struct HasViewProjection
	{
	protected:
		// This struct is not instantiatable, except by the derived classes.
		HasViewProjection(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool);

	public:
		const DirectX::XMFLOAT4X4& GetViewMatrix() const { return m_viewMatrix; }
		const DirectX::XMFLOAT4X4& GetInverseViewMatrix() const { return m_inverseViewMatrix; }
		const DirectX::XMFLOAT4X4& GetProjectionMatrix() const { return m_projectionMatrix; }
		virtual void CreateDeviceDependentResources();
		virtual void ReleaseDeviceDependentResources();

		//template<PipelineStage stage>
		//void BindViewProjConstantBuffer(unsigned int slot) { m_resourcePool->SetConstantBuffer<stage>(m_viewProjCB.Get(), slot); }

	protected:
		template<PipelineStage stage, unsigned int viewProjSlot>
		void BindViewProj() { m_resourcePool->SetConstantBuffer<stage>(m_viewProjCB.Get(), viewProjSlot); }
		void StoreViewProj();

		const std::shared_ptr<DX::DeviceResources> m_deviceResources;
		const std::shared_ptr<ResourcePool> m_resourcePool;

		DirectX::XMFLOAT4X4 m_viewMatrix;
		DirectX::XMFLOAT4X4 m_inverseViewMatrix;
		DirectX::XMFLOAT4X4 m_projectionMatrix;

		MatrixConstantBuffer	m_viewProjMatrix;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_viewProjCB;
	};
}