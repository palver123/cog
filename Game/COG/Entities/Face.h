#pragma once

#include "DrawableGameComponent.h"
#include "COG/Mesh/ResourcePool.h"

namespace COG
{
	class Face : public DrawableGameComponent
	{
	public:
		Face(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			const LightType ltype,
			const bool lightHasNonDefaultColor,
			const bool shadowMappingEnabled = false,
			const bool castShadows = false,
			const bool receivessShadows = false);

		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void Update(DX::StepTimer const& timer) override { Update(timer, DirectX::XMMatrixIdentity()); }
		void Update(DX::StepTimer const& timer, const DirectX::XMMATRIX& worldTransform);
		void Render() override;
		void GetDepth(bool shadowPass = false) override;
		void ToggleShadowMapping(bool enabled) override;

	private:
		std::shared_ptr<DX::DeviceResources> m_deviceResources;
		std::shared_ptr<ResourcePool> m_resourcePool;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_main;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer_prepass;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_perInstanceConstantBuffer_main;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_perInstanceConstantBuffer_prepass;
		std::wstring m_VSkey_main;
		std::wstring m_VSkey_prepass;
		std::wstring m_VSkey_shadow;
		std::wstring m_PSkey;// = L"facePS";
		MeshDescription m_meshDescription;
		VertexShaderConfiguration m_vsConfig_main;
		PrepassVSConfiguration m_vsConfig_prepass;
		PixelShaderConfiguration m_psConfig;
		MatrixCollectionConstantBuffer m_matrices;
	};
}