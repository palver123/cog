#include "pch.h"
#include "HasViewProj.h"

using namespace std;
using namespace DirectX;
using namespace COG;

HasViewProjection::HasViewProjection(const shared_ptr<DX::DeviceResources>& deviceResources, const shared_ptr<ResourcePool>& resourcePool) :
	m_deviceResources{ deviceResources },
	m_resourcePool{ resourcePool }
{
	CreateDeviceDependentResources();
}

void HasViewProjection::StoreViewProj()
{
	XMStoreFloat4x4(&m_viewProjMatrix.matrix, XMLoadFloat4x4(&m_projectionMatrix) * XMLoadFloat4x4(&m_viewMatrix));
	auto context = m_deviceResources->GetD3DDeviceContext();

	// Prepare the constant buffers to send it to the graphics device.
	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	DX::ThrowIfFailed(
		context->Map(
			m_viewProjCB.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
		)
	);
	memcpy(mappedResource.pData, &m_viewProjMatrix.matrix, sizeof(XMFLOAT4X4));
	context->Unmap(m_viewProjCB.Get(), 0);
}

void HasViewProjection::CreateDeviceDependentResources()
{
	CD3D11_BUFFER_DESC viewProjConstantBufferDesc(
		sizeof(XMFLOAT4X4),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&viewProjConstantBufferDesc,
			nullptr,
			m_viewProjCB.GetAddressOf()
		)
	);
}

void HasViewProjection::ReleaseDeviceDependentResources()
{
	m_viewProjCB.Reset();
}