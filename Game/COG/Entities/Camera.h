#pragma once

#include "GameComponent.h"
#include "HasViewProj.h"
#include "COG/DeviceResources.h"

#define _CAMERA_PITCH_LIMIT_RADIANS_ (1.308996939f)
#define _DEFAULT_CAMERA_SPEED_ (0.03f)

using namespace DirectX;

namespace COG
{
	class Camera :
		public GameComponent,
		public HasViewProjection
	{
	public:
		// This constructor should be used if the aspect ratio is aligned to the screen/window size
		Camera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float fovAngleY,
			const XMFLOAT3& eyePos,
			const XMFLOAT3& up);

		// This constructor should only be used if you want to use an aspect ratio other than
		// the screen/window size ratio to present the program.
		Camera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float aspectRatio,
			float fovAngleY,
			const XMFLOAT3& eyePos,
			const XMFLOAT3& up);

		// Initializes view parameters when the window size changes.
		virtual void CreateWindowSizeDependentResources();
		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;

		template<PipelineStage stage>
		void BindViewProjConstantBuffer() { BindViewProj<stage, 1>(); }
		template<PipelineStage stage>
		void BindConstantBuffer() { m_resourcePool->SetConstantBuffer<stage>(m_camposCB.Get(), ResourcePool::camposCB_slot); }

		// Getters / Setters
		XMFLOAT3 GetEyePos() const { return m_eyePos; }
		XMFLOAT3 GetUpDirection() const { return m_upDirection; }
		float GetFOVVertical() const { return m_fovAngleY; }
		float GetAspectRatio() const { return m_aspectRatio; }

		void SetFOVVertical(float FOV) { m_fovAngleY = FOV; }
		void IncreaseFOVVertical() { m_fovAngleY += 0.3f * XM_PI / 180.0f; }
		void DecreaseFOVVertical() { m_fovAngleY -= 0.3f * XM_PI / 180.0f; }
		void SetAspectRatio(float aspectRatio) { m_aspectRatio = aspectRatio; }

		// Destructor
		virtual ~Camera();

		// Members
		bool m_alignAspectR2Screen;
	protected:
		// Static members
		static const float m_epsilon;

		// Methods
		virtual void UpdateViewMatrices() { StoreViewProj(); StoreCameraPositionCB(); }
		void StoreCameraPositionCB();

		// Members
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_camposCB;
		XMFLOAT3 m_eyePos;
		XMFLOAT3 m_upDirection;
		float m_fovAngleY;
		float m_aspectRatio;
	};
}