#include "pch.h"
#include "Camera.h"

using namespace COG;

const float Camera::m_epsilon = 0.01f;

Camera::Camera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up) :
	HasViewProjection{ deviceResources, resourcePool },
	m_aspectRatio(1.0f),
	m_fovAngleY(fovAngleY),
	m_eyePos(eyePos),
	m_upDirection(up),
	m_alignAspectR2Screen(true)
{
	CD3D11_BUFFER_DESC float4ConstantBufferDesc(
		sizeof(XMFLOAT4),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&float4ConstantBufferDesc,
			nullptr,
			m_camposCB.GetAddressOf()
		)
	);
}

Camera::Camera(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	float aspectRatio,
	float fovAngleY,
	const XMFLOAT3& eyePos,
	const XMFLOAT3& up) :
	HasViewProjection{ deviceResources, resourcePool },
	m_aspectRatio(aspectRatio),
	m_fovAngleY(fovAngleY),
	m_eyePos(eyePos),
	m_upDirection(up),
	m_alignAspectR2Screen(false)
{
	CD3D11_BUFFER_DESC float4ConstantBufferDesc(
		sizeof(XMFLOAT4),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&float4ConstantBufferDesc,
			nullptr,
			m_camposCB.GetAddressOf()
		)
	);
}

Camera::~Camera()
{
}

void Camera::CreateWindowSizeDependentResources()
{
	if (m_alignAspectR2Screen)
	{
		Windows::Foundation::Size outputSize = m_deviceResources->GetOutputSize();
		m_aspectRatio = outputSize.Width / outputSize.Height;

		// This is a simple example of change that can be made when the app is in
		// portrait or snapped view.
		if (m_aspectRatio < 1.0f)
		{
			m_fovAngleY *= 2.0f;
		}
	}

	// Left-handed coordinate system.
	XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovLH(
		m_fovAngleY,
		m_aspectRatio,
		0.2f,
		100.0f
		);

	// Note that the OrientationTransform3D matrix is post-multiplied here
	// in order to correctly orient the scene to match the display orientation.
	// This post-multiplication step is required for any draw calls that are
	// made to the swap chain render target. For draw calls to other targets,
	// this transform should not be applied.
	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMStoreFloat4x4(
		&m_projectionMatrix,
		XMMatrixTranspose(perspectiveMatrix * XMLoadFloat4x4(&orientation))
		);

	UpdateViewMatrices();
}

void Camera::CreateDeviceDependentResources()
{
	HasViewProjection::CreateDeviceDependentResources();
	CD3D11_BUFFER_DESC float4ConstantBufferDesc(
		sizeof(XMFLOAT4),
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE);
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBuffer(
			&float4ConstantBufferDesc,
			nullptr,
			m_camposCB.GetAddressOf()
		)
	);
}

void Camera::ReleaseDeviceDependentResources()
{
	m_camposCB.Reset();
	HasViewProjection::ReleaseDeviceDependentResources();
}

void Camera::StoreCameraPositionCB()
{
	auto context = m_deviceResources->GetD3DDeviceContext();

	// Prepare the constant buffers to send it to the graphics device.
	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	DX::ThrowIfFailed(
		context->Map(
			m_camposCB.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
		)
	);
	memcpy(mappedResource.pData, &m_eyePos, sizeof(m_eyePos));
	context->Unmap(m_camposCB.Get(), 0);
}