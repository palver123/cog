#include "pch.h"
#include "Face.h"

#include "..\COG\DirectXHelper.h"

using namespace std;
using namespace DirectX;
using namespace COG;

Face::Face(
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	const LightType ltype,
	const bool lightHasNonDefaultColor,
	const bool shadowMappingEnabled,
	const bool castsShadows,
	const bool receivesShadows) :
	DrawableGameComponent{ castsShadows, receivesShadows },
	m_deviceResources{ deviceResources },
	m_resourcePool{ resourcePool },
	m_vsConfig_prepass{false, true }
{
	m_meshDescription.hasNormals = true;	// The rest is default.
	m_vsConfig_main = VertexShaderConfiguration{ false, true, true, false, ltype == COG_Positional || ltype == COG_Spot, shadowMappingEnabled && receivesShadows, 0 };
	m_psConfig.lightType = ltype;
	m_psConfig.lightHasNonDefaultColor = lightHasNonDefaultColor;
	m_psConfig.hasNormal = m_meshDescription.hasNormals;
	m_psConfig.usesShadowMap = m_vsConfig_main.usesShadowMap;
	CreateDeviceDependentResources();
}

void Face::CreateDeviceDependentResources()
{
	if (m_vsConfig_main.hasNormalMatrix)
		if (m_vsConfig_main.hasWorldMatrix)
			m_matrices.matrices.resize(2);
		else
			m_matrices.matrices.resize(1);
	else if (m_vsConfig_main.hasWorldMatrix)
		m_matrices.matrices.resize(1);

	m_matrices.matrices.shrink_to_fit();
	if (m_vsConfig_main.hasWorldMatrix || m_vsConfig_main.hasNormalMatrix)
	{
		CD3D11_BUFFER_DESC constantBufferDesc(
			sizeof(XMFLOAT4X4)* (unsigned int)m_matrices.matrices.size(),
			D3D11_BIND_CONSTANT_BUFFER,
			D3D11_USAGE_DYNAMIC,
			D3D11_CPU_ACCESS_WRITE);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&constantBufferDesc,
				nullptr,
				m_perInstanceConstantBuffer_main.GetAddressOf()
			)
		);

		constantBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&constantBufferDesc,
				nullptr,
				m_perInstanceConstantBuffer_prepass.GetAddressOf()
			)
		);
	}

	auto createFaceTask = Concurrency::task<void>([this]() {
		// Create a simple vertex shader for the visualization
		m_resourcePool->CreateVertexShader(
			m_meshDescription,
			m_VSkey_main,
			m_vsConfig_main);

		m_resourcePool->CreateVertexShader(
			m_VSkey_prepass,
			m_vsConfig_prepass);

		if (isShadowCaster)
		{
			auto shadowVSConfig = m_vsConfig_prepass;
			shadowVSConfig.SetUsesShadowMap(true);
			m_resourcePool->CreateVertexShader(
				m_VSkey_shadow,
				shadowVSConfig);
		}

		// The default behaviour is that the input layout resource key is the same as the vertex shader resource key

		// Create the pixel shader for the mesh.
		m_PSkey = m_psConfig.Stringify();
		m_resourcePool->CreatePixelShader(m_PSkey, m_psConfig);
	});

	auto createVBO = createFaceTask.then([this]() {
		const VertexPosition vertices_prepass[] =
		{
			{ XMFLOAT3{ -10.0f, 0.0f, - 10.0f } },
			{ XMFLOAT3{ -10.0f, 0.0f, 10.0f } },
			{ XMFLOAT3{ 10.0f, 0.0f, -10.0f } },
			{ XMFLOAT3{ 10.0f, 0.0f, 10.0f } }
		};

		const VertexPositionNormal vertices_main[] =
		{
			{ XMFLOAT3{ -10.0f, 0.0f, -10.0f }, XMFLOAT3{ 0.0f, 1.0f, 0.0f } },
			{ XMFLOAT3{ -10.0f, 0.0f, 10.0f }, XMFLOAT3{ 0.0f, 1.0f, 0.0f } },
			{ XMFLOAT3{ 10.0f, 0.0f, -10.0f }, XMFLOAT3{ 0.0f, 1.0f, 0.0f } },
			{ XMFLOAT3{ 10.0f, 0.0f, 10.0f }, XMFLOAT3{ 0.0f, 1.0f, 0.0f } }
		};


		D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
		vertexBufferData.pSysMem = vertices_prepass;
		vertexBufferData.SysMemPitch = 0;
		vertexBufferData.SysMemSlicePitch = 0;
		CD3D11_BUFFER_DESC vertexBufferDesc{ sizeof(vertices_prepass), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE };

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&vertexBufferDesc,
				&vertexBufferData,
				m_vertexBuffer_prepass.GetAddressOf()
			)
		);

		vertexBufferData.pSysMem = vertices_main;
		vertexBufferDesc.ByteWidth = sizeof(vertices_main);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
				&vertexBufferDesc,
				&vertexBufferData,
				m_vertexBuffer_main.GetAddressOf()
			)
		);

		m_loadingComplete = true;
	});
}

void Face::ReleaseDeviceDependentResources()
{
	m_loadingComplete = false;
	m_perInstanceConstantBuffer_main.Reset();
	m_perInstanceConstantBuffer_prepass.Reset();
	m_vertexBuffer_main.Reset();
	m_vertexBuffer_prepass.Reset();
}

void Face::Update(DX::StepTimer const& timer, const XMMATRIX& worldTransform)
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete || !m_perInstanceConstantBuffer_main)
		return;

	static D3D11_MAPPED_SUBRESOURCE mappedResource;
	const auto context = m_deviceResources->GetD3DDeviceContext();

	if (m_vsConfig_main.hasWorldMatrix)
	{
		XMStoreFloat4x4(&m_matrices[0], worldTransform);
		// If there are normal vectors in the shader, pass the inverse transpose of the world transform matrix as well.
		if (m_vsConfig_main.hasNormalMatrix)
			XMStoreFloat4x4(&m_matrices[1], XMMatrixTranspose(XMMatrixInverse(nullptr, worldTransform)));

		DX::ThrowIfFailed(
			context->Map(
			m_perInstanceConstantBuffer_prepass.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
			)
			);

		memcpy(mappedResource.pData, m_matrices.matrices.data(), sizeof(XMFLOAT4X4));
		context->Unmap(m_perInstanceConstantBuffer_prepass.Get(), 0);
	}
	else if (m_vsConfig_main.hasNormalMatrix)
		XMStoreFloat4x4(&m_matrices[0], XMMatrixTranspose(XMMatrixInverse(nullptr, worldTransform)));
	else
		return;

	// Prepare the constant buffer to send it to the graphics device.
	DX::ThrowIfFailed(
		context->Map(
			m_perInstanceConstantBuffer_main.Get(),
			0,
			D3D11_MAP_WRITE_DISCARD,
			0,
			&mappedResource
		)
	);

	memcpy(mappedResource.pData, m_matrices.matrices.data(), m_matrices.matrices.size() * sizeof(XMFLOAT4X4));
	context->Unmap(m_perInstanceConstantBuffer_main.Get(), 0);
}

void Face::Render()
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete)
		return;

	auto context = m_deviceResources->GetD3DDeviceContext();

	UINT stride = m_resourcePool->GetVertexStride(m_VSkey_main);
	UINT offset = 0;

	// Send the normal and world matrices constant buffer to the graphics device.
	m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_main.Get(), 4);

	// Bind the mesh's vertex buffer to the vertex shader.
	context->IASetVertexBuffers(
		0,
		1,
		m_vertexBuffer_main.GetAddressOf(),
		&stride,
		&offset
	);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	
	// Attach our input layout.
	m_resourcePool->SetInputLayout(m_VSkey_main);

	// Attach our vertex shader.
	m_resourcePool->SetVertexShader(m_VSkey_main);

	// Attach our pixel shader.
	m_resourcePool->SetPixelShader(m_PSkey);

	// Draw the objects.
	context->Draw(4, 0);

	m_resourcePool->ResetAvailableSlots();
}

void Face::GetDepth(bool shadowPass /*= false*/)
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete)
		return;

	auto context = m_deviceResources->GetD3DDeviceContext();

	UINT stride = m_resourcePool->GetVertexStride(m_VSkey_prepass);
	UINT offset = 0;

	// Send the normal and world matrices constant buffer to the graphics device.
	m_resourcePool->SetConstantBuffer<VertexShader>(m_perInstanceConstantBuffer_prepass.Get(), 4);

	// Bind the mesh's vertex buffer to the vertex shader.
	context->IASetVertexBuffers(
		0,
		1,
		m_vertexBuffer_prepass.GetAddressOf(),
		&stride,
		&offset
		);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// Attach our input layout.
	m_resourcePool->SetInputLayout(m_VSkey_prepass);

	// Attach our vertex shader.
	m_resourcePool->SetVertexShader(shadowPass ? m_VSkey_shadow : m_VSkey_prepass);

	// Attach our pixel shader.
	m_resourcePool->SetPixelShader(ResourcePool::prepassPS_key);

	// Draw the objects.
	context->Draw(4, 0);

	m_resourcePool->ResetAvailableSlots();
}

void Face::ToggleShadowMapping(bool enabled)
{
	// Loading is asynchronous.
	if (!m_loadingComplete)
		return;

	m_loadingComplete = false;
	m_psConfig.usesShadowMap = enabled;
	m_vsConfig_main.usesShadowMap = enabled;
	auto createFaceTask = Concurrency::task<void>([this]() {
		// Create a simple vertex shader for the visualization
		m_resourcePool->CreateVertexShader(
			m_meshDescription,
			m_VSkey_main,
			m_vsConfig_main);

		m_PSkey = m_psConfig.Stringify();
		m_resourcePool->CreatePixelShader(m_PSkey, m_psConfig);
		m_loadingComplete = true;
	});
}