#pragma once

#include "COG/DeviceResources.h"
#include "Camera.h"

namespace COG
{
	class StaticCamera final: public Camera
	{
	public:
		// Constructors
		StaticCamera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float fovAngleY = 70.0f * XM_PI / 180.0f,
			const XMFLOAT3& eyePos = XMFLOAT3(0.0f, 0.7f, -13.5f),
			const XMFLOAT3& up = XMFLOAT3(0.0f, 1.0f, 0.0f),
			const XMFLOAT3& lookAt = XMFLOAT3(0.0f, 0.2f, 0.0f));

		/* This constructor should only be used if you want an aspect ratio other than the window used to present the program. */
		StaticCamera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float aspectRatio,
			float fovAngleY = 70.0f * XM_PI / 180.0f,
			const XMFLOAT3& eyePos = XMFLOAT3(0.0f, 0.7f, 1.5f),
			const XMFLOAT3& up = XMFLOAT3(0.0f, 1.0f, 0.0f),
			const XMFLOAT3& lookAt = XMFLOAT3(0.0f, 0.2f, 0.0f));


		// Methods
		void Update(DX::StepTimer const& timer) override { /* Nothing to update, since it's a 'static camera'*/}

		// Getters
		const XMFLOAT3& GetLookAt() const { return m_lookAt; }

		// Destructor
		~StaticCamera();

	private:
		// This method is never used actually, since StaticCamera is immutable. If that changes in the future, this function will be ready to deal with the changes.
		void UpdateViewMatrices() override;

		// Members
		XMFLOAT3 m_lookAt;
	};
}