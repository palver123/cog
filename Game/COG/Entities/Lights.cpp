#include "pch.h"
#include "Lights.h"

using namespace std;
using namespace COG;

void DirectionalLight::SetDirection(const XMFLOAT3& sunDir)
{
	const auto aligned = XMFLOAT4{ sunDir.x, sunDir.y, sunDir.z, 0.0f };
	XMStoreFloat4(&m_lightParameters.values[0], XMVector4Normalize(XMLoadFloat4(&aligned)));
	m_needsUpdate = true;
}

void DirectionalLight::SetViewWidth(float viewWidth)
{
	m_viewWidth = viewWidth;
	m_needsUpdate = true;
}

void DirectionalLight::SetViewHeight(float viewHeight)
{
	m_viewHeight = viewHeight;
	m_needsUpdate = true;
}

void DirectionalLight::SetNearZ(float nearZ)
{
	m_nearZ = nearZ;
	m_needsUpdate = true;
}

void DirectionalLight::SetFarZ(float farZ)
{
	m_farZ = farZ;
	m_needsUpdate = true;
}

void DirectionalLight::Update(DX::StepTimer const& timer)
{
	if (m_needsUpdate)
	{
		XMVECTOR eye = XMLoadFloat3(&m_camera->GetEyePos());
		XMVECTOR lookDir = XMLoadFloat4(&m_lightParameters.values[0]);
		XMVECTOR upDir = XMLoadFloat3(&m_camera->GetUpDirection());

		XMMATRIX rotation = XMMatrixLookToLH(eye, lookDir, upDir);

		XMStoreFloat4x4(&m_inverseViewMatrix, XMMatrixTranspose(XMMatrixTranslationFromVector(eye - lookDir * m_viewWidth * 0.5f) * rotation));
		XMStoreFloat4x4(&m_viewMatrix, XMMatrixTranspose(rotation) * XMMatrixTranspose(XMMatrixTranslationFromVector(-(eye - lookDir * m_viewWidth * 0.5f))));
		XMStoreFloat4x4(&m_projectionMatrix, XMMatrixTranspose(XMMatrixOrthographicLH(
			m_viewWidth,
			m_viewHeight,
			m_nearZ,
			m_farZ)
			));

		StoreViewProj();
		StoreLightCB();

		m_needsUpdate = false;
	}
}

const XMFLOAT3 DirectionalLight::GetDirection() const
{
	return XMFLOAT3{ m_lightParameters.values[0].x, m_lightParameters.values[0].y, m_lightParameters.values[0].z };
}

const XMFLOAT3 ILight<1, COG_Directional>::defaultColor = XMFLOAT3{ -1.0f, -1.0f, -1.0f };

//////////////////////////////////////////////////////////////////////////

void PositionalLight::SetPosition(const XMFLOAT3& pos)
{
	m_lightParameters.values[0] = XMFLOAT4{ pos.x, pos.y, pos.z, 1.0f };
	m_needsUpdate = true;
}

void PositionalLight::Update(DX::StepTimer const& timer)
{
	throw logic_error{ "Not implemented! Positional lights would need a texture cube, rather than the standard 2D texture for the shadow map." };
	//if (m_needsUpdate)
	//{
	//	m_needsUpdate = false;
	//}
}

const XMFLOAT3 ILight<1, COG_Positional>::defaultColor = XMFLOAT3{ -1.0f, -1.0f, -1.0f };

//////////////////////////////////////////////////////////////////////////

void SpotLight::SetDirection(const XMFLOAT3& lookDir)
{
	const auto aligned = XMFLOAT4{ lookDir.x, lookDir.y, lookDir.z, 0.0f };
	XMStoreFloat4(&m_lightParameters.values[0], XMVector4Normalize(XMLoadFloat4(&aligned)));
	m_needsUpdate = true;
}

void SpotLight::SetPosition(const XMFLOAT3& pos)
{
	m_lightParameters.values[1] = XMFLOAT4{ pos.x, pos.y, pos.z, 1.0f };
	m_needsUpdate = true;
}

void SpotLight::SetFOVVertical(float fov)
{
	m_fovAngleY = fov;
	m_needsUpdate = true;
}

void SpotLight::SetAspectRatio(float aspectRatio)
{
	m_aspectRatio = aspectRatio;
	m_needsUpdate = true;
}

void SpotLight::SetNearZ(float nearZ)
{
	m_nearZ = nearZ;
	m_needsUpdate = true;
}

void SpotLight::SetFarZ(float farZ)
{
	m_farZ = farZ;
	m_needsUpdate = true;
}

void SpotLight::Update(DX::StepTimer const& timer)
{
	if (m_needsUpdate)
	{
		static const auto upVector = XMFLOAT3{ 0.0f, 1.0f, 0.0f };
		XMVECTOR eye = XMLoadFloat4(&m_lightParameters.values[1]);
		XMVECTOR lookDir = XMLoadFloat4(&m_lightParameters.values[0]);
		XMVECTOR upDir = XMLoadFloat3(&upVector);

		XMMATRIX rotation = XMMatrixLookToLH(eye, lookDir, upDir);

		XMStoreFloat4x4(&m_inverseViewMatrix, XMMatrixTranspose(XMMatrixTranslationFromVector(eye) * rotation));
		XMStoreFloat4x4(&m_viewMatrix, XMMatrixTranspose(rotation) * XMMatrixTranspose(XMMatrixTranslationFromVector(-eye)));
		XMStoreFloat4x4(&m_projectionMatrix, XMMatrixTranspose(XMMatrixPerspectiveFovLH(
			m_fovAngleY,
			m_aspectRatio,
			m_nearZ,
			m_farZ)
			));

		StoreViewProj();
		StoreLightCB();

		m_needsUpdate = false;
	}
}

const XMFLOAT3 ILight<2, COG_Spot>::defaultColor = XMFLOAT3{ -1.0f, -1.0f, -1.0f };