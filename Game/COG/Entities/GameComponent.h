#pragma once

#include "COG/StepTimer.h"

namespace COG
{
	// An abstract class for all kinds of game components (static/moving objects, characters, cameras, lights,
	// sound effect players, speech engines, overlay engines, path objects, etc...)
	class GameComponent
	{
	public:
		GameComponent();

		virtual void Update(DX::StepTimer const& timer) = 0;

		virtual ~GameComponent();

		// Variables used with the game loop.
		bool m_loadingComplete;
	};
}