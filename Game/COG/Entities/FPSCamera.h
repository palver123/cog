#pragma once

#include "COG\DeviceResources.h"
#include "Camera.h"

namespace COG
{
	class FPSCamera final: public Camera
	{
	public:
		// This constructor should be used if the aspect ratio is aligned to the screen/window size
		FPSCamera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float fovAngleY = 70.0f * XM_PI / 180.0f,
			const XMFLOAT3& eyePos = XMFLOAT3(0.0f, 0.7f, -3.5f),
			const XMFLOAT3& up = XMFLOAT3(0.0f, 1.0f, 0.0f),
			const XMFLOAT3& lookDirection = XMFLOAT3(0.0f, 0.0f, 1.0f),
			const XMFLOAT3& rollPitchYaw = XMFLOAT3(0.0f, 0.0f, 0.0f));

		// This constructor should only be used if you want to use an aspect ratio other than
		// the screen/window size ratio to present the program.
		FPSCamera(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			float aspectRatio,
			float fovAngleY = 70.0f * XM_PI / 180.0f,
			const XMFLOAT3& eyePos = XMFLOAT3(0.0f, 0.7f, 1.5f),
			const XMFLOAT3& up = XMFLOAT3(0.0f, 1.0f, 0.0f),
			const XMFLOAT3& lookDirection = XMFLOAT3(0.0f, 0.0f, 1.0f),
			const XMFLOAT3& rollPitchYaw = XMFLOAT3(0.0f, 0.0f, 0.0f));


		// Methods
		void MoveForward();
		void MoveBackward();
		void MoveRight();
		void MoveLeft();
		void LeanLeft();
		void LeanRight();
		void Reorient(float deltaX, float deltaY);

		// Getters
		const XMFLOAT3& GetRollPitchYaw() const { return m_rollPitchYaw; }
		const XMFLOAT3& GetLookDirection() const { return m_lookDirection; }

		// Virtual methods
		void Update(DX::StepTimer const& timer) override;

		// Destructor
		~FPSCamera();

	private:
		// Methods
		/// <summary> Updates the view and inverse view matrices. </summary>
		/// <param name="elapsedTime">The elapsed time in seconds since the last Update call.</param>
		void UpdateViewMatrices(/*const double elapsedTime*/) override;

		// Members
		XMFLOAT3 m_rollPitchYaw;
		XMFLOAT3 m_lookDirection;
		XMFLOAT3 m_strafeDirection;
		float m_speed;
		bool m_stateChanged;
	};
}