#pragma once

#include "Camera.h"

namespace COG{
	template<size_t numValues, LightType ltype>
	struct ILight:
		public GameComponent,
		public HasViewProjection
	{
	protected:
		// This struct is not instantiatable, except by the derived structs.
		ILight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool) :
			HasViewProjection{ deviceResources, resourcePool },
			m_lightParameters{ numValues },
			m_needsUpdate{ false }
		{
			CreateLightCB();
		}

		// This struct is not instantiatable, except by the derived structs.
		ILight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const XMFLOAT3& c) :
			HasViewProjection{ deviceResources, resourcePool },
			m_lightParameters{numValues + 1},
			m_needsUpdate{ false }
		{
			m_lightParameters.values[numValues] = XMFLOAT4{ c.x, c.y, c.z, 0.0f};
			CreateLightCB();
		}
	public:
		void CreateDeviceDependentResources() override
		{
			HasViewProjection::CreateDeviceDependentResources();
			CreateLightCB();
		}

		void ReleaseDeviceDependentResources() override
		{
			m_lightCB.Reset();
			HasViewProjection::ReleaseDeviceDependentResources();
		}

		template<PipelineStage stage>
		void BindViewProjConstantBuffer() { BindViewProj<stage, 2>(); }
		template<PipelineStage stage>
		void BindConstantBuffer() { m_resourcePool->SetConstantBuffer<stage>(m_lightCB.Get(), ResourcePool::lightCB_slot); }

		LightType GetLightType() const { return ltype; }
		bool HasColor() const { return m_lightParameters.values.size() > numValues; }
	protected:
		void CreateLightCB()
		{
			CD3D11_BUFFER_DESC bufferDesc(
				(UINT)(sizeof(XMFLOAT4) * m_lightParameters.values.size()),
				D3D11_BIND_CONSTANT_BUFFER,
				D3D11_USAGE_DYNAMIC,
				D3D11_CPU_ACCESS_WRITE);
			DX::ThrowIfFailed(
				m_deviceResources->GetD3DDevice()->CreateBuffer(
					&bufferDesc,
					nullptr,
					m_lightCB.GetAddressOf()
				)
			);
		}

		void StoreLightCB()
		{
			auto context = m_deviceResources->GetD3DDeviceContext();

			// Prepare the constant buffers to send it to the graphics device.
			static D3D11_MAPPED_SUBRESOURCE mappedResource;
			DX::ThrowIfFailed(
				context->Map(
					m_lightCB.Get(),
					0,
					D3D11_MAP_WRITE_DISCARD,
					0,
					&mappedResource
				)
			);
			memcpy(mappedResource.pData, m_lightParameters.values.data(), sizeof(XMFLOAT4) * m_lightParameters.values.size());
			context->Unmap(m_lightCB.Get(), 0);
		}
		
		static const XMFLOAT3 defaultColor;

		bool m_needsUpdate;
		LightConstantBuffer	m_lightParameters;
		Microsoft::WRL::ComPtr<ID3D11Buffer> m_lightCB;
	};

	struct DirectionalLight : public ILight<1, COG_Directional>
	{
		DirectionalLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const std::shared_ptr<Camera>& camera, const XMFLOAT3& sunDir = XMFLOAT3{ 0.0f, -1.0f, 0.0f }, const XMFLOAT4& parameters = XMFLOAT4{ 25.0f, 25.0f, 0.2f, 100.0f }) :
			ILight{ deviceResources, resourcePool },
			m_camera{ camera },
			m_viewWidth{ parameters.x },
			m_viewHeight{ parameters.y },
			m_nearZ{ parameters.z },
			m_farZ{ parameters.w }
		{
			SetDirection(sunDir);
		}

		DirectionalLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const std::shared_ptr<Camera>& camera, const XMFLOAT3& sunDir, const XMFLOAT3& c, const XMFLOAT4& parameters = XMFLOAT4{ 25.0f, 25.0f, 0.2f, 100.0f }) :
			ILight{ deviceResources, resourcePool, c },
			m_camera{ camera },
			m_viewWidth{ parameters.x },
			m_viewHeight{ parameters.y },
			m_nearZ{ parameters.z },
			m_farZ{ parameters.w }
		{
			SetDirection(sunDir);
		}

		const XMFLOAT3 GetDirection() const;

		void SetDirection(const XMFLOAT3& sunDir);
		void SetViewWidth(float viewWidth);
		void SetViewHeight(float viewHeight);
		void SetNearZ(float nearZ);
		void SetFarZ(float farZ);
		void Update(DX::StepTimer const& timer) override;

	protected:
		std::shared_ptr<Camera> m_camera;
		float m_viewWidth;
		float m_viewHeight;
		float m_nearZ;
		float m_farZ;
	};

	struct PositionalLight : public ILight<1, COG_Positional>
	{
		PositionalLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const XMFLOAT3& pos = XMFLOAT3{ 0.0f, 10.0f, 0.0f }) :
			ILight{ deviceResources, resourcePool }
		{
			SetPosition(pos);
		}

		PositionalLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const XMFLOAT3& pos, const XMFLOAT3& c) :
			ILight{ deviceResources, resourcePool, c }
		{
			SetPosition(pos);
		}

		void SetPosition(const XMFLOAT3& pos);
		void Update(DX::StepTimer const& timer) override;
	};

	struct SpotLight : public ILight<2, COG_Spot>
	{
		SpotLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const XMFLOAT3& pos = XMFLOAT3{ 0.0f, 10.0f, 0.0f }, const XMFLOAT3& lookDir = XMFLOAT3{ 0.0f, -1.0f, 0.0f }, const XMFLOAT4& parameters = XMFLOAT4{ 1.221730476396030703846583537942f, 1.0f, 0.2f, 100.0f }) :
			ILight{ deviceResources, resourcePool },
			m_fovAngleY{ parameters.x },
			m_aspectRatio{ parameters.y },
			m_nearZ{ parameters.z },
			m_farZ{ parameters.w }
		{
			SetDirection(lookDir);
			SetPosition(pos);
		}

		SpotLight(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<ResourcePool>& resourcePool, const XMFLOAT3& c, const XMFLOAT3& pos, const XMFLOAT3& lookDir, const XMFLOAT4& parameters = XMFLOAT4{ 1.221730476396030703846583537942f, 1.0f, 0.2f, 100.0f }) :
			ILight{ deviceResources, resourcePool, c },
			m_fovAngleY{ parameters.x },
			m_aspectRatio{ parameters.y },
			m_nearZ{ parameters.z },
			m_farZ{ parameters.w }
		{
			SetDirection(lookDir);
			SetPosition(pos);
		}

		void SetDirection(const XMFLOAT3& lookDir);
		void SetPosition(const XMFLOAT3& pos);
		void SetFOVVertical(float fov);
		void SetAspectRatio(float aspectRatio);
		void SetNearZ(float nearZ);
		void SetFarZ(float farZ);
		void Update(DX::StepTimer const& timer) override;
	protected:
		float m_fovAngleY;
		float m_aspectRatio;
		float m_nearZ;
		float m_farZ;
	};
}