#pragma once

#include "GameComponent.h"

namespace COG
{
	// An abstract class for drawable game components. It does implement GameComponent::Update()
	// but leaves DrawableGameComponent::Render() abstract so that the user of this class is forced to override it.
	class DrawableGameComponent : public GameComponent
	{
	public:
		bool isShadowCaster;
		bool isShadowReceiver;

		DrawableGameComponent() : DrawableGameComponent{ false, false } {}

		DrawableGameComponent(bool castsShadows, bool receivesShadows):
			isShadowCaster{ castsShadows },
			isShadowReceiver{ receivesShadows }
		{}

		virtual void CreateDeviceDependentResources() = 0;
		virtual void ReleaseDeviceDependentResources() = 0;

		void Update(DX::StepTimer const& timer) override { }
		virtual void Render() = 0;
		virtual void GetDepth(bool shadowPass = false) = 0;
		virtual void ToggleShadowMapping(bool enabled) = 0;

		virtual ~DrawableGameComponent() { }
	};
}