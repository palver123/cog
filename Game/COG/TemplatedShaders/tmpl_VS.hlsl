//========================================================================//
// The services this shader offers:
// vertex structure
#define COG_OFFER_NORMAL
#define COG_OFFER_TANGENT_AND_BINORMAL
#define COG_OFFER_TEXCOORD_0
#define COG_OFFER_TEXCOORD_1
#define COG_OFFER_TEXCOORD_2
#define COG_OFFER_TEXCOORD_3
#define COG_OFFER_TEXCOORD_4
#define COG_OFFER_TEXCOORD_5
#define COG_OFFER_TEXCOORD_6
#define COG_OFFER_VERTEX_COLOR_0
#define COG_OFFER_VERTEX_COLOR_1
#define COG_OFFER_VERTEX_COLOR_2
#define COG_OFFER_VERTEX_COLOR_3
#define COG_OFFER_VERTEX_COLOR_4
#define COG_OFFER_VERTEX_COLOR_5
#define COG_OFFER_VERTEX_COLOR_6
#define COG_OFFER_VERTEX_COLOR_7
#define COG_OFFER_SKINNING
// constant buffers
#define COG_OFFER_WORLDMATRIX
#define COG_OFFER_NORMALMATRIX
#define COG_OFFER_TIME_VARIANT
#define COG_OFFER_SHADOW_MAP
//========================================================================//

#ifdef COG_HAS_WORLDMATRIX
#define COG_HAS_PER_MODEL_CB
#endif
#ifdef COG_HAS_NORMALMATRIX
#define COG_HAS_PER_MODEL_CB
#endif


#ifdef COG_TIME_VARIANT
// Per-frame constant buffers
cbuffer PerFrameCB : register(b0)
{
	float4 frameTime;
}
#endif

// Per-pass constant buffers
cbuffer PerPassCB : register(b1)
{
	matrix viewProjMatrix;
};

#ifdef COG_SHADOW_MAP
cbuffer LightCB : register(b2)
{
	matrix lightViewProjMatrix;
}
#endif

#ifdef COG_HAS_SKINNING
// Per-skinned model constant buffers
cbuffer PerSkinnedModelCB : register(b3)
{
	matrix<float, 4, 4> bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL];
}
#endif

#ifdef COG_HAS_PER_MODEL_CB
// Per-model constant buffers
cbuffer PerModelCB : register(b4)
{
#ifdef COG_HAS_WORLDMATRIX
	matrix worldMatrix;
#endif
#ifdef COG_HAS_NORMALMATRIX
	matrix normalMatrix;
#endif
}
#endif

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos			: POSITION;
#ifdef COG_HAS_NORMAL
	float3 normal		: NORMAL0;
#endif
#ifdef COG_HAS_TANGENT_AND_BINORMAL
	float3 tangent		: TANGENT0;
	float3 binormal		: BINORMAL0;
#endif
#ifdef COG_HAS_SKINNING
	float4 weights		: BLENDWEIGHT0;
	uint4  boneIndices	: BLENDINDICES0;
#endif
#ifdef COG_HAS_TEXCOORD_0
	float2 tex0			: TEXCOORD0;
#endif
#ifdef COG_HAS_TEXCOORD_1
	float2 tex1			: TEXCOORD1;
#endif
#ifdef COG_HAS_TEXCOORD_2
	float2 tex2			: TEXCOORD2;
#endif
#ifdef COG_HAS_TEXCOORD_3
	float2 tex3			: TEXCOORD3;
#endif
#ifdef COG_HAS_TEXCOORD_4
	float2 tex4			: TEXCOORD4;
#endif
#ifdef COG_HAS_TEXCOORD_5
	float2 tex5			: TEXCOORD5;
#endif
#ifdef COG_HAS_TEXCOORD_6
	float2 tex6			: TEXCOORD6;
#endif
#ifdef COG_HAS_VERTEX_COLOR_0
	float4 color0		: COLOR0;
#endif
#ifdef COG_HAS_VERTEX_COLOR_1
	float4 color1		: COLOR1;
#endif
#ifdef COG_HAS_VERTEX_COLOR_2
	float4 color2		: COLOR2;
#endif
#ifdef COG_HAS_VERTEX_COLOR_3
	float4 color3		: COLOR3;
#endif
#ifdef COG_HAS_VERTEX_COLOR_4
	float4 color4		: COLOR4;
#endif
#ifdef COG_HAS_VERTEX_COLOR_5
	float4 color5		: COLOR5;
#endif
#ifdef COG_HAS_VERTEX_COLOR_6
	float4 color6		: COLOR6;
#endif
#ifdef COG_HAS_VERTEX_COLOR_7
	float4 color7		: COLOR7;
#endif
};

struct PixelShaderInput
{
	float4 pos			: SV_POSITION;	// Position from the camera's point of view.
#ifdef COG_SHADOW_MAP
	float4 lpos			: TEXCOORD0;	// Position from the light's point of view.
#endif
#ifdef COG_HAS_NORMAL
	float3 normal		: NORMAL0;
	#ifdef COG_USE_WORLDPOS
		float3 worldPos	: COLOR8;	// Not sure color 8 semantics is allowed. Can't find the maximum supported semantic index for COLOR... Let's hope for the best!
	#endif
#endif
#ifdef COG_HAS_TANGENT_AND_BINORMAL
	float3 tangent		: TANGENT0;
	float3 binormal		: BINORMAL0;
#endif
#ifdef COG_HAS_TEXCOORD_0
	float2 tex0			: TEXCOORD1;
#endif
#ifdef COG_HAS_TEXCOORD_1
	float2 tex1			: TEXCOORD2;
#endif
#ifdef COG_HAS_TEXCOORD_2
	float2 tex2			: TEXCOORD3;
#endif
#ifdef COG_HAS_TEXCOORD_3
	float2 tex3			: TEXCOORD4;
#endif
#ifdef COG_HAS_TEXCOORD_4
	float2 tex4			: TEXCOORD5;
#endif
#ifdef COG_HAS_TEXCOORD_5
	float2 tex5			: TEXCOORD6;
#endif
#ifdef COG_HAS_TEXCOORD_6
	float2 tex6			: TEXCOORD7;
#endif
#ifdef COG_HAS_VERTEX_COLOR_0
	float4 color0		: COLOR0;
#endif
#ifdef COG_HAS_VERTEX_COLOR_1
	float4 color1		: COLOR1;
#endif
#ifdef COG_HAS_VERTEX_COLOR_2
	float4 color2		: COLOR2;
#endif
#ifdef COG_HAS_VERTEX_COLOR_3
	float4 color3		: COLOR3;
#endif
#ifdef COG_HAS_VERTEX_COLOR_4
	float4 color4		: COLOR4;
#endif
#ifdef COG_HAS_VERTEX_COLOR_5
	float4 color5		: COLOR5;
#endif
#ifdef COG_HAS_VERTEX_COLOR_6
	float4 color6		: COLOR6;
#endif
#ifdef COG_HAS_VERTEX_COLOR_7
	float4 color7		: COLOR7;
#endif
};

// Simple shader to do vertex processing on the GPU.
PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

	float4 pos = float4(input.pos, 1.0f);
#ifdef COG_HAS_NORMAL
	float4 normal = float4(input.normal, 0.0f);
#endif
#ifdef COG_HAS_TANGENT_AND_BINORMAL
	float4 tangent = float4(input.tangent, 0.0f);
	float4 binormal = float4(input.binormal, 0.0f);
#endif
#ifdef COG_HAS_SKINNING
	// Apply the skinning transformations
	matrix<float, 4, 4> skinning = 0;

	[unroll]
	for (int i = 0; i < COG_MAX_NUMBER_OF_BONES_PER_VERTEX; i++)
	{
		skinning += bones[input.boneIndices[i]] * input.weights[i];
	}
	pos = mul(pos, skinning);
	#ifdef COG_HAS_NORMAL
		normal = mul(normal, skinning);
	#endif
	#ifdef COG_HAS_TANGENT_AND_BINORMAL
		tangent = mul(tangent, skinning);
		binormal = mul(binormal, skinning);
	#endif
#endif
#ifdef COG_HAS_WORLDMATRIX
	// Apply model-to-world transformation
	pos = mul(pos, worldMatrix);
#endif

#ifdef COG_USE_WORLDPOS
	// Store the world position for lighting
	output.worldPos = pos.xyz;
#endif
	// Apply world-to-normalized_screen_space transformation
	output.pos = mul(pos, viewProjMatrix);

#ifdef COG_SHADOW_MAP
	//store worldspace projected to light clip space with a texcoord semantic to be interpolated across the surface
	output.lpos = mul(pos, lightViewProjMatrix);
#endif

#ifdef COG_HAS_NORMALMATRIX
	// Apply normal vector transformation
	#ifdef COG_HAS_NORMAL
		output.normal = normalize(mul(normal, normalMatrix)).xyz;
	#endif
	#ifdef COG_HAS_TANGENT_AND_BINORMAL
		output.tangent = normalize(mul(tangent, normalMatrix)).xyz;
		output.binormal = normalize(mul(binormal, normalMatrix)).xyz;
	#endif
#else
	#ifdef COG_HAS_NORMAL
		output.normal = normalize(normal.xyz);
	#endif
	#ifdef COG_HAS_TANGENT_AND_BINORMAL
		output.tangent = normalize(tangent.xyz);
		output.binormal = normalize(binormal.xyz);
	#endif
#endif

	// Pass the texture coordinates through without modification.
#ifdef COG_HAS_TEXCOORD_0
	output.tex0 = input.tex0;
#endif
#ifdef COG_HAS_TEXCOORD_1
	output.tex1 = input.tex1;
#endif
#ifdef COG_HAS_TEXCOORD_2
	output.tex2 = input.tex2;
#endif
#ifdef COG_HAS_TEXCOORD_3
	output.tex3 = input.tex3;
#endif
#ifdef COG_HAS_TEXCOORD_4
	output.tex4 = input.tex4;
#endif
#ifdef COG_HAS_TEXCOORD_5
	output.tex5 = input.tex5;
#endif
#ifdef COG_HAS_TEXCOORD_6
	output.tex6 = input.tex6;
#endif
#ifdef COG_HAS_TEXCOORD_7
	output.tex7 = input.tex7;
#endif
	// Pass the vertex colours through without modification.
#ifdef COG_HAS_VERTEX_COLOR_0
	output.color0 = input.color0;
#endif
#ifdef COG_HAS_VERTEX_COLOR_1
	output.color1 = input.color1;
#endif
#ifdef COG_HAS_VERTEX_COLOR_2
	output.color2 = input.color2;
#endif
#ifdef COG_HAS_VERTEX_COLOR_3
	output.color3 = input.color3;
#endif
#ifdef COG_HAS_VERTEX_COLOR_4
	output.color4 = input.color4;
#endif
#ifdef COG_HAS_VERTEX_COLOR_5
	output.color5 = input.color5;
#endif
#ifdef COG_HAS_VERTEX_COLOR_6
	output.color6 = input.color6;
#endif
#ifdef COG_HAS_VERTEX_COLOR_7
	output.color7 = input.color7;
#endif

	return output;
}