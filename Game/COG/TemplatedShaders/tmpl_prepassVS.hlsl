//========================================================================//
// The services this shader offers //
//========================================================================//
#define COG_OFFER_SKINNING
#define COG_OFFER_WORLDMATRIX
#define COG_OFFER_TIME_VARIANT
#define COG_OFFER_SHADOW_MAP
#define COG_PREPASS_DEPTH

//========================================================================//
// Constant buffers (with fixed register numbers!) //
//========================================================================//

#ifdef COG_TIME_VARIANT
// Per-frame constant buffers
cbuffer PerFrameCB : register(b0)
{
	float4 frameTime;
};
#endif

// Per-pass constant buffers
#ifdef COG_SHADOW_MAP
cbuffer PerPassCB : register(b2)
#else
cbuffer PerPassCB : register(b1)
#endif
{
	matrix viewProjMatrix;
};

#ifdef COG_HAS_SKINNING
// Per-skinned_instance constant buffers
cbuffer PerSkinnedModelCB : register(b3)
{
	matrix<float, 4, 4> bones[COG_MAX_NUMBER_OF_BONES_PER_MODEL];
};
#endif

#ifdef COG_HAS_WORLDMATRIX
// Per-instance constant buffers
cbuffer PerModelCB : register(b4)
{
	matrix worldMatrix;
};
#endif


//========================================================================//
// Shader input structures //
//========================================================================//

// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos			: POSITION0;
#ifdef COG_HAS_SKINNING
	float4 weights		: BLENDWEIGHT0;
	uint4  boneIndices	: BLENDINDICES0;
#endif
};

struct PixelShaderInput
{
   float4 pos 			: SV_POSITION;
   float prepass 		: TEXCOORD0;
};


//========================================================================//
// Simple shader to do vertex processing on the GPU //
//========================================================================//

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	output.pos = float4(input.pos, 1.0f);

#ifdef COG_HAS_SKINNING
	// Apply the skinning transformations
	matrix<float, 4, 4> skinning = 0;

	[unroll]
	for (int i = 0; i < COG_MAX_NUMBER_OF_BONES_PER_VERTEX; i++)
	{
		skinning += bones[input.boneIndices[i]] * input.weights[i];
	}
	output.pos = mul(output.pos, skinning);
#endif

#ifdef COG_HAS_WORLDMATRIX
	// Apply model-to-world transformation
	output.pos = mul(output.pos, worldMatrix);
#endif

	// Apply world-to-normalized_screen_space transformation
	output.pos = mul(output.pos, viewProjMatrix);
#ifdef COG_PREPASS_DEPTH
	// Depth is Z/W.  It is stored in the first component of the prepass register.
	// Subtracting from 1 would give us more precision in floating point (but come with an extra substraction :P ).
	output.prepass = output.pos.z / output.pos.w;
#endif

	return output;
}