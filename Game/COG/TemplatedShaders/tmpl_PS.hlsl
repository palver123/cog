//========================================================================//
// The services this shader offers:
// pixel structure
#define COG_OFFER_NORMAL
#define COG_OFFER_TANGENT_AND_BINORMAL
#define COG_OFFER_TEXCOORD_0
#define COG_OFFER_TEXCOORD_1
#define COG_OFFER_TEXCOORD_2
#define COG_OFFER_TEXCOORD_3
#define COG_OFFER_TEXCOORD_4
#define COG_OFFER_TEXCOORD_5
#define COG_OFFER_TEXCOORD_6
#define COG_OFFER_VERTEX_COLOR_0
#define COG_OFFER_VERTEX_COLOR_1
#define COG_OFFER_VERTEX_COLOR_2
#define COG_OFFER_VERTEX_COLOR_3
#define COG_OFFER_VERTEX_COLOR_4
#define COG_OFFER_VERTEX_COLOR_5
#define COG_OFFER_VERTEX_COLOR_6
#define COG_OFFER_VERTEX_COLOR_7
// time variance
#define COG_OFFER_TIME_VARIANT
// maps
#define COG_OFFER_DIFFUSE_MAP
#define COG_OFFER_SPECULAR_MAP
#define COG_OFFER_AMBIENT_MAP
//#define COG_OFFER_EMISSIVE_MAP
#define COG_OFFER_NORMALS_MAP
#define COG_OFFER_SHININESS_MAP
#define COG_OFFER_OPACITY_MAP
#define COG_OFFER_SHADOW_MAP
//#define COG_OFFER_LIGHT_MAP
//#define COG_OFFER_REFLECTION_MAP
//========================================================================//

#define COG_DIR_LIGHT_ID 0
#define COG_POINT_LIGHT_ID 1
#define COG_SPOT_LIGHT_ID 2

// Consistency check:
#if defined(COG_DIFFUSE_SAMPLER_NAME) && !defined(COG_DIFFUSE_TEX_CHANNEL) || defined(COG_DIFFUSE_TEX_CHANNEL) && !defined(COG_DIFFUSE_SAMPLER_NAME)
	#error ERROR! If you wan't to use a diffuse map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_SPECULAR_SAMPLER_NAME) && !defined(COG_SPECULAR_TEX_CHANNEL) || defined(COG_SPECULAR_TEX_CHANNEL) && !defined(COG_SPECULAR_SAMPLER_NAME)
	#error ERROR! If you wan't to use a specular map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_AMBIENT_SAMPLER_NAME) && !defined(COG_AMBIENT_TEX_CHANNEL) || defined(COG_AMBIENT_TEX_CHANNEL) && !defined(COG_AMBIENT_SAMPLER_NAME)
	#error ERROR! If you wan't to use an ambient map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_EMISSIVE_SAMPLER_NAME) && !defined(COG_EMISSIVE_TEX_CHANNEL) || defined(COG_EMISSIVE_TEX_CHANNEL) && !defined(COG_EMISSIVE_SAMPLER_NAME)
	#error ERROR! If you wan't to use an emissive map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_NORMALS_SAMPLER_NAME) && (!defined(COG_NORMALS_TEX_CHANNEL) || !defined(COG_HAS_TANGENT_AND_BINORMAL))
	#error ERROR! If you wan't to use a normal map you'll have to define BOTH the sampler name, the texture coordinate channel AND need to pass tangent and binormal vectors to every pixel.
#endif

#if defined(COG_SHININESS_SAMPLER_NAME) && !defined(COG_SHININESS_TEX_CHANNEL) || defined(COG_SHININESS_TEX_CHANNEL) && !defined(COG_SHININESS_SAMPLER_NAME)
	#error ERROR! If you wan't to use a shininess map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_OPACITY_SAMPLER_NAME) && !defined(COG_OPACITY_TEX_CHANNEL) || defined(COG_OPACITY_TEX_CHANNEL) && !defined(COG_OPACITY_SAMPLER_NAME)
	#error ERROR! If you wan't to use an opacity map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_LIGHTMAP_SAMPLER_NAME) && !defined(COG_LIGHTMAP_TEX_CHANNEL) || defined(COG_LIGHTMAP_TEX_CHANNEL) && !defined(COG_LIGHTMAP_SAMPLER_NAME)
	#error ERROR! If you wan't to use a light map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if defined(COG_REFLECTION_SAMPLER_NAME) && !defined(COG_REFLECTION_TEX_CHANNEL) || defined(COG_REFLECTION_TEX_CHANNEL) && !defined(COG_REFLECTION_SAMPLER_NAME)
	#error ERROR! If you wan't to use an environment map you'll have to define BOTH the sampler name AND the texture coordinate channel.
#endif

#if !defined(COG_LIGHT_TYPE)
	#error ERROR! 'COG_LIGHT_TYPE' is missing. Please specify what kind of light the pixel shader uses! Spot light is 2, Positional light is 1, directional light is 0.
#elif COG_LIGHT_TYPE == COG_DIR_LIGHT_ID
	#if !defined(COG_HAS_NORMAL)
		#error ERROR! If you are going to use directional light(s) provide per-vertex normal vectors!
	#endif
#elif COG_LIGHT_TYPE == COG_POINT_LIGHT_ID
	#if !defined(COG_HAS_NORMAL)
		#error ERROR! If you are going to use positional light(s) provide per-vertex normal vectors!
	#endif
#elif COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID
	#if !defined(COG_HAS_NORMAL)
		#error ERROR! If you are going to use spot light(s) provide per-vertex normal vectors!
	#endif
#else
	#error ERROR! 'COG_LIGHT_TYPE' must be one of the following: 0 (directional), 1 (positional), 2 (spot).
#endif

#ifndef COG_HAS_NORMAL
	#if defined(COG_NORMALS_SAMPLER_NAME) || defined(COG_DIFFUSE_SAMPLER_NAME) || defined(COG_USE_SPECULAR_HIGHLIGHTS)
		#error ERROR! Provide per-pixel normal vectors if you want to use one of the following techniques: diffuse shading, specular shading, normal mapping.
	#endif
#endif

//========================================================================//
// The shader itself:

#ifdef COG_TIME_VARIANT
// Per-frame constant buffers
cbuffer PerFrameCB : register(b0)
{
	float4 frameTime;
}
#endif

#if defined(COG_USE_SPECULAR_HIGHLIGHTS) && !defined(COG_SPECULAR_SAMPLER_NAME)	// We only need the camera position if specular highlights are enabled and there is no specular map provided
// Per-pass constant buffers
cbuffer PerPassCB : register(b1)
{
	float4 camPos;
};
#endif

#if defined(COG_MATERIAL_HAS_COLOR) || defined(COG_MATERIAL_HAS_SHININESS) || defined(COG_MATERIAL_HAS_OPACITY)
// Per-frame constant buffers
cbuffer PerMaterialCB : register(b2)
{
	#ifdef COG_MATERIAL_HAS_COLOR
	float3 matDiffuse;
	#endif
	#ifdef COG_MATERIAL_HAS_SHININESS
	float matShininess;
	#endif
	#ifdef COG_MATERIAL_HAS_OPACITY
	float matOpacity;
	#endif
}
#endif

// The constant buffer of the light.
cbuffer lightCB : register(b3)
{
#if COG_LIGHT_TYPE == COG_DIR_LIGHT_ID
	float3 sunDir;
#elif COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID
	float3 spotDir;
#endif
#if COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID || COG_LIGHT_TYPE == COG_POINT_LIGHT_ID
	float3 lightPos;
#endif
#if COG_LIGHT_HAS_COLOR
	float3 lightColor;
#endif
}

/////////////
// GLOBALS //
/////////////
#ifdef COG_SHADOW_MAP
Texture2D m_shadowMap: register(t0);	// slot 0 is reserved for shadow map
#endif

#define COG_TEXTURE_REGISTER t1

#ifdef COG_DIFFUSE_SAMPLER_NAME
Texture2D m_textureDiffuse: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t2
#endif

#ifdef COG_SPECULAR_SAMPLER_NAME
Texture2D m_textureSpecular: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t3
#endif

#ifdef COG_AMBIENT_SAMPLER_NAME
Texture2D m_textureAmbient: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t4
#endif

#ifdef COG_EMISSIVE_SAMPLER_NAME
Texture2D m_textureEmissive: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t5
#endif

#ifdef COG_NORMALS_SAMPLER_NAME
Texture2D m_textureNormal: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t6
#endif

#ifdef COG_SHININESS_SAMPLER_NAME
Texture2D m_textureShininess: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t7
#endif

#ifdef COG_OPACITY_SAMPLER_NAME
Texture2D m_textureOpacity: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#define COG_TEXTURE_REGISTER t8
#endif

#ifdef COG_LIGHTMAP_SAMPLER_NAME
Texture2D m_textureLightmap: register(COG_TEXTURE_REGISTER);
#undef COG_TEXTURE_REGISTER
#endif

COG_PLACEHOLDER_SAMPLERS

// Per-pixel color data passed through the pixel shader.
struct PixelShaderInput
{
	float4 pos			: SV_POSITION;
#ifdef COG_SHADOW_MAP
	float4 lpos			: TEXCOORD0;
#endif
#ifdef COG_HAS_NORMAL
	float3 normal		: NORMAL0;
	#if COG_LIGHT_TYPE == COG_POINT_LIGHT_ID || COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID || (defined(COG_USE_SPECULAR_HIGHLIGHTS) && !defined(COG_SPECULAR_SAMPLER_NAME))
		float3 worldPos	: COLOR8;	// Not sure color 8 semantics is allowed. Can't find the maximum supported semantic index for COLOR... Let's hope for the best!
	#endif
#endif
#ifdef COG_HAS_TANGENT_AND_BINORMAL
	float3 tangent		: TANGENT0;
	float3 binormal		: BINORMAL0;
#endif
#ifdef COG_HAS_TEXCOORD_0
	float2 tex0			: TEXCOORD1;
#endif
#ifdef COG_HAS_TEXCOORD_1
	float2 tex1			: TEXCOORD2;
#endif
#ifdef COG_HAS_TEXCOORD_2
	float2 tex2			: TEXCOORD3;
#endif
#ifdef COG_HAS_TEXCOORD_3
	float2 tex3			: TEXCOORD4;
#endif
#ifdef COG_HAS_TEXCOORD_4
	float2 tex4			: TEXCOORD5;
#endif
#ifdef COG_HAS_TEXCOORD_5
	float2 tex5			: TEXCOORD6;
#endif
#ifdef COG_HAS_TEXCOORD_6
	float2 tex6			: TEXCOORD7;
#endif
#ifdef COG_HAS_VERTEX_COLOR_0
	float4 color0		: COLOR0;
#endif
#ifdef COG_HAS_VERTEX_COLOR_1
	float4 color1		: COLOR1;
#endif
#ifdef COG_HAS_VERTEX_COLOR_2
	float4 color2		: COLOR2;
#endif
#ifdef COG_HAS_VERTEX_COLOR_3
	float4 color3		: COLOR3;
#endif
#ifdef COG_HAS_VERTEX_COLOR_4
	float4 color4		: COLOR4;
#endif
#ifdef COG_HAS_VERTEX_COLOR_5
	float4 color5		: COLOR5;
#endif
#ifdef COG_HAS_VERTEX_COLOR_6
	float4 color6		: COLOR6;
#endif
#ifdef COG_HAS_VERTEX_COLOR_7
	float4 color7		: COLOR7;
#endif
};

//  Conditionals written like this: '#if defined VAR && VAR >= 1024' can generally be simplified to just '#if VAR >= 1024', since if VAR is not defined, it will be interpreted as having the value zero. 
#if COG_FEATURE_LEVEL > 41216	// The device supports Shader Model 5, which is necessary for the 'earlydepthstencil' attribute
[earlydepthstencil]
#endif
float4 main(PixelShaderInput input) : SV_TARGET
{
	// Determining the ambient color.
#ifdef COG_AMBIENT_SAMPLER_NAME
	float4 outColor = m_textureAmbient.Sample(COG_AMBIENT_SAMPLER_NAME, input.COG_AMBIENT_TEX_CHANNEL);
#elif defined(COG_DEFAULT_AMBIENT)
	float4 outColor = COG_DEFAULT_AMBIENT;
#else
	float4 outColor = float4(0.1f, 0.1f, 0.1f, 1.0f);
#endif	// COG_AMBIENT_SAMPLER_NAME

#ifdef COG_SHADOW_MAP
	// Testing for shadows
	//re-homogenize position after interpolation
	input.lpos.xyz /= input.lpos.w;
	//if position is not visible to the light - dont illuminate it
	//results in hard light frustum
//	if (input.lpos.x < -1.0f || input.lpos.x > 1.0f ||
//		input.lpos.y < -1.0f || input.lpos.y > 1.0f ||
//		input.lpos.z < 0.0f || input.lpos.z > 1.0f)
//		return outColor;

	//transform clip space coords to texture space coords (-1:1 to 0:1)
	input.lpos.x = input.lpos.x / 2.0f + 0.5f;
	input.lpos.y = input.lpos.y / -2.0f + 0.5f;

	//sample shadow map - point sampler
	float shadowMapDepth = m_shadowMap.Sample(shadowSampler, input.lpos.xy).r;

	//if clip space z value greater than shadow map value then pixel is in shadow
	if (shadowMapDepth < (input.lpos.z - 0.002f))
		return outColor;
#endif // COG_SHADOW_MAP

	// Determining the diffuse color.
#if defined(COG_DIFFUSE_SAMPLER_NAME)
	float4 diffuseColor = m_textureDiffuse.Sample(COG_DIFFUSE_SAMPLER_NAME, input.COG_DIFFUSE_TEX_CHANNEL);
#elif defined(COG_MATERIAL_HAS_COLOR)
	float4 diffuseColor = matDiffuse;
#else
	float4 diffuseColor = float4(0.7f, 1.0f, 0.45f, 1.0f);	// The default diffuse colour is light green.
#endif	// COG_DIFFUSE_SAMPLER_NAME

	// If there are no per-pixel normals, we are done. The result color = ambient color.
#ifdef COG_HAS_NORMAL
	// Determining the normal vector. It might be distorted by a normal map.
	#ifdef COG_NORMALS_SAMPLER_NAME
		float4 normalMap = m_textureNormal.Sample(COG_NORMALS_SAMPLER_NAME, input.COG_NORMALS_TEX_CHANNEL);
		// Expand the range of the normal value from (0, +1) to (-1, +1).
		normalMap = (normalMap * 2.0f) - 1.0f;
		// Calculate the normal from the data in the bump map. Then normalize it.
		float3 normal = normalize(normalMap.r * input.tangent + normalMap.g * input.binormal + input.normal);
	#else
		float3 normal = input.normal;
	#endif	// COG_NORMALS_SAMPLER_NAME

	// As soon as we have acquired the normal vector, determine the light direction based on the type of light we use.
	#if COG_LIGHT_TYPE == COG_DIR_LIGHT_ID
		float3 lightDir = -sunDir;
	#elif COG_LIGHT_TYPE == COG_POINT_LIGHT_ID || COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID
		float3 lightDir = normalize(lightPos - input.worldPos);
	#endif	// COG_LIGHT_TYPE
	float lambertian = max(dot(normal, lightDir), 0.0f);	// cosine of an angle
	#if COG_LIGHT_TYPE == COG_SPOT_LIGHT_ID
		if (abs(dot(spotDir, -lightDir) < 0.6f)
			lambertian *= 0.15f;
	#endif
	#ifdef COG_LIGHT_HAS_COLOR
		outColor += diffuseColor * lightColor * lambertian;
	#else
		outColor += diffuseColor * lambertian;				// light colour is WHITE by default
	#endif
	// If we do not need specular highlights we are done. The result color = ambient + diffuse.
	#ifdef COG_USE_SPECULAR_HIGHLIGHTS
		// The specular level could be calculated or read from a texture map.
		#ifdef COG_SPECULAR_SAMPLER_NAME
			float4 specularColor = m_textureSpecular.Sample(COG_SPECULAR_SAMPLER_NAME, input.COG_SPECULAR_TEX_CHANNEL);
			outColor += specularColor;
		#else
			// Blinn-Phong model: http://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_shading_model#Fragment_shader
			float3 viewDir = normalize(camPos.xyz - input.worldPos);
			float3 H = normalize(lightDir + viewDir);	// halfway vector
			float specularAngleCos = max(dot(H, normal), 0.0f);
			#ifdef COG_SHININESS_SAMPLER_NAME
				float shininessTex = m_textureShininess.Sample(COG_SHININESS_SAMPLER_NAME, input.COG_SHININESS_TEX_CHANNEL);
				float specIntensity = pow(specularAngleCos, shininessTex);
				outColor += float4(specIntensity, specIntensity, specIntensity, 1.0f);	// Specular color is WHITE by default. Alpha channel is set later.
			#elif defined(COG_MATERIAL_HAS_SHININESS)
				float specIntensity = pow(specularAngleCos, matShininess);
				outColor.xyz += float3(specIntensity, specIntensity * 0.9f, specIntensity * 0.7f);	// Specular color is {1.0, 0.9, 0.7} by default. Alpha channel is set later.
			#else
				outColor += float4(specularAngleCos, specularAngleCos, specularAngleCos, 1.0f);	// If not material properties are given, the shininess will be 0, eliminating the costly power function but giving almost no specular highlights.
			#endif	// COG_SHININESS_SAMPLER_NAME
		#endif	// COG_SPECULAR_SAMPLER_NAME
	#endif	// COG_USE_SPECULAR_HIGHLIGHTS
#endif	// COG_HAS_NORMAL

	// Determine the alpha value of the return color. It either comes from a texture, from a material attribute or is 1 by default.
#ifdef COG_OPACITY_SAMPLER_NAME
	outColor.a = m_textureOpacity.Sample(COG_OPACITY_SAMPLER_NAME, input.COG_OPACITY_TEX_CHANNEL).r;
#elif defined(COG_MATERIAL_HAS_OPACITY)
	outColor.a = matOpacity;
#else
	outColor.a = 1.0f;
#endif	// COG_OPACITY_SAMPLER_NAME
	return outColor;
}