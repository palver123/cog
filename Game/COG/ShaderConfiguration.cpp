// These includes replace the pch
#include <stdexcept>
#include <string>
#include <vector>
#include <d3dcommon.h>
#include <DirectXMath.h>

#include "COG\ShaderConfiguration.h"

namespace COG{
	const DirectX::XMFLOAT3 MaterialInfo::default_color = DirectX::XMFLOAT3{ -1.0f, -1.0f, -1.0f };
	const float MaterialInfo::defaultShininess = -1.0;
	const float MaterialInfo::defaultOpacity = -1.0;

	std::string Stringify(TextureType t){
		switch (t){
		case COG_Diffuse_map:
			return "DIFFUSE";
			break;
		case COG_Specular_map:
			return "SPECULAR";
			break;
		case COG_Ambient_map:
			return "AMBIENT";
			break;
		case COG_Emissive_map:
			return "EMMISSIVE";
			break;
		case COG_Normals_map:
			return "NORMALS";
			break;
		case COG_Shininess_map:
			return "SHININESS";
			break;
		case COG_Opacity_map:
			return "OPACITY";
			break;
		case COG_Light_map:
			return "LIGHT";
			break;
		case COG_Reflection_map:
			return "REFLECTION";
			break;
		default:
			throw std::runtime_error("ERROR! Unknown texture type!");
			break;
		}
	}

	std::string Stringify(LightType t){
		switch (t){
		case COG_Directional:
			return "0";
			break;
		case COG_Positional:
			return "1";
			break;
		case COG_Spot:
			return "2";
			break;
		default:
			throw std::runtime_error("ERROR! Unknown light type!");
			break;
		}
	}

	std::wstring Stringify(PrepassFlag f)
	{
		std::wstring res;
		if ((f & Depth) != 0)
			res.append(L"D");
		if ((f & ObjectID) != 0)
			res.append(L"|O");

		return res;
	}
}