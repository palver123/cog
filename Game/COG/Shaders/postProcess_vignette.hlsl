Texture2D diffuseTexture : register(t0);
SamplerState diffuseSampler : register(s0);

struct PixelShaderInput
{
	float4 pos 			: SV_POSITION;
	float2 textureCoord	: TEXCOORD0;
};


float4 main(PixelShaderInput input) : SV_TARGET
{
	float4 c = diffuseTexture.Sample(diffuseSampler, input.textureCoord);

	float2 vignetteTexCoord = input.textureCoord - float2(0.5f, 0.5f);
	float vignette = pow(1.0f - (dot(vignetteTexCoord, vignetteTexCoord) * 1.0f), 2.0f);

	return float4(c.xyz * vignette, 1);
}