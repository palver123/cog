Texture2D originalTexture : register(t0);
SamplerState originalSampler : register(s0);

struct PixelShaderInput
{
	float4 pos 		: SV_POSITION;
	float2 TexCoord	: TEXCOORD0;
};

struct AdjacentTexel
{
	float2 texCoord1 : TEXCOORD1;
	float2 texCoord2 : TEXCOORD2;
	float2 texCoord3 : TEXCOORD3;
	float2 texCoord4 : TEXCOORD4;
	float2 texCoord5 : TEXCOORD5;
	float2 texCoord6 : TEXCOORD6;
	float2 texCoord7 : TEXCOORD7;
	float2 texCoord8 : TEXCOORD8;
	float2 texCoord9 : TEXCOORD9;
};

float4 main(PixelShaderInput input) : SV_TARGET
{
//	return originalTexture.Sample(originalSampler, input.TexCoord.xy);
	AdjacentTexel texel;
	float texelSize;
	float weight0, weight1, weight2, weight3, weight4;
	float normalization;
	float4 color;
	float textureWidth, textureHeight;
	originalTexture.GetDimensions(textureWidth, textureHeight);

	// Determine the floating point size of a texel for a screen.
	texelSize = 1.0f / textureWidth;

	// Create the weights that each neighbor pixel will contribute to the blur.
	// I use normal distribution for a Gaussian blur
	weight0 = 1.0f;
	weight1 = 0.92f;
	weight2 = 0.7f;
	weight3 = 0.38f;
	weight4 = 0.2f;

	// Create a normalized value to average the weights out a bit.
	normalization = weight0 + 2.0f * (weight1 + weight2 + weight3 + weight4);

	// Normalize the weights.
	weight0 = weight0 / normalization;
	weight1 = weight1 / normalization;
	weight2 = weight2 / normalization;
	weight3 = weight3 / normalization;
	weight4 = weight4 / normalization;

	// Initialize the color to black.
	color = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Create UV coordinates for the pixel and its four horizontal neighbors on either side.
	texel.texCoord1 = input.TexCoord + float2(texelSize * -4.0f, 0.0f);
	texel.texCoord2 = input.TexCoord + float2(texelSize * -3.0f, 0.0f);
	texel.texCoord3 = input.TexCoord + float2(texelSize * -2.0f, 0.0f);
	texel.texCoord4 = input.TexCoord + float2(texelSize * -1.0f, 0.0f);
	texel.texCoord5 = input.TexCoord;
	texel.texCoord6 = input.TexCoord + float2(texelSize *  1.0f, 0.0f);
	texel.texCoord7 = input.TexCoord + float2(texelSize *  2.0f, 0.0f);
	texel.texCoord8 = input.TexCoord + float2(texelSize *  3.0f, 0.0f);
	texel.texCoord9 = input.TexCoord + float2(texelSize *  4.0f, 0.0f);

	// Add the nine horizontal pixels to the color by the specific weight of each.
	color += originalTexture.Sample(originalSampler, texel.texCoord1) * weight4;
	color += originalTexture.Sample(originalSampler, texel.texCoord2) * weight3;
	color += originalTexture.Sample(originalSampler, texel.texCoord3) * weight2;
	color += originalTexture.Sample(originalSampler, texel.texCoord4) * weight1;
	color += originalTexture.Sample(originalSampler, texel.texCoord5) * weight0;
	color += originalTexture.Sample(originalSampler, texel.texCoord6) * weight1;
	color += originalTexture.Sample(originalSampler, texel.texCoord7) * weight2;
	color += originalTexture.Sample(originalSampler, texel.texCoord8) * weight3;
	color += originalTexture.Sample(originalSampler, texel.texCoord9) * weight4;

	// Set the alpha channel to one.
	color.a = 1.0f;

	return color;
}