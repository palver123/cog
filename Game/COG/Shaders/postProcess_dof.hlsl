Texture2D originalTex : register(t0);
Texture2D depthTex : register(t1);
Texture2D blurredTex : register(t2);

SamplerState linearSampler : register(s0);

#define FOCUS_DISTANCE 0.75f

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float2 TexCoord : TEXCOORD0;
};

float4 main(VertexShaderOutput input) : SV_TARGET
{
	float3 original = originalTex.Sample(linearSampler, input.TexCoord.xy).rgb;
	float3 blurred = blurredTex.Sample(linearSampler, input.TexCoord.xy).rgb;
	float depth = depthTex.Sample(linearSampler, input.TexCoord.xy).r;

	float CoC = saturate(pow(2.71, -10.0f * abs(FOCUS_DISTANCE - depth)));

	return float4(lerp(blurred, original, CoC), 1);
}