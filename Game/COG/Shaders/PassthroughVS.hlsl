// This structure fits into a single 16 byte register
// See packing rules: https://msdn.microsoft.com/en-us/library/windows/desktop/bb509632%28v=vs.85%29.aspx
struct VertexShaderInput
{
	float2 pos 			: POSITION0;
	float2 textureCoord	: TEXCOORD0;
};

struct PixelShaderInput
{
	float4 pos 			: SV_POSITION;
	float2 textureCoord	: TEXCOORD0;
};


PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

	output.pos = float4(input.pos.x, input.pos.y, 0.0, 1.0);
	output.textureCoord = input.textureCoord;

	return output;
}