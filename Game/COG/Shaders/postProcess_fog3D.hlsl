Texture2D diffuseTexture : register(t0);
Texture2D depthTexture : register(t1);

SamplerState linearSampler : register(s0);

struct PixelShaderInput
{
	float4 pos 			: SV_POSITION;
	float2 textureCoord	: TEXCOORD0;
};


float4 main(PixelShaderInput input) : SV_TARGET
{
	float3 original = diffuseTexture.Sample(linearSampler, input.textureCoord).rgb;
	float depth = depthTexture.Sample(linearSampler, input.textureCoord).r;

	return float4(lerp(original, float3(0.6f, 0.7f, 0.64f), saturate((depth + 0.0f) * 0.8f)), 1);
}