Texture2D originalTexture : register(t0);
SamplerState originalSampler : register(s0);

struct PixelShaderInput
{
	float4 pos 			: SV_POSITION;
	float2 textureCoord	: TEXCOORD0;
}; 

float4 main(PixelShaderInput input) : SV_TARGET
{
	return originalTexture.Sample(originalSampler, input.textureCoord.xy);
}