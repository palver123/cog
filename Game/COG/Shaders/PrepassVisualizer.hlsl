struct PixelShaderInput
{
	float4 pos 					: SV_POSITION;
	noperspective float prepass	: TEXCOORD0;
}; 

float main(PixelShaderInput input) : SV_TARGET
{
	return input.prepass;
}