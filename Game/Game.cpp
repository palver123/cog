﻿#include "pch.h"
#include "Game.h"
#include "COG/DirectXHelper.h"
#include "COG/PostProcessing/DepthOfFieldEffect.h"

#include "assimp/postprocess.h"

using namespace UberGame;
using namespace Windows::Foundation;
using namespace Windows::System::Threading;
using namespace Concurrency;
using namespace std;

Game::Game(const shared_ptr<DX::DeviceResources>& deviceResources) :
	m_deviceResources(deviceResources),
	m_cursorDelta(XMFLOAT2{ 0.0f, 0.0f }),
	m_shadowMappingEnabled{ true },
	m_deferredShadingEnabled{ true },
	m_postEffectsEnabled{ true },
	m_changeDeferredShading{ false },
	m_changeShadowMapping{ false },
	m_changePostEffects{ false }
{
	// Register to be notified if the Device is lost or recreated
	m_deviceResources->RegisterDeviceNotify(this);

	// Create an assimp importer to read asset files.
	m_importer = make_unique<Assimp::Importer>();

	// Create a resource pool object for the game.
	m_resourcePool = make_shared<ResourcePool>(deviceResources, m_shadowMappingEnabled);
	ID3D11RasterizerState1* rs = nullptr;
	D3D11_RASTERIZER_DESC1 rs_desc;
	rs_desc.FillMode = D3D11_FILL_SOLID;
	rs_desc.CullMode = D3D11_CULL_NONE;
	rs_desc.FrontCounterClockwise = true;
	rs_desc.DepthBias = 0;
	rs_desc.SlopeScaledDepthBias = 0.0f;
	rs_desc.DepthBiasClamp = 0.0f;
	rs_desc.DepthClipEnable = true;
	rs_desc.ScissorEnable = false;
	rs_desc.MultisampleEnable = false;
	rs_desc.AntialiasedLineEnable = false;
	rs_desc.ForcedSampleCount = 0;
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateRasterizerState1(&rs_desc, &rs)
		);
	m_resourcePool->AddRasterizerState(L"cull_none", rs);

	D3D11_DEPTH_STENCIL_DESC dss_desc;
	dss_desc.DepthEnable = true;
	dss_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;	// non-default !
	dss_desc.StencilEnable = false;
	dss_desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	dss_desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	dss_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dss_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dss_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dss_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dss_desc.BackFace = dss_desc.FrontFace;
	dss_desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;	// non-default !
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateDepthStencilState(&dss_desc, &m_deferredShadingDSS)
	);

	// Content initialization.
	m_camera = make_shared<FPSCamera>(m_deviceResources, m_resourcePool);
	m_light = make_unique<DirectionalLight>(m_deviceResources, m_resourcePool, m_camera, XMFLOAT3{ -0.1f, 0.0f, -0.2f });
//	m_light = make_unique<SpotLight>(m_deviceResources, m_resourcePool);

//	const aiScene* scene = m_importer->ReadFile("Assets/models/avatar.dae",
//	const aiScene* scene = m_importer->ReadFile("Assets/models/boblampclean.md5mesh",
//	const aiScene* scene = m_importer->ReadFile("Assets/models/altair.dae",
	const aiScene* scene = m_importer->ReadFile("Assets/models/altair_idle.dae",
//	const aiScene* scene = m_importer->ReadFile("Assets/models/altair_walk.dae",
		aiProcessPreset_TargetRealtime_Fast |
		aiProcess_ConvertToLeftHanded);
	if (scene == nullptr)
		throw std::runtime_error{m_importer->GetErrorString()};

	auto avatar = make_unique<Avatar>(
		scene,
		m_deviceResources,
		m_resourcePool,
		m_light->GetLightType(),
		m_light->HasColor(),
		m_shadowMappingEnabled,
		true);
	auto referencePlane = make_unique<Face>(
		m_deviceResources,
		m_resourcePool,
		m_light->GetLightType(),
		m_light->HasColor(),
		m_shadowMappingEnabled,
		false,
		true
		);
	auto fpsTextRenderer = make_unique<SampleFpsTextRenderer>(m_deviceResources);

	AddDrawableComponent(move(avatar));
	AddDrawableComponent(move(referencePlane));
	AddDrawableComponent(move(fpsTextRenderer));

//	m_postProcessors.push_back(make_unique<PostProcessor>(m_deviceResources, m_resourcePool, Fog3D));
	m_postProcessors.push_back(make_unique<PostProcessor>(m_deviceResources, m_resourcePool, Vignette));
	m_postProcessors.push_back(make_unique<DoFPostProcessor>(m_deviceResources, m_resourcePool));

	// Change the timer settings if you want something other than the default variable timestep mode.
	// e.g. for 60 FPS fixed timestep update logic, call:
	//
	//m_timer.SetFixedTimeStep(true);
	//m_timer.SetTargetElapsedSeconds(1.0 / 60);
}

Game::~Game()
{
	// Deregister device notification
	m_deviceResources->RegisterDeviceNotify(nullptr);
}

// Updates application state when the window size changes (e.g. device orientation change)
void Game::CreateWindowSizeDependentResources() 
{
	m_camera->CreateWindowSizeDependentResources();
	PostProcessor::CreateWindowSizeDependentResources(m_deviceResources->GetD3DDevice(), static_cast<unsigned int>(m_deviceResources->GetBackBufferSize().Width), static_cast<unsigned int>(m_deviceResources->GetBackBufferSize().Height));
	for (const auto& postProcessor : m_postProcessors)
		postProcessor->CreateWindowSizeDependentResources();
//	m_blurProcessor->CreateWindowSizeDependentResources();
}

void Game::StartRenderLoop()
{
	// If the animation render loop is already running then do not start another thread.
	if (m_renderLoopWorker != nullptr && m_renderLoopWorker->Status == AsyncStatus::Started)
	{
		return;
	}

	// Create a task that will be run on a background thread.
	auto workItemHandler = ref new WorkItemHandler([this](IAsyncAction ^ action)
	{
		// Calculate the updated frame and render once per vertical blanking interval.
		while (action->Status == AsyncStatus::Started)
		{
			critical_section::scoped_lock lock(m_criticalSection);
			Update();
			if (Render())
			{
				m_deviceResources->Present();
			}
		}
	});

	// Run task on a dedicated high priority background thread.
	m_renderLoopWorker = ThreadPool::RunAsync(workItemHandler, WorkItemPriority::High, WorkItemOptions::TimeSliced);
}

void Game::StopRenderLoop()
{
	m_renderLoopWorker->Cancel();
}

// Updates the application state once per frame.
void Game::Update() 
{
	ProcessInput();

	// Update scene objects.
	m_timer.Tick([&]()
	{
		if (m_changeDeferredShading)
		{
			m_deferredShadingEnabled = !m_deferredShadingEnabled;
			m_changeDeferredShading = false;
		}
		if (m_changeShadowMapping)
		{
			m_shadowMappingEnabled = !m_shadowMappingEnabled;
			m_resourcePool->m_shadowMappingEnabled = m_shadowMappingEnabled;
			for (const auto& c : m_shadowCasterComponents)
				if (c->isShadowReceiver)
					c->ToggleShadowMapping(m_shadowMappingEnabled);

			for (const auto& c : m_notShadowCasterComponents)
				if (c->isShadowReceiver)
					c->ToggleShadowMapping(m_shadowMappingEnabled);
			m_changeShadowMapping = false;
		}
		if (m_changePostEffects)
		{
			m_postEffectsEnabled = !m_postEffectsEnabled;
			m_changePostEffects = false;
		}

		m_camera->Update(m_timer);
		m_light->SetDirection(XMFLOAT3{ (float)cos(m_timer.GetTotalSeconds() * 0.33f), -1.7f, (float)sin(m_timer.GetTotalSeconds() * 0.4f) });
//		m_light->SetPosition(m_camera->GetEyePos());
//		m_light->SetDirection(m_camera->GetLookDirection());
		m_light->Update(m_timer);
		m_resourcePool->Update((float)m_timer.GetTotalSeconds());

		for (const auto& c : m_miscGameComponents)
			c->Update(m_timer);

		for (const auto& c : m_shadowCasterComponents)
			c->Update(m_timer);

		for (const auto& c : m_notShadowCasterComponents)
			c->Update(m_timer);
	});
}

// Process all input from the user before updating game state
void Game::ProcessInput()
{
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::W])
		m_camera->MoveForward();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::A])
		m_camera->MoveLeft();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::S])
		m_camera->MoveBackward();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::D])
		m_camera->MoveRight();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::Q])
		m_camera->LeanLeft();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::E])
		m_camera->LeanRight();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::G])
		m_camera->IncreaseFOVVertical();
	if (m_keyboardState[(uint32)Windows::System::VirtualKey::H])
		m_camera->DecreaseFOVVertical();

	if (m_cursorDelta.x != 0.0f || m_cursorDelta.y != 0.0f)
	{
		m_camera->Reorient(m_cursorDelta.x, m_cursorDelta.y);
		m_cursorDelta = { 0.0f, 0.0f };
	}
}

// Renders the current frame according to the current application state.
// Returns true if the frame was rendered and is ready to be displayed.
bool Game::Render() 
{
	// Don't try to render anything before the first Update.
	if (m_timer.GetFrameCount() == 0 || !m_resourcePool->Ready() || !PostProcessor::Ready())
	{
		return false;
	}
	for (const auto& p : m_postProcessors)
		if (!p->IsReady())
			return false;

	m_resourcePool->BindCommonResources();
	auto context = m_deviceResources->GetD3DDeviceContext();
	ID3D11ShaderResourceView *const voidSRVs[] = { nullptr, nullptr, nullptr };
	context->PSSetShaderResources(0, 3, voidSRVs);		// Either the shadow map (or a post processor's texture(s) if shadow mapping is disabled) is still bound on slot 0,1,2. Free it! Post processors may have left 2-3 texture bound to slot 0,1 and 2, free both of them.

	// If shadow mapping is enabled, project shadow casters to the shadow map.
	if (m_shadowMappingEnabled)
	{
		// Reset the viewport to target the shadow map texture.
		context->RSSetViewports(1, m_deviceResources->GetShadowMapViewport());
		m_light->BindViewProjConstantBuffer<VertexShader>();
		GenerateShadowMap(m_deviceResources->GetShadowMapRenderTargetView());
	}
	// Reset the viewport to target the whole screen.
	context->RSSetViewports(1, m_deviceResources->GetScreenViewport());

	// If deferred shading is enabled perform the so-called 'prepass'.
	m_camera->BindViewProjConstantBuffer<VertexShader>();
	if (m_deferredShadingEnabled)
	{
		GenerateDepthMap(m_deviceResources->GetPrepassRenderTargetView());
		context->OMSetDepthStencilState(m_deferredShadingDSS.Get(), 0);	// custom depth-stencil state with LESS_OR_EQUAL
		// The current depth stencil buffer contains a 'perfect Z buffer'. Do not clear it!
	}
	else
		// If deferred shading is not enabled we need to clear the depth-stencil buffer.
		context->ClearDepthStencilView(m_deviceResources->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);

	// Now the main 'shading' pass is next. If there are post processes to apply we should render to texture, otherwise render to the back frame buffer.
	ID3D11RenderTargetView* const frameBuffers[] = { m_postProcessors.empty() || !m_postEffectsEnabled ?
		m_deviceResources->GetBackBufferRenderTargetView() :
		PostProcessor::GetPostProcessRenderTargetView()
	};
	context->OMSetRenderTargets(1, frameBuffers, m_deviceResources->GetDepthStencilView());
	context->ClearRenderTargetView(frameBuffers[0], DirectX::Colors::CornflowerBlue);

	m_camera->BindConstantBuffer<PixelShader>();	// contains the camera position for specular shading.
	m_light->BindConstantBuffer<PixelShader>();		// contains the light's position, direction, colour etc...
	if (m_shadowMappingEnabled)
	{
		const auto shadowMap = m_deviceResources->GetShadowMapShaderResourceView();
		context->PSSetShaderResources(0, 1, &shadowMap);	// The shadow map is always stored in slot 0. It is reserved.
	}

	// Render the scene objects.
	for (const auto& c : m_shadowCasterComponents)
		c->Render();

	for (const auto& c : m_notShadowCasterComponents)
		c->Render();

	if (m_deferredShadingEnabled)
		context->OMSetDepthStencilState(nullptr, 0);	// If the depth-stencil state was altered, change it back to the default!

	if (m_postProcessors.empty() || !m_postEffectsEnabled)
		return true;

//	m_blurProcessor->ApplyBlurTo(PostProcessor::GetPostProcessShaderResourceView());
	// Apply all the post processes one-by-one.
	for (unsigned int i = 0; i < m_postProcessors.size() - 1; i++)
	{
		// It is crucial that we query the shader resource view BEFORE the render target view.
		// The order of evaluation of a method call's parameters is undefined in C++.
		// Therefore I can't directly pass the return value of GetPostProcessShaderResourceViewAndSwap() to Apply() if I want it to run before GetPostProcessRenderTargetView().
		if (m_postProcessors[i]->RequiresDepthMap())
		{
			ID3D11ShaderResourceView* const sourceTextures[] = { PostProcessor::GetPostProcessShaderResourceViewAndSwap(), m_deviceResources->GetPrepassShaderResourceView() };
			m_postProcessors[i]->Apply(sourceTextures, 2, PostProcessor::GetPostProcessRenderTargetView());
		}
		else
		{
			ID3D11ShaderResourceView* const sourceTextures[] = { PostProcessor::GetPostProcessShaderResourceViewAndSwap() };
			m_postProcessors[i]->Apply(sourceTextures, 1, PostProcessor::GetPostProcessRenderTargetView());
		}
	}

	// Apply the last post effect and render it to the back buffer directly
	if (m_postProcessors[m_postProcessors.size() - 1]->RequiresDepthMap())
	{
		ID3D11ShaderResourceView* const sourceTextures[] = { PostProcessor::GetPostProcessShaderResourceView(), m_deviceResources->GetPrepassShaderResourceView() };
		m_postProcessors[m_postProcessors.size() - 1]->Apply(sourceTextures, 2, m_deviceResources->GetBackBufferRenderTargetView());
	}
	else
	{
		ID3D11ShaderResourceView* const sourceTextures[] = { PostProcessor::GetPostProcessShaderResourceView() };
		m_postProcessors[m_postProcessors.size() - 1]->Apply(sourceTextures, 1, m_deviceResources->GetBackBufferRenderTargetView());
	}

	return true;
}

void Game::GenerateShadowMap(ID3D11RenderTargetView* const shadowMap)
{
	auto context = m_deviceResources->GetD3DDeviceContext();
	context->OMSetRenderTargets(1, &shadowMap, m_deviceResources->GetShadowDepthStencilView());

	// Clear the shadow texture and depth stencil view.
	context->ClearRenderTargetView(shadowMap, DirectX::Colors::White);
	context->ClearDepthStencilView(m_deviceResources->GetShadowDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);

	// Render the scene objects.
	for (const auto& c : m_shadowCasterComponents)
		c->GetDepth(true);
}

// Use a render target view because if I write depth data into a depth stencil view and try to reuse this data in the next pass both as a texture AND depth stencil view that would mess things up.
void Game::GenerateDepthMap(ID3D11RenderTargetView* const depthMap)
{
	auto context = m_deviceResources->GetD3DDeviceContext();
	context->OMSetRenderTargets(1, &depthMap, m_deviceResources->GetDepthStencilView());

	// Clear the depth texture and depth stencil view.
	context->ClearRenderTargetView(depthMap, DirectX::Colors::White);
	context->ClearDepthStencilView(m_deviceResources->GetDepthStencilView(), D3D11_CLEAR_DEPTH, 1.0f, 0);

	// Render the scene objects.
	for (const auto& c : m_shadowCasterComponents)
		c->GetDepth();

	for (const auto& c : m_notShadowCasterComponents)
		c->GetDepth();
}

// Notifies renderers that device resources need to be released.
void Game::OnDeviceLost()
{
	m_resourcePool->ReleaseDeviceDependentResources();
	for (const auto& p : m_postProcessors)
		p->ReleaseDeviceDependentResources();
	for (const auto& c : m_shadowCasterComponents)
		c->ReleaseDeviceDependentResources();
	for (const auto& c : m_notShadowCasterComponents)
		c->ReleaseDeviceDependentResources();
	PostProcessor::ReleaseResources();
}

// Notifies renderers that device resources may now be recreated.
void Game::OnDeviceRestored()
{
	m_resourcePool->CreateDeviceDependentResources();
	for (const auto& p : m_postProcessors)
		p->CreateDeviceDependentResources();
	for (const auto& c : m_shadowCasterComponents)
		c->CreateDeviceDependentResources();
	for (const auto& c : m_notShadowCasterComponents)
		c->CreateDeviceDependentResources();

	ID3D11RasterizerState1* rs = nullptr;
	D3D11_RASTERIZER_DESC1 rs_desc;
	rs_desc.FillMode = D3D11_FILL_SOLID;
	rs_desc.CullMode = D3D11_CULL_NONE;
	rs_desc.FrontCounterClockwise = true;
	rs_desc.DepthBias = 0;
	rs_desc.SlopeScaledDepthBias = 0.0f;
	rs_desc.DepthBiasClamp = 0.0f;
	rs_desc.DepthClipEnable = true;
	rs_desc.ScissorEnable = false;
	rs_desc.MultisampleEnable = false;
	rs_desc.AntialiasedLineEnable = false;
	rs_desc.ForcedSampleCount = 0;
	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateRasterizerState1(&rs_desc, &rs)
		);
	m_resourcePool->AddRasterizerState(L"cull_none", rs);

	CreateWindowSizeDependentResources();
}

void Game::OnKeyDown(Windows::System::VirtualKey key)
{
	uint32 keyCode = (uint32)key;
	if (keyCode >= 0 && keyCode < _VIRTUAL_KEYS_COUNT_)
	{
		m_keyboardState[keyCode] = true;
	}
}

void Game::OnKeyUp(Windows::System::VirtualKey key)
{
	uint32 keyCode = (uint32)key;
	if (keyCode >= 0 && keyCode < _VIRTUAL_KEYS_COUNT_)
	{
		m_keyboardState[keyCode] = false;
	}
}

void Game::OnMouseMoved(int deltaX, int deltaY)
{
	Windows::Foundation::Size outputSize = m_deviceResources->GetOutputSize();

	m_cursorDelta.x += deltaY / outputSize.Width;
	m_cursorDelta.y += deltaX / outputSize.Height;
}

void Game::ToggleDeferredShading()
{
	m_changeDeferredShading = !m_changeDeferredShading;
}

void Game::ToggleShadows()
{
	// TODO: Implement shadow on/off
	m_changeShadowMapping = !m_changeShadowMapping;
}

void Game::TogglePostProcessing()
{
	m_changePostEffects = !m_changePostEffects;
}

void Game::AddComponent(std::unique_ptr<GameComponent>&& component)
{
	m_miscGameComponents.push_back(move(component));
}

void Game::AddDrawableComponent(std::unique_ptr<DrawableGameComponent>&&  component)
{
	if (component->isShadowCaster)
		m_shadowCasterComponents.push_back(move(component));
	else
		m_notShadowCasterComponents.push_back(move(component));
}