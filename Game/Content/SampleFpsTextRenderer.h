﻿#pragma once

#include <string>
#include "COG/DeviceResources.h"
#include "COG/Entities/DrawableGameComponent.h"

namespace UberGame
{
	// Renders the current FPS value in the bottom right corner of the screen using Direct2D and DirectWrite.
	class SampleFpsTextRenderer: public COG::DrawableGameComponent
	{
	public:
		SampleFpsTextRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void Update(DX::StepTimer const& timer) override;
		void Render() override;
		void GetDepth(bool shadowPass = false) override { }
		void ToggleShadowMapping(bool enabled) override { }

	private:
		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Resources related to text rendering.
		std::wstring                                    m_text;
		DWRITE_TEXT_METRICS	                            m_textMetrics;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush>    m_whiteBrush;
		Microsoft::WRL::ComPtr<ID2D1DrawingStateBlock>  m_stateBlock;
		Microsoft::WRL::ComPtr<IDWriteTextLayout>       m_textLayout;
		Microsoft::WRL::ComPtr<IDWriteTextFormat>		m_textFormat;
	};
}