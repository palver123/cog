struct GeometryShaderInput
{
	float4 pos	: SV_POSITION;
	float age : AGE;
};

struct GeometryShaderOutput
{
	float4 pos	: SV_POSITION;
	float age	: AGE;
	float2 tex	: TEXCOORD0;
};


[maxvertexcount(4)]
void main(
	point GeometryShaderInput input[1],
	inout TriangleStream< GeometryShaderOutput > stream)
{
	float s = 1.0f - input[0].age;
	float billboardWidth = 1.0f * s;
	float billboardHeight = 1.51f * s;
	GeometryShaderOutput output;

	output.age = input[0].age;

	output.pos = input[0].pos;
	output.pos.x += billboardWidth;
	output.pos.y += billboardHeight;
	output.tex = float2(1.0f, 0.0f);
	stream.Append(output);
	output.pos = input[0].pos;
	output.pos.x += billboardWidth;
	output.pos.y -= billboardHeight;
	output.tex = float2(1.0f, 1.0f);
	stream.Append(output);
	output.pos = input[0].pos;
	output.pos.x -= billboardWidth;
	output.pos.y += billboardHeight;
	output.tex = float2(0.0f, 0.0f);
	stream.Append(output);
	output.pos = input[0].pos;
	output.pos.x -= billboardWidth;
	output.pos.y -= billboardHeight;
	output.tex = float2(0.0f, 1.0f);
	stream.Append(output);
}