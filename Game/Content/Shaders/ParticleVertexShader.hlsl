// A constant buffer that stores the three basic column-major matrices for composing geometry.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix model;
	matrix view;
	matrix projection;
};

// A constant buffer that stores the elapsed time in seconds.
cbuffer TimeConstantBuffer : register(b1)
{
	float4 elapsedTime;
};


// Per-vertex data used as input to the vertex shader.
struct VertexShaderInput
{
	float3 pos			: POSITION;
	float3 velocity		: COLOR0;
	float2 age_lifespan : TEXCOORD0;
};

struct GeometryShaderInput
{
	float4 pos	: SV_POSITION;
	float age : AGE;
};

// Simple shader to do vertex processing on the GPU.
GeometryShaderInput main(VertexShaderInput input)
{
	GeometryShaderInput output;
	// normalized age = (initial_age + (elapsedtime % lifespan)) / lifespan
	output.age = (input.age_lifespan.x + fmod(elapsedTime.x, input.age_lifespan.y)) / input.age_lifespan.y;
	float4 particlePosition = float4(input.pos + input.velocity * (output.age - 0.5f), 1.0f);

	// Transform the vertex position into projected space.
	particlePosition = mul(particlePosition, model);
	particlePosition = mul(particlePosition, view);
	particlePosition = mul(particlePosition, projection);
	
	output.pos = particlePosition;

	return output;
}
