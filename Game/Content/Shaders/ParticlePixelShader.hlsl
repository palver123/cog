#define SQRT2OVER2	0.70710678118654752440084436210485f

/////////////
// GLOBALS //
/////////////
Texture2D particleTexture;
SamplerState particleTextureSampler;

// Per-pixel color data passed through the pixel shader.
struct PixelShaderInput
{
	float4 pos	: SV_POSITION;
	float age	: AGE;
	float2 tex	: TEXCOORD0;
};

// A pass-through function for the (interpolated) color data.
float4 main(PixelShaderInput input) : SV_TARGET
{
	float4 texColor = particleTexture.Sample(particleTextureSampler, input.tex);
	clip(texColor.a - 0.2f);
	float r = distance(input.tex, float2(0.5f, 0.5f));
	float ttl = 1.0f - input.age;
	float4 particleColor = lerp(
		float4(1.0f, 1.0f, 0.0f, ttl),
		float4(1.0f, 0.0f, 0.2f, ttl),
		1.41 * r / SQRT2OVER2);
	particleColor.xyz *= ttl;
	return texColor * particleColor;
}
