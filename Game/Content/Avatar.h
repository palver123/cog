#pragma once

#include "COG/Mesh/SkinnedModel.h"
#include "COG/Entities/Camera.h"
#include "COG/Entities/DrawableGameComponent.h"
#include "assimp/scene.h"

using namespace COG;

namespace UberGame
{
	class Avatar final: public DrawableGameComponent
	{
	public:
		Avatar(const aiScene* model,
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			const std::shared_ptr<ResourcePool>& resourcePool,
			const LightType ltype,
			const bool lightHasNonDefaultColor,
			const bool shadowMappingEnabled = false,
			const bool castsShadows = false,
			const bool receivesShadows = false);

		void CreateDeviceDependentResources() override;
		void ReleaseDeviceDependentResources() override;
		void Update(DX::StepTimer const& timer) override;
		void Render() override;
		void GetDepth(bool shadowPass = false) override;
		void ToggleShadowMapping(bool enabled) override;

		std::vector<std::unique_ptr<SkinnedModel>> m_skinnedModel;
	};
}
