#include "pch.h"
#include "ParticleSystem.h"

#include "COG/DirectXHelper.h"
#include "COG/DDS/DDSTextureLoader.h"

using namespace UberGame;

using namespace DirectX;
using namespace COG;

using namespace Windows::Foundation;

// Loads vertex, geometry and pixel shaders from files and instantiates the initial geometry.
ParticleSystem::ParticleSystem(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<FPSCamera>& camera, uint32 particleCount) :
m_loadingComplete(false),
m_deviceResources(deviceResources),
m_texture(),
m_blendState(),
m_samplerState(),
m_camera(camera)
{
	this->m_vertexCount = particleCount;
	CreateDeviceDependentResources();
}

// Called once per frame, calculates the model and view matrices.
void ParticleSystem::Update(DX::StepTimer const& timer)
{
	m_TimeConstantBufferData.value = DirectX::XMFLOAT4{ (float)timer.GetTotalSeconds(), 0.0f, 0.0f, 0.0f };
	m_MVPConstantBufferData.projection = m_camera->GetProjectionMatrix();
	m_MVPConstantBufferData.view = m_camera->GetViewMatrix();
}

// Renders one frame using the vertex and pixel shaders.
void ParticleSystem::Render()
{
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete)
	{
		return;
	}

	auto context = m_deviceResources->GetD3DDeviceContext();

	// Set render targets to the screen.
	ID3D11RenderTargetView *const targets[1] = { m_deviceResources->GetBackBufferRenderTargetView() };
	context->OMSetRenderTargets(1, targets, NULL);

	// Set the blend state for alpha composition
	context->OMSetBlendState(m_blendState.Get(), NULL, 0xffffff);

	// Prepare the constant buffer to send it to the graphics device.
	context->UpdateSubresource(
		m_MVPConstantBuffer.Get(),
		0,
		NULL,
		&m_MVPConstantBufferData,
		0,
		0
		);

	// Prepare the constant buffer to send it to the graphics device.
	context->UpdateSubresource(
		m_TimeConstantBuffer.Get(),
		0,
		NULL,
		&m_TimeConstantBufferData,
		0,
		0
		);

	// Each vertex is one instance of the VertexPositionColor struct.
	UINT stride = sizeof(ParticleVertex);
	UINT offset = 0;
	context->IASetVertexBuffers(
		0,
		1,
		m_vertexBuffer.GetAddressOf(),
		&stride,
		&offset
		);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	context->IASetInputLayout(m_inputLayout.Get());

	// Attach our vertex shader.
	context->VSSetShader(
		m_vertexShader.Get(),
		nullptr,
		0
		);

	// Send the constant buffers to the graphics device.
	context->VSSetConstantBuffers(
		0,
		1,
		m_MVPConstantBuffer.GetAddressOf()
		);

	context->VSSetConstantBuffers(
		1,
		1,
		m_TimeConstantBuffer.GetAddressOf()
		);

	// Attach our geometry shader.
	context->GSSetShader(
		m_geometryShader.Get(),
		nullptr,
		0
		);

	// Attach our pixel shader.
	context->PSSetShader(
		m_pixelShader.Get(),
		nullptr,
		0
		);

	// Set shader texture resource in the pixel shader.
	context->PSSetShaderResources(0, 1, m_texture.GetAddressOf());

	// Set the sampler state in the pixel shader.
	context->PSSetSamplers(0, 1, m_samplerState.GetAddressOf());

	// Draw the objects.
	context->Draw(
		m_vertexCount,
		0);

	// Null out the geometry shader so that other drawable objects do not get messed up
	context->GSSetShader(NULL, nullptr, 0);
}

void ParticleSystem::CreateDeviceDependentResources()
{
	// Load shaders asynchronously.
	auto loadVSTask = DX::ReadDataAsync(L"ParticleVertexShader.cso");
	auto loadGSTask = DX::ReadDataAsync(L"ParticleGeometryShader.cso");
	auto loadPSTask = DX::ReadDataAsync(L"ParticlePixelShader.cso");

	// After the vertex shader file is loaded, create the shader and input layout.
	auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateVertexShader(
			&fileData[0],
			fileData.size(),
			nullptr,
			&m_vertexShader
			)
			);

		static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(ParticleVertex, initialPos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(ParticleVertex, initialVelocity), D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(ParticleVertex, age_lifespan), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateInputLayout(
				vertexDesc,
				ARRAYSIZE(vertexDesc),
				&fileData[0],
				fileData.size(),
				&m_inputLayout
				)
			);

		CD3D11_BUFFER_DESC MVPConstantBufferDesc(sizeof(ModelViewProjectionConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
		CD3D11_BUFFER_DESC TimeConstantBufferDesc(sizeof(TimeConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
			&MVPConstantBufferDesc,
			nullptr,
			&m_MVPConstantBuffer
			)
			);

		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
			&TimeConstantBufferDesc,
			nullptr,
			&m_TimeConstantBuffer
			)
			);
	});

	// After the geometry shader file is loaded, create the shader.
	auto createGSTask = loadGSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateGeometryShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&m_geometryShader
				)
			);
	});

	// After the pixel shader file is loaded, create the shader and constant buffer.
	auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreatePixelShader(
				&fileData[0],
				fileData.size(),
				nullptr,
				&m_pixelShader
				)
			);
	});

	// After the pixel shader is created, create the resources for the texture sampling
	auto createPSResourcesTask = createPSTask.then([this]() {
		D3D11_SAMPLER_DESC samplerDesc;

		// Create a texture sampler state description.
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateSamplerState(
			&samplerDesc,
			m_samplerState.GetAddressOf()
			)
			);

		wchar_t filename[100] = L"Assets/noise.dds";

		// Create the texture object.
		DX::ThrowIfFailed(
			DirectX::CreateDDSTextureFromFile(
			m_deviceResources->GetD3DDevice(),
			filename,
			nullptr,
			m_texture.GetAddressOf()
			)
			);

	});

	// Once both shaders are loaded, create the mesh.
	auto createParticlesTask = (createPSResourcesTask && createVSTask).then([this]() {

		// Load mesh vertices. Each vertex has a position and a color.
		static ParticleVertex* particleSystemVertices = new ParticleVertex[m_vertexCount];

		for (size_t i = 0; i < m_vertexCount; i++)
		{
			float r = ((float)rand() / (float)RAND_MAX) * 0.75f;
			float phi = ((float)rand() / (float)RAND_MAX) * XM_2PI;
			// Polar coordinate system to cartesian system
			particleSystemVertices[i].initialPos = XMFLOAT3(
				r * cos(phi),										// x
				((float)rand() / (float)RAND_MAX) / 2.0f - 0.25f,	// y
				r * sin(phi)										// z
				);
			particleSystemVertices[i].age_lifespan = XMFLOAT2(
				((float)rand() / (float)RAND_MAX) * 5.0f,		// initial age
				((float)rand() / (float)RAND_MAX) * 5.0f + 5.0f	// lifespan
				);
			particleSystemVertices[i].initialVelocity = XMFLOAT3(
				((float)rand() / (float)RAND_MAX) * 0.5f,			// x
				((float)rand() / (float)RAND_MAX) * 5.0f,	// y
				((float)rand() / (float)RAND_MAX) * 0.5f			// z
				);
		}

		D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
		vertexBufferData.pSysMem = particleSystemVertices;
		vertexBufferData.SysMemPitch = 0;
		vertexBufferData.SysMemSlicePitch = 0;
		CD3D11_BUFFER_DESC vertexBufferDesc(m_vertexCount * sizeof(ParticleVertex), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE);
		DX::ThrowIfFailed(
			m_deviceResources->GetD3DDevice()->CreateBuffer(
			&vertexBufferDesc,
			&vertexBufferData,
			&m_vertexBuffer
			)
			);

		delete[] particleSystemVertices;
		particleSystemVertices = nullptr;
	});

	// Once the particle vertices are loaded, the object is ready to be rendered.
	createParticlesTask.then([this]() {
		m_loadingComplete = true;
	});

	// Alpha blending
	CD3D11_BLEND_DESC blendStateDescription;
	blendStateDescription.AlphaToCoverageEnable = FALSE;
	blendStateDescription.IndependentBlendEnable = FALSE;
	blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
	blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDescription.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	DX::ThrowIfFailed(
		m_deviceResources->GetD3DDevice()->CreateBlendState(
			&blendStateDescription,
			m_blendState.GetAddressOf()
			)
	);
}

void ParticleSystem::ReleaseDeviceDependentResources()
{
	m_loadingComplete = false;
	m_vertexShader.Reset();
	m_geometryShader.Reset();
	m_pixelShader.Reset();
	m_MVPConstantBuffer.Reset();
	m_TimeConstantBuffer.Reset();
	m_inputLayout.Reset();
	m_vertexBuffer.Reset();
	m_blendState.Reset();
	m_samplerState.Reset();
	m_texture.Reset();
}