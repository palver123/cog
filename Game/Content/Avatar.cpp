#include "pch.h"
#include "Avatar.h"
#include "COG/ShaderStructures.h"

using namespace UberGame;

#if defined( DEBUG ) || defined( _DEBUG )
#define _NUMBER_OF_TESTINSTANCES (10)
#else
#define _NUMBER_OF_TESTINSTANCES (200)
#endif

Avatar::Avatar(
	const aiScene* model,
	const std::shared_ptr<DX::DeviceResources>& deviceResources,
	const std::shared_ptr<ResourcePool>& resourcePool,
	const LightType ltype,
	const bool lightHasNonDefaultColor,
	const bool shadowMappingEnabled,
	const bool castsShadows,
	const bool receivesShadows):
	DrawableGameComponent{ castsShadows, receivesShadows }
{
	VertexShaderConfiguration vs_conf{ true, false, false, false, true, shadowMappingEnabled && receivesShadows };
	PixelShaderConfiguration ps_conf;
	ps_conf.material.shininess = 50.0f;
	ps_conf.useSpecularHighlight = true;
	ps_conf.lightType = ltype;
	ps_conf.lightHasNonDefaultColor = lightHasNonDefaultColor;
	ps_conf.usesShadowMap = vs_conf.usesShadowMap;
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel.push_back(std::make_unique<SkinnedModel>(model, deviceResources, resourcePool, "Altair", vs_conf, ps_conf, castsShadows, receivesShadows));
}

void Avatar::CreateDeviceDependentResources()
{
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel[i]->CreateDeviceDependentResources();
}

void Avatar::ReleaseDeviceDependentResources()
{
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel[i]->ReleaseDeviceDependentResources();
}

void Avatar::Update(DX::StepTimer const& timer)
{
	int size = (int)sqrt(_NUMBER_OF_TESTINSTANCES) + 1;
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
	{
		m_skinnedModel[i]->Update(timer, XMMatrixTranspose(XMMatrixTranslation(1.3f * ((i - size/2) % size), 0.0f, 1.31f * (i - size/2) / size)));
	}
}

void Avatar::Render()
{
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel[i]->Render();
}

void Avatar::GetDepth(bool shadowPass /*= false*/)
{
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel[i]->GetDepth(shadowPass);
}

void Avatar::ToggleShadowMapping(bool enabled)
{
	for (int i = 0; i < _NUMBER_OF_TESTINSTANCES; i++)
		m_skinnedModel[i]->ToggleShadowMapping(enabled);
}