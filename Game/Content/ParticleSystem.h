#pragma once

#include "COG/Entities/DrawableGameComponent.h"

namespace UberGame{
	class ParticleSystem: public COG::DrawableGameComponent
	{
	public:
		ParticleSystem(const std::shared_ptr<DX::DeviceResources>& deviceResources, const std::shared_ptr<COG::FPSCamera>& camera, uint32 particleCount);
		void CreateDeviceDependentResources();
		void ReleaseDeviceDependentResources();
		void Update(DX::StepTimer const& timer) override;
		void Render() override;
		void GetDepth() override { };

	private:
		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Direct3D resources for the geometry.
		Microsoft::WRL::ComPtr<ID3D11InputLayout>	m_inputLayout;
		Microsoft::WRL::ComPtr<ID3D11Buffer>		m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11VertexShader>	m_vertexShader;
		Microsoft::WRL::ComPtr<ID3D11GeometryShader> m_geometryShader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader>	m_pixelShader;
		Microsoft::WRL::ComPtr<ID3D11Buffer>		m_MVPConstantBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer>		m_TimeConstantBuffer;
		Microsoft::WRL::ComPtr<ID3D11SamplerState>	m_samplerState;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_texture;
		Microsoft::WRL::ComPtr<ID3D11BlendState>	m_blendState;

		// System resources for the geometry and render loop.
		ModelViewProjectionConstantBuffer	m_MVPConstantBufferData;
		TimeConstantBuffer					m_TimeConstantBufferData;
		uint32 m_vertexCount;

		// Variables used with the rendering loop.
		std::shared_ptr<COG::FPSCamera> m_camera;
		bool	m_loadingComplete;
	};
}