﻿#pragma once

#include "assimp/Importer.hpp"
#include "assimp/scene.h"

#include "COG/StepTimer.h"
#include "COG/PostProcessing/BlurEffect.h"
#include "COG/Entities/FPSCamera.h"
#include "COG/Entities/Lights.h"
#include "COG/Entities/Face.h"
//#include "Content/ParticleSystem.h"
#include "Content/Avatar.h"
#include "Content/SampleFpsTextRenderer.h"

#define _VIRTUAL_KEYS_COUNT_ 173

// Renders Direct2D and 3D content on the screen.
namespace UberGame
{
	class Game : public DX::IDeviceNotify
	{
	public:
		/// <summary>
		/// Loads and initializes application assets when the application is loaded. </summary>
		Game(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~Game();
		void CreateWindowSizeDependentResources();
		void StartRenderLoop();
		void StopRenderLoop();
		Concurrency::critical_section& GetCriticalSection() { return m_criticalSection; }

		// IDeviceNotify
		virtual void OnDeviceLost();
		virtual void OnDeviceRestored();

		// Core window event delegates
		void OnKeyDown(Windows::System::VirtualKey key);
		void OnKeyUp(Windows::System::VirtualKey key);
		void OnMouseMoved(int deltaX, int deltaY);
		void ToggleDeferredShading();
		void ToggleShadows();
		void TogglePostProcessing();

	private:
		void ProcessInput();
		void Update();
		bool Render();
		void GenerateShadowMap(ID3D11RenderTargetView* const shadowMap);
		void GenerateDepthMap(ID3D11RenderTargetView* const depthMap);
		//bool ApplyPostProcess(ID3D11ShaderResourceView* const texture);
		//void CreateVisualizerResources();
		void AddComponent(std::unique_ptr<GameComponent>&& component);
		void AddDrawableComponent(std::unique_ptr<DrawableGameComponent>&& component);

		Game(const Game& other) = delete; // non construction-copyable
		Game& operator=(const Game&) = delete; // non copyable

		// Objects responsible for post processes
		std::vector<std::unique_ptr<PostProcessor>>	m_postProcessors;

		// Cached pointers to device resources.
		std::shared_ptr<DX::DeviceResources>	m_deviceResources;
		std::shared_ptr<ResourcePool>			m_resourcePool;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_deferredShadingDSS;

		// Content
		std::shared_ptr<FPSCamera>				m_camera;	// this is a special component
		std::unique_ptr<DirectionalLight>		m_light;	// this is a special component
		std::vector<std::unique_ptr<DrawableGameComponent>>	m_shadowCasterComponents;
		std::vector<std::unique_ptr<DrawableGameComponent>>	m_notShadowCasterComponents;
		std::vector<std::unique_ptr<GameComponent>>	m_miscGameComponents;

		Windows::Foundation::IAsyncAction^		m_renderLoopWorker;
		Concurrency::critical_section			m_criticalSection;

		// Rendering loop timer.
		DX::StepTimer m_timer;

		// Track current input pointer position.
		bool m_keyboardState[_VIRTUAL_KEYS_COUNT_];
		XMFLOAT2 m_cursorDelta;

		// Assimp stuff
		std::unique_ptr<Assimp::Importer> m_importer;

		bool m_shadowMappingEnabled;
		bool m_deferredShadingEnabled;
		bool m_postEffectsEnabled;

		bool m_changeDeferredShading;
		bool m_changeShadowMapping;
		bool m_changePostEffects;
	};
}